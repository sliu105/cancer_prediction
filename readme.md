# Crowd Cancer Prediction

The current working repository is located on gitlab, due to constraints that github places on file sizes (the pytorch trained model files are about 600mb).

## Crowdsourcing/

All files to do with the crowdsourcing aspect!

## Deep_learning

Deep learning models and datasets!

## Inference script
* script functionality: measures **image-wide accuracy** on test dataset using majority vote; measures **patch-wide accuracy** on test dataset using patch prediction
* script location: `deep_learning/keras_neural_network/majority_voter.py`
* how to use?
    1. set path to model.json file by editing variable `model_file_path`;
    2. set path to weights.h5 file by editing variable `weights_file_path`;
    3. set path to test dataset foler by editing variable `test_data_path`;
    4. run the script as: `python majority_voter.py > path_to_log_file`

## Surveys
Literature writeups and reviews!

