# Software

## Qualification setup

https://github.com/akuznets0v/custom_qual_mturk_lambdas_demo

## Version 2.0 of HIT

https://github.com/akuznets0v/crowd-cancer-qualled-hit

# v1/

Soumitri HITs + Vipul/Akash analysis

# v2/ 

v2 HITs, Result Viewer, answer key checker.

# qual_fetch.py

Code used to fetch list of workers that had completed Soumitri's HIT. CONTAINS CREDENTIALS.

# workers_with_og_qual.csv
A list of workers that successfully completed Soumitri's training HIT.
