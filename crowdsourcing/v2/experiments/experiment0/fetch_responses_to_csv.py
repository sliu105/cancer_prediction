import boto3
import xml.etree.ElementTree as ET
import csv
 
ACCESS_ID = "AKIAJ63RZNZ7R5W57UEQ"
SECRET_KEY = "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"
HOST = 'mechanicalturk.amazonaws.com'
 
REGION = "us-east-1"
MTURK_CLIENT = boto3.client('mturk', REGION, aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=SECRET_KEY)
MTURK_TEST_CLIENT = boto3.client('mturk', REGION, aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=SECRET_KEY, endpoint_url='https://mturk-requester-sandbox.us-east-1.amazonaws.com')

client = MTURK_CLIENT

HIT_ID = "3NC6WP7WJHBZUV0P6AUN9OLILWIWWE"

# Returns a list of HIT_ids
def getHITs():
    all_HITs = [] 
    hits = client.list_hits()
    for i in range(0,hits['NumResults']):                   
        hitId = hits['HITs'][i]['HITId']
        all_HITs.append(hitId)
    return all_HITs

# Returns assignments for HIT, and worker responses
def get_info_from_assignments(hitId):
    results_string_list = []
    assignments = client.list_assignments_for_hit(
                    HITId = hitId,
                    AssignmentStatuses = ['Approved'],
                    MaxResults=100)

    for i in range(0,assignments['NumResults']):

        response = assignments['Assignments'][i]

        hit_id = response['HITId'].encode('utf-8')
        assignment_id = response['AssignmentId'].encode('utf-8')
        worker_id = response['WorkerId'].encode('utf-8')
        xml_string_answer = response['Answer'].encode('utf-8')

        if worker_id == "A2PNOE4EF7SJDD":
            print xml_string_answer

        assignment_info = {"hit_id": hit_id, 
                           "assignment_id": assignment_id,
                           "worker_id": worker_id,
                           "xml_string_answer": xml_string_answer}

        results_string_list.append(assignment_info)

    return results_string_list

def parse_xml_string(xml_string):
    ns = {'name_space': "http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2005-10-01/QuestionFormAnswers.xsd"}
    root = ET.fromstring(xml_string)
    answer_dict = {}
    for answer in root.findall('name_space:Answer', ns):
        key = answer.find('name_space:QuestionIdentifier', ns).text
        ans = answer.find('name_space:FreeText', ns).text
        answer_dict[key] = ans
    return answer_dict

# Converts a list of dictionaries (all with same keys) into a csv
def dict_to_csv(dict_list, file_name):
    with open(file_name, 'w') as myfile:
        writer = csv.DictWriter(myfile, fieldnames=dict_list[0].keys())
        writer.writeheader()
        for dictionary in dict_list:
            writer.writerow(dictionary)


if __name__ == "__main__":
    all_hits = getHITs()
    for hit_id in all_hits:
        if hit_id == HIT_ID:
            assignment_info_list = get_info_from_assignments(hit_id)
            for assignment_info in assignment_info_list:
                # print assignment_info
                xml_string = assignment_info['xml_string_answer'] 
                assignment_info.update(parse_xml_string(xml_string))
                assignment_info.pop('xml_string_answer', None)
            dict_to_csv(assignment_info_list, "answers_new.csv")

