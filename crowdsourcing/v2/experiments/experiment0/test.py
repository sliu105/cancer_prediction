import boto
import boto3
from boto.mturk.connection import MTurkConnection
from boto.mturk.question import HTMLQuestion
from boto.mturk.qualification import Qualifications,Requirement,PercentAssignmentsApprovedRequirement,NumberHitsApprovedRequirement
import random
from random import randint
# Create your connection to MTurk

"""
The bulk of this code is the HTMl that will be created into a question. Then, towards the end of the code, we merely sue the mTurk API to create a HIT and send it out. At this point we attach it to a qualification called demo qual 1.

To change the credentials, Just write your accessID and secret key in the space below. To switch between using the sandbox and the normal mturk, switch out the client= TEST_CLIENT line, in line 32 to client = MTURK_CLIENT
"""


ACCESS_ID = "AKIAJ63RZNZ7R5W57UEQ"
SECRET_KEY = "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"
HOST_SANDBOX = 'mechanicalturk.sandbox.amazonaws.com'
HOST = 'mechanicalturk.amazonaws.com'
REGION = "us-east-1" 

mtc = MTurkConnection(aws_access_key_id=ACCESS_ID,
                      aws_secret_access_key=SECRET_KEY,
                      host=HOST_SANDBOX)
 
 
MTURK_CLIENT = boto3.client('mturk', REGION, aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=SECRET_KEY)
MTURK_TEST_CLIENT = boto3.client('mturk', REGION, aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=SECRET_KEY, endpoint_url='https://mturk-requester-sandbox.us-east-1.amazonaws.com')
client = MTURK_CLIENT

DEMO_QUAL_1 = '3LESAWA92506TRXHB1SLTUQT7AL99Z'

title = 'Prostate cancer cell detection'
description = ('Describe some cells to help us test for prostate cancer cells!')

arrayOfCells = ["r21","g47","g9","r89"]
pivots = ["r35","r22","g71","g16"]
randCells = [""] * len(arrayOfCells) * len(pivots)
randPivs = [""] * len(arrayOfCells) * len(pivots) 


#Vertical shuffle
count_cell = 0;
count_pivt = 0;
num = random.sample(range(0,16),16);
for i in range(0,len(arrayOfCells)*len(pivots)):
	randCells[num[i]] = arrayOfCells[count_cell];
	randPivs[num[i]] = pivots[count_pivt];
	count_cell+=1
	if count_cell == 4:
		count_cell = 0
		count_pivt += 1



#print '\n'
print len(randCells)
print len(randPivs)

question_html_value = """
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<script src='https://s3.amazonaws.com/mturk-public/externalHIT_v1.js' type='text/javascript'></script>
</head>
<body>
<!-- HTML to handle creating the HIT form -->
<form name='mturk_form' method='post' id='mturk_form' action='https://www.mturk.com/mturk/externalSubmit'>
<input type='hidden' value='' name='assignmentId' id='assignmentId'/>

<!-- ############################################INTRO TASKS####################################################### -->

<h2>Instructions</h2>

<p>There are three main tasks we expect you to do for this HIT:</p>
<p>1) Identify different parts of tissue: this part teaches you how glands and lymphocytes look like. If you choose the wrong answer, the right answer will be explained to you.</p>
<p>2) Given two cells, identify which cell has the higher number of lymphocytes (with answers): If you choose the wrong answer, why it is wrong will be explained to you.</p>
<p>3) Given two cells, identify which cell has the higher number of lymphocytes (without answers): This is the main task of this assignment. Based on the brief introduction to the features we have given you, tell us which cell has the higher number of lymphocytes.</p>

<h2><u> PART I </u></h2>

<h3>Tissue image components</h3>

We will first learn how to identify the two critical components of a tissue image. We will then see how to compare these tissue images based on the number of Lymphocytes. The two critical components of a tissue image  are:

<ol>
	<li>Gland</li>
	<li>Lymphocytes</li>
</ol>

Your first task is the following: given a tissue image, you have to identify these two components. A typical image you will see, along with its annotated version is provided below:
<br><br>
<div id="instr1"></div>
<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/24.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/misc/exp3eg1.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td>Original image</td>
    <td>Annotated image</td>
  </tr>
</table>
<br><br>

We provide a verbal description for identifying the two components, <i>please read them carefully</i>. Try to match the annotated image provided above with the following description:

<ol>
  <li>Gland
  <ul style="list-style-type:circle">
    <li>Glands are round to oval shaped in general. Their shape can be slightly disfigured sometimes.</li>
    <li>Glands are proportionally large, and they are typically seperated from each other.</li>
    <li>They often have a <i>white</i> cavity inside them. Note that this need not be the case everytime.</li>
    <li>Their borders are <i>purple</i> in color.</li>
  </ul>
  </li>
  <li>Lymphocytes
  <ul style="list-style-type:circle">
  	<li>Lymphocytes are small spots that are <i>dark purple</i> in color.</li>
    <li>They are scattered randomly in the tissue image.</li>
    <li>Lymphocytes do <b>not</b> appear inside a gland.</li>
  </ul>
  </li>
</ol>

<h2>Example tasks: Identify tissue components</h2>

Your task is to correctly identify the components that we point out to in the following tissue images. We will let you know if you get an answer wrong; you are then expected to go through the <a href="#instr1">instructions</a> again if you are unable to identify the component correctly.

<h3>Image 1</h3>

<table>
	<tr>
		<td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/misc/exp3img1.png" height="500" width="500" />
		</td>
		<td>
			What does region 1 represent?  
	        <input type="radio" name="i1q1" value="1"> Gland
	        <input type="radio" name="i1q1" value="2"> Lymphocytes
          <div id='ai1q1'></div>
	        What does region 2 represent?   
	        <input type="radio" name="i1q2" value="1"> Gland
	        <input type="radio" name="i1q2" value="2"> Lymphocytes
	        <div id='ai1q2'></div>
	        What does region 3 represent?  
	        <input type="radio" name="i1q3" value="1"> Gland
	        <input type="radio" name="i1q3" value="2"> Lymphocytes
	        <div id='ai1q3'></div>
	        What does region 4 represent?   
	        <input type="radio" name="i1q4" value="1"> Gland
	        <input type="radio" name="i1q4" value="2"> Lymphocytes
          <div id='ai1q4'></div>
		</td>
	</tr>
</table>

<h3>Image 2</h3>

<table>
	<tr>
		<td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/misc/exp3img2.png" height="500" width="500" />
		</td>
		<td>
			What does region 1 represent?  
	        <input type="radio" name="i2q1" value="1"> Gland
	        <input type="radio" name="i2q1" value="2"> Lymphocytes
	        <div id='ai2q1'></div>
	        What does region 2 represent?   
	        <input type="radio" name="i2q2" value="1"> Gland
	        <input type="radio" name="i2q2" value="2"> Lymphocytes
	        <div id='ai2q2'></div>
	        What does region 3 represent?  
	        <input type="radio" name="i2q3" value="1"> Gland
	        <input type="radio" name="i2q3" value="2"> Lymphocytes
	        <div id='ai2q3'></div>
	        What does region 4 represent?   
	        <input type="radio" name="i2q4" value="1"> Gland
	        <input type="radio" name="i2q4" value="2"> Lymphocytes
          <div id='ai2q4'></div>
		</td>
	</tr>
</table>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type='text/javascript'>


$(document).ready(function(){
    $('input[name=i1q1]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai1q1').html("Correct!");
                  break;
            case '2':
                  $('#ai1q1').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i1q2]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai1q2').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai1q2').html("Correct!");
                  break;
        }
    });
    $('input[name=i1q3]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai1q3').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai1q3').html("Correct!");
                  break;
        }
    });
    $('input[name=i1q4]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai1q4').html("Correct!");
                  break;
            case '2':
                  $('#ai1q4').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i2q1]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai2q1').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai2q1').html("Correct!");
                  break;
        }
    });
    $('input[name=i2q2]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai2q2').html("Correct!");
                  break;
            case '2':
                  $('#ai2q2').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i2q3]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai2q3').html("Correct!");
                  break;
            case '2':
                  $('#ai2q3').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i2q4]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai2q4').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai2q4').html("Correct!");
                  break;
        }
    });

    $('input[name=et1]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#aet1').html("Correct!");
                  break;
            case '2':
                  $('#aet1').html("Wrong answer. Try again!<br>Hint: The glands in image 1 look a bit disfigured don't they?");
                  break;
        }
    });

    $('input[name=et2]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#aet2').html("Wrong answer. Try again!<br>Hint: Notice the large number of lymphocytes in image 2. The glands in image 2 look pretty small too!");
                  break;
            case '2':
                  $('#aet2').html("Correct!");
                  break;
        }
    });

    $('input[name=et3]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#aet3').html("Correct!");
                  break;
            case '2':
                  $('#aet3').html("Wrong answer. Try again!<br>Hint: The glands in image 2 seem bigger and well structured compared to the ones in image 1!");
                  break;
        }
    });

    $('input[name=et4]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#aet4').html("Wrong answer. Try again!<br>Hint: So many lymphocytes in image 2! Certainly can't be a good sign.");
                  break;
            case '2':
                  $('#aet4').html("Correct!");
                  break;
        }
    });
});


</script>


<h2><u> PART II: Compare images </u></h2>

We will now provide you with several pairs of images. In each pair, one image has more number of lymphocytes than the other. Your task is to figure out which one has <b>higher number of lymphocytes</b> among the two. We will let you know if you get an answer wrong. Please go through the <a href="#instr1">instructions</a> again if you are unable to answer correctly.

<h3>Example task 1</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/93.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/35.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td><b>Image 1</b></td>
    <td><b>Image 2</b></td>
  </tr>
</table>
<ol>
  <li>
    Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
    <br>
      <input type="radio" name="et1" value="1"> Image 1
      <input type="radio" name="et1" value="2"> Image 2
      <div id='aet1'></div>
  </li>
</ol>

<h3>Example task 2</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/93.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/22.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td><b>Image 1</b></td>
    <td><b>Image 2</b></td>
  </tr>
</table>
<ol>
  <li>
    Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
    <br>
      <input type="radio" name="et2" value="1"> Image 1
      <input type="radio" name="et2" value="2"> Image 2
      <div id='aet2'></div>
  </li>
</ol>

<h3>Example task 3</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/93.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/71.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td><b>Image 1</b></td>
    <td><b>Image 2</b></td>
  </tr>
</table>
<ol>
  <li>
    Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
    <br>
      <input type="radio" name="et3" value="1"> Image 1
      <input type="radio" name="et3" value="2"> Image 2
      <div id='aet3'></div>
  </li>
</ol>

<h3>Example task 4</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/93.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/16.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td><b>Image 1</b></td>
    <td><b>Image 2</b></td>
  </tr>
</table>
<ol>
  <li>
    Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
    <br>
      <input type="radio" name="et4" value="1"> Image 1
      <input type="radio" name="et4" value="2"> Image 2
      <div id='aet4'></div>
  </li>
</ol>


<h2><u> PART III </u></h2>
<h3>Final Tasks: Identify image type</h3>

The images till now were used to give you a good introduction to the work we want you to do and to familiarize yourself with how a gland and a lymphocyte looks, and how to identify which cell has higher or lesser number of lymphocytes. Now, use the information that we have given you to do the actual task at hand. There will be 2 images in each task, choose which image has the higher number of lymphocytes. God luck!

"""
randCellsNew = [""] * len(arrayOfCells) * (len(pivots)-1)
randPivsNew = [""] * len(arrayOfCells) * (len(pivots)-1)
counter = 0;
index = 0;

for i in range(0,len(randCells)):
	if (randCells[i] == 'g47' and randPivs[i] == 'r35'):
		counter = counter + 1;
	elif (randCells[i] == 'r21' and randPivs[i] == 'g16'):
		counter = counter + 1;
	elif (randCells[i] == 'g9' and randPivs[i] == 'r22'):
		counter = counter + 1;
	elif (randCells[i] == 'r89' and randPivs[i] == 'r35'):
		counter = counter + 1;
	if (counter == 0):
		randCellsNew[index] = randCells[i]
		randPivsNew[index] = randPivs[i]
		index+=1;
	counter = 0;

								
outer_index = 0
#horizontal shuffle
horShuffle = randint(1,2)


for i in range(0,len(randCellsNew)):
	question_html_value += "<h3>Task %s</h3>\n" % str(i+1)
	if horShuffle == 1: 
		question_html_value += """
		<table> <tr>    <td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/%s/%s.png\" height=\"500\" width=\"500\" /></td>     <td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/%s/%s.png\" height=\"500\" width=\"500\" /></td>   </tr>  <tr align=\"center\">    <td><b>Image 1</b></td>     <td><b>Image 2</b></td> </tr> </table>\n<ol><li>Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?<br> <label><input type="radio" value="0" name="i%sp%s">Image 1</label> <label><input type="radio" value="1" name="i%sp%s">Image 2</label></li></ol>
		"""% (str(randCellsNew[i][:1]),str(randCellsNew[i][1:]),str(randPivsNew[i][:1]),str(randPivsNew[i][1:]),str(randCellsNew[i]),str(randPivsNew[i]),str(randCellsNew[i]),str(randPivsNew[i]) )

	elif horShuffle == 2:
		question_html_value += """
		<table> <tr>    <td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/%s/%s.png\" height=\"500\" width=\"500\" /></td>     <td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/%s/%s.png\" height=\"500\" width=\"500\" /></td>   </tr>  <tr align=\"center\">    <td><b>Image 1</b></td>     <td><b>Image 2</b></td> </tr> </table>\n<ol><li>Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?<br> <label><input type="radio" value="0" name="i%sp%s">Image 1</label> <label><input type="radio" value="1" name="i%sp%s">Image 2</label></li></ol>
		"""% (str(randPivsNew[i][:1]),str(randPivsNew[i][1:]),str(randCellsNew[i][:1]),str(randCellsNew[i][1:]),str(randCellsNew[i]),str(randPivsNew[i]),str(randCellsNew[i]),str(randPivsNew[i]) )



ending = """
<h2>Please tell us any comments about the test to improve it or how you felt about it. Thanks!</h2>
<input type="text" id="text" name="text_name" style="width: 300px;height: 50px;" />



<!-- HTML to handle submitting the HIT -->
<p><input type='submit' id='submitButton' value='Submit' /></p> </form>
<script language='Javascript'>turkSetAssignmentID();</script>
</body>
</html>
"""
question_html_value += ending





#print "Your HIT has been created. You can see it at this link:"
#print "https://mturk.com/mturk/preview?groupId={}".format(hit_type_id)
print "Your HIT ID is: {}".format(hit_id)

