import boto
import boto3
from boto.mturk.connection import MTurkConnection
from boto.mturk.question import HTMLQuestion
from boto.mturk.qualification import Qualifications,Requirement,PercentAssignmentsApprovedRequirement,NumberHitsApprovedRequirement

"""
This code has two parts: We send out the screener, and the second part is to register the results from the HIT to be automatically sent to a queue which then does some processing to review the scores off the answers. The bulk of this code is a html code that will be appended. The function that does the signing up to a queue is called register_qual_hit.

To change the credentials, Just write your accessID and secret key in the space below. To switch between using the sandbox and the normal mturk, switch out the client= TEST_CLIENT line, in line 32 to client = MTURK_CLIENT
"""

ACCESS_ID = "AKIAJ63RZNZ7R5W57UEQ"
SECRET_KEY = "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"
HOST_SANDBOX = 'mechanicalturk.sandbox.amazonaws.com'
HOST = 'mechanicalturk.amazonaws.com'
 
mtc_test = MTurkConnection(aws_access_key_id=ACCESS_ID,
                      aws_secret_access_key=SECRET_KEY,
                      host=HOST_SANDBOX)
                      
mtc = MTurkConnection(aws_access_key_id=ACCESS_ID,
                      aws_secret_access_key=SECRET_KEY,
                      host=HOST)

pivots = ["r35","r22","g71","g16"]

question_html_value = """
<html>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<script src='https://s3.amazonaws.com/mturk-public/externalHIT_v1.js' type='text/javascript'></script>
</head>
<body>
<!-- HTML to handle creating the HIT form -->
<form name='mturk_form' method='post' id='mturk_form' action='https://www.mturk.com/mturk/externalSubmit'>
<input type='hidden' value='' name='assignmentId' id='assignmentId'/>

<!-- ############################################INTRO TASKS####################################################### -->

<h2>Instructions</h2>
<p>This is a qualifying test where you will learn how to identify two tissue image components, and compare images based on one such component.</p>
<!-- <p>There are three main tasks we expect you to do for this HIT:</p>
<p>1) Identify different parts of tissue: this part teaches you how glands and lymphocytes look like. If you choose the wrong answer, the right answer will be explained to you.</p>
<p>2) Given two cells, identify which cell has the higher number of lymphocytes(With answers): If you choose the wrong answer, why it is wrong will be explained to you.</p>
<p>3) Given two cells, identify which cell has the higher number of lymphocytes(Without answers): This is the main task of this assignment. Based on the brief introduction to the features we have given you, tell us which cell has the higher number of lymphocytes.</p> -->

<!-- <h3> Qualifying Test </h3> -->
<h2> <u>LEARNING TASK</u> </h2>
<h3>Goal: Compare tissue images</h3>
<ul>
  <li>We will give you various tissue images, and your task is to compare these images based on the number of Lymphocytes.</li>
  <li>We will explain how to go about this.</li>
  <li>We know that this task is not easy. However, we expect you to read the instructions carefully and answer to the best of your abilities.</li>
</ul>

<h3>Tissue image components</h3>

We will first learn how to identify the two critical components of a tissue image. We will then see how to compare these tissue images based on the number of Lymphocytes. The two critical components of a tissue image  are:

<ol>
  <li>Gland</li>
  <li>Lymphocytes</li>
</ol>

Your first task is the following: given a tissue image, you have to identify these two components. A typical image you will see, along with its annotated version is provided below:
<br><br>
<div id="instr1"></div>
<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/24.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/misc/exp3eg1.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td>Original image</td>
    <td>Annotated image</td>
  </tr>
</table>
<br><br>

We provide a verbal description for identifying the two components, <i>please read them carefully</i>. Try to match the annotated image provided above with the following description:

<ol>
  <li>Gland
  <ul style="list-style-type:circle">
    <li>Glands are round to oval shaped in general. Their shape can be slightly disfigured sometimes.</li>
    <li>Glands are proportionally large, and they are typically seperated from each other.</li>
    <li>They often have a <i>white</i> cavity inside them. Note that this need not be the case everytime.</li>
    <li>Their borders are <i>purple</i> in color.</li>
  </ul>
  </li>
  <li>Lymphocytes
  <ul style="list-style-type:circle">
    <li>Lymphocytes are small spots that are <i>dark purple</i> in color.</li>
    <li>They are scattered randomly in the tissue image.</li>
    <li>Lymphocytes do <b>not</b> appear inside a gland.</li>
  </ul>
  </li>
</ol>
<h2> <u>PART I: IDENTIFYING TISSUE COMPONENTS</u></h2>
<h3>Example tasks</h3>

Your task is to correctly identify the components that we point out to in the following tissue images. We will let you know if you get an answer wrong; you are then expected to go through the <a href="#instr1">instructions</a> again if you are unable to identify the component correctly. Note that there are three images and the <b>third</b> image here will be part of the qualifying test. So, the first two are designed to help you.

<h3>Image 1</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/misc/exp3img1.png" height="500" width="500" />
    </td>
    <td>
      What does region 1 represent?  
          <input type="radio" name="i1q1" value="1"> Gland
          <input type="radio" name="i1q1" value="2"> Lymphocytes
          <div id='ai1q1'></div>
          What does region 2 represent?   
          <input type="radio" name="i1q2" value="1"> Gland
          <input type="radio" name="i1q2" value="2"> Lymphocytes
          <div id='ai1q2'></div>
          What does region 3 represent?  
          <input type="radio" name="i1q3" value="1"> Gland
          <input type="radio" name="i1q3" value="2"> Lymphocytes
          <div id='ai1q3'></div>
          What does region 4 represent?   
          <input type="radio" name="i1q4" value="1"> Gland
          <input type="radio" name="i1q4" value="2"> Lymphocytes
          <div id='ai1q4'></div>
    </td>
  </tr>
</table>

<h3>Image 2</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/misc/exp3img2.png" height="500" width="500" />
    </td>
    <td>
      What does region 1 represent?  
          <input type="radio" name="i2q1" value="1"> Gland
          <input type="radio" name="i2q1" value="2"> Lymphocytes
          <div id='ai2q1'></div>
          What does region 2 represent?   
          <input type="radio" name="i2q2" value="1"> Gland
          <input type="radio" name="i2q2" value="2"> Lymphocytes
          <div id='ai2q2'></div>
          What does region 3 represent?  
          <input type="radio" name="i2q3" value="1"> Gland
          <input type="radio" name="i2q3" value="2"> Lymphocytes
          <div id='ai2q3'></div>
          What does region 4 represent?   
          <input type="radio" name="i2q4" value="1"> Gland
          <input type="radio" name="i2q4" value="2"> Lymphocytes
          <div id='ai2q4'></div>
    </td>
  </tr>
</table>

<h3>Test task</h3>

<h3>Image 3</h3>
<p>This image is a test so answer the questions carefully.</p>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/misc/exp3img3.png" height="500" width="500" />
    </td>
    <td>
      What does region 1 represent?  
          <input type="radio" name="i3q1" value="1"> Gland
          <input type="radio" name="i3q1" value="2"> Lymphocytes
          <div id='ai3q1'></div>
          What does region 2 represent?   
          <input type="radio" name="i3q2" value="1"> Gland
          <input type="radio" name="i3q2" value="2"> Lymphocytes
          <div id='ai3q2'></div>
          What does region 3 represent?  
          <input type="radio" name="i3q3" value="1"> Gland
          <input type="radio" name="i3q3" value="2"> Lymphocytes
          <div id='ai3q3'></div>
          What does region 4 represent?   
          <input type="radio" name="i3q4" value="1"> Gland
          <input type="radio" name="i3q4" value="2"> Lymphocytes
          <div id='ai3q4'></div>
    </td>
  </tr>
</table>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script type='text/javascript'>


$(document).ready(function(){
    $('input[name=i1q1]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai1q1').html("Correct!");
                  break;
            case '2':
                  $('#ai1q1').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i1q2]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai1q2').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai1q2').html("Correct!");
                  break;
        }
    });
    $('input[name=i1q3]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai1q3').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai1q3').html("Correct!");
                  break;
        }
    });
    $('input[name=i1q4]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai1q4').html("Correct!");
                  break;
            case '2':
                  $('#ai1q4').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i2q1]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai2q1').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai2q1').html("Correct!");
                  break;
        }
    });
    $('input[name=i2q2]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai2q2').html("Correct!");
                  break;
            case '2':
                  $('#ai2q2').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i2q3]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai2q3').html("Correct!");
                  break;
            case '2':
                  $('#ai2q3').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i2q4]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai2q4').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai2q4').html("Correct!");
                  break;
        }
    });
    /*
    $('input[name=i3q1]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai3q1').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai3q1').html("Correct!");
                  break;
        }
    });

    $('input[name=i3q2]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai3q2').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai3q2').html("Correct!");
                  break;
        }
    });
    $('input[name=i3q3]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai3q3').html("Correct!");
                  break;
            case '2':
                  $('#ai3q3').html("Wrong answer. Try again!");
                  break;
        }
    });
    $('input[name=i3q4]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#ai3q4').html("Wrong answer. Try again!");
                  break;
            case '2':
                  $('#ai3q4').html("Correct!");
                  break;
        }
    });
    */

    $('input[name=et1]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#aet1').html("Correct!");
                  break;
            case '2':
                  $('#aet1').html("Wrong answer. Try again!<br>Hint: The glands in image 1 look a bit disfigured don't they?");
                  break;
        }
    });

    $('input[name=et2]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#aet2').html("Wrong answer. Try again!<br>Hint: Notice the large number of lymphocytes in image 2. The glands in image 2 look pretty small too!");
                  break;
            case '2':
                  $('#aet2').html("Correct!");
                  break;
        }
    });

    $('input[name=et3]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#aet3').html("Correct!");
                  break;
            case '2':
                  $('#aet3').html("Wrong answer. Try again!<br>Hint: The glands in image 2 seem bigger and well structured compared to the ones in image 1!");
                  break;
        }
    });

    $('input[name=et4]').on('change', function(){
    var n = $(this).val();
    switch(n)
    {
            case '1':
                  $('#aet4').html("Wrong answer. Try again!<br>Hint: So many lymphocytes in image 2! Certainly can't be a good sign.");
                  break;
            case '2':
                  $('#aet4').html("Correct!");
                  break;
        }
    });
});


</script>

<h2><u> PART II: Compare tissue images</u></h2>
<h3>Example tasks</h3>

We will now provide you with several pairs of images. In each pair, one image has more number of lymphocytes than the other. Your task is to figure out which one has <b>higher number of lymphocytes</b> among the two. We will let you know if you get an answer wrong. Please go through the <a href="#instr1">instructions</a> again if you are unable to answer correctly.

<h3>Example task 1</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/93.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/35.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td><b>Image 1</b></td>
    <td><b>Image 2</b></td>
  </tr>
</table>
<ol>
  <li>
    Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
    <br>
      <input type="radio" name="et1" value="1"> Image 1
      <input type="radio" name="et1" value="2"> Image 2
      <div id='aet1'></div>
  </li>
</ol>

<h3>Example task 2</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/93.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/22.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td><b>Image 1</b></td>
    <td><b>Image 2</b></td>
  </tr>
</table>
<ol>
  <li>
    Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
    <br>
      <input type="radio" name="et2" value="1"> Image 1
      <input type="radio" name="et2" value="2"> Image 2
      <div id='aet2'></div>
  </li>
</ol>

<h3>Example task 3</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/93.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/71.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td><b>Image 1</b></td>
    <td><b>Image 2</b></td>
  </tr>
</table>
<ol>
  <li>
    Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
    <br>
      <input type="radio" name="et3" value="1"> Image 1
      <input type="radio" name="et3" value="2"> Image 2
      <div id='aet3'></div>
  </li>
</ol>

<h3>Example task 4</h3>

<table>
  <tr>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/93.png" height="500" width="500" /></td>
    <td><img alt="Tissue image" src="http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/16.png" height="500" width="500" /></td>
  </tr>
  <tr align="center">
    <td><b>Image 1</b></td>
    <td><b>Image 2</b></td>
  </tr>
</table>
<ol>
  <li>
    Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
    <br>
      <input type="radio" name="et4" value="1"> Image 1
      <input type="radio" name="et4" value="2"> Image 2
      <div id='aet4'></div>
  </li>
</ol>


<!--
TESTS START HERE
 -->
 <h3>TESTS</h3>
 <h3>Test 1</h3>


<table>
	<tr>
		<td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/35.png\" height=\"500\" width=\"500\" /></td>     
		<td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/47.png\" height=\"500\" width=\"500\" /></td>   
	</tr>  
		<tr align=\"center\">    
		<td><b>Image 1</b></td>     
		<td><b>Image 2</b></td> 
		</tr> 
</table>\n
		
		<ol><li>Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
		<br> <label><input type="radio" value="0" name="ir21pg16">Image 1</label> 
		<label><input type="radio" value="1" name="ir21pg16">Image 2</label>
		</li></ol>




<h3>Test 2</h3>



<table>
	<tr>
		<td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/16.png\" height=\"500\" width=\"500\" /></td>     
		<td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/21.png\" height=\"500\" width=\"500\" /></td>   </tr>  <tr align=\"center\">    
		<td><b>Image 1</b></td>     
		<td><b>Image 2</b></td> 
	</tr> 
	</table>\n
		<ol><li>Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?
		<br> 
		<label><input type="radio" value="0" name="ig47pr35">Image 1</label> 
		<label><input type="radio" value="1" name="ig47pr35">Image 2</label>
		</li></ol>


<h3>Test 3</h3>



<table>
	<tr>
		<td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/22.png\" height=\"500\" width=\"500\" /></td>     
		<td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/g/9.png\" height=\"500\" width=\"500\" /></td>   </tr>  <tr align=\"center\">    
		<td><b>Image 1</b></td>     
		<td><b>Image 2</b></td> 
		</tr> 
		</table>\n
		<ol><li>Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?<br> 
		<label><input type="radio" value="0" name="ig47pr22">Image 1</label> 
		<label><input type="radio" value="1" name="ig47pr22">Image 2</label>
		</li></ol>


<h3>Test 4</h3>



<table>
	<tr>
		<td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/89.png\" height=\"500\" width=\"500\" /></td>     
		<td><img src=\"http://web.engr.illinois.edu/~vvnktrm2/research/crowd-cancer/images/r/35.png\" height=\"500\" width=\"500\" /></td>   
		</tr>  <tr align=\"center\">    
		<td><b>Image 1</b></td>     
		<td><b>Image 2</b></td> 
		</tr> 
		</table>\n
		<ol><li>Which one among Image 1 and Image 2 has <i>higher</i> number of lymphocytes?<br> 
		<label><input type="radio" value="0" name="ir21pg71">Image 1</label> 
		<label><input type="radio" value="1" name="ir21pg71">Image 2</label>
		</li></ol>



<!-- HTML to handle submitting the HIT -->
<p><input type='submit' id='submitButton' value='Submit' /></p> </form>
<script language='Javascript'>turkSetAssignmentID();</script>
</body>
</html>

"""


REGION = "us-east-1"
MTURK_CLIENT = boto3.client('mturk', REGION, aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=SECRET_KEY)
MTURK_TEST_CLIENT = boto3.client('mturk', REGION, aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=SECRET_KEY, endpoint_url='https://mturk-requester-sandbox.us-east-1.amazonaws.com')
SQS_URL = "https://sqs.us-east-1.amazonaws.com/807541732347/testing_queue_2"
SNS_ARN = "arn:aws:sns:us-east-1:807541732347:test_sns_2"

def ExternalQuestion(ExternalUrl, FrameHeight, xmlns="http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2006-07-14/ExternalQuestion.xsd"):
    encoded_string = '<ExternalQuestion xmlns="' + xmlns + '"><ExternalURL>' + str(ExternalUrl) + '</ExternalURL><FrameHeight>' + str(FrameHeight) + '</FrameHeight></ExternalQuestion>'
    return encoded_string


def HTMLQuestion(content,FrameHeight, xmlns="http://mechanicalturk.amazonaws.com/AWSMechanicalTurkDataSchemas/2011-11-11/HTMLQuestion.xsd"):
    encoded_string = '<HTMLQuestion xmlns="' + xmlns + '"><HTMLContent><![CDATA[<!DOCTYPE html>'+str(content)+']]></HTMLContent><FrameHeight>' + str(FrameHeight) + '</FrameHeight></HTMLQuestion>'
    return encoded_string

html_question = HTMLQuestion(question_html_value, 500)


def post_qual_hit(url, TestBool=False, ReverseEffectAfter=False):
    if TestBool:
        client = MTURK_TEST_CLIENT
    else:
        client = MTURK_CLIENT

    question_form = ExternalQuestion(url, 800)  
    html_Q = HTMLQuestion(question_html_value, 500)
    response = client.create_hit(
        MaxAssignments=20,
        AutoApprovalDelayInSeconds=5,
        LifetimeInSeconds=600000,
        AssignmentDurationInSeconds=600,
        Reward='0.15',
        Title='Screener for prostate cancer HIT',
        Keywords='test, screener',
        Description='This is a screener HIT that will allow you to do a HIT about prostate cancer.',
        Question=html_question
    )
    hit_type_id = response['HIT']['HITTypeId']
    hit_id = response['HIT']['HITId']
    print "hit type id "+hit_type_id
    print "hit id "+hit_id
    if ReverseEffectAfter:
        client.update_expiration_for_hit(HITId=hit_id, ExpireAt=(datetime.datetime.now() - datetime.timedelta(days=2))) 
        client.delete_hit(HITId=hit_id)
    else:
        return response

def register_qual_hit(HITTypeId, TestBool=False, ReverseEffectAfter=False):
    if TestBool:
        client = MTURK_TEST_CLIENT
    else:
        client = MTURK_CLIENT
    client.update_notification_settings(
        HITTypeId=HITTypeId,
        Notification={
            'Destination': SQS_URL,
            'Transport': 'SQS',
            'Version': '2006-05-05',
            'EventTypes': ['AssignmentSubmitted'],
        },
        Active=True
    )
    if ReverseEffectAfter:
        client.update_notification_settings(
            HITTypeId=HITTypeId,
            Notification={
                'Destination': SQS_URL,
                'Transport': 'SQS',
                'Version': '2006-05-05',
                'EventTypes': ['AssignmentSubmitted'],
            },
            Active=False
        )    

hit_type_id = post_qual_hit("https://salty-wave-21673.herokuapp.com/", ReverseEffectAfter=False)['HIT']['HITTypeId']
print "hit type id "+hit_type_id
register_qual_hit(hit_type_id, ReverseEffectAfter=False)

