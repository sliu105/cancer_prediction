import pandas as pd

FILE_LOCATION = "answers_new.csv"

"""
Applies a python lambda to each row of a dataframe 
"""

def apply_by_row_and_output_as_list(df, lambda_fx):
    df['json'] = df.apply(lambda_fx, axis=1)
    return df['json'].tolist()

if __name__ == "__main__":
	pd.read_csv(FILE_LOCATION, sep = ",")