# viewer/

Viewer for viewing the results of experiments. Experiment.jsons need to be copied in if you'd like to browse them, see experimen0.json as an example. Use a basic webserver to serve the folder and the popups will guide you to everything else.

# experiments/

Folder containing self-contained experiments. Each should contain at least an answers.json and a responses.csv .

# accuracy_test.py

Checks answers against an answer key. 

## Sample usage

python accuracy_test.py experiments/experiment0/answers.json experiments/experiment0/responses.csv -o experiments/experiment0/experiment0.json

# analyse_responses.py

Basic code containing functions for looking over a csv.
