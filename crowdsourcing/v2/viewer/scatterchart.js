
// root = JSON.parse( "data.json" );

// Surprisingly required
function roundNumber(num, scale) {
  if(!("" + num).includes("e")) {
    return +(Math.round(num + "e+" + scale)  + "e-" + scale);
  } else {
    var arr = ("" + num).split("e");
    var sig = ""
    if(+arr[1] + scale > 0) {
      sig = "+";
    }
    return +(Math.round(+arr[0] + "e" + sig + (+arr[1] + scale)) + "e-" + scale);
  }
}

// The following below is a bunch of d3 nonsense
function scatterplot(originalData){
  // console.log(json);
  var numPoints = originalData.length;

  // originalData = [0.6428571428571429, 0.8214285714285714, 0.7857142857142857, 0.6428571428571429, 0.75, 0.8214285714285714, 0.7857142857142857, 0.75, 0.8214285714285714, 0.7142857142857143, 0.7142857142857143, 0.6785714285714286, 0.7142857142857143, 0.7857142857142857, 0.7142857142857143];

  // Data format must be array of tuples (arrays of two)
  var data = []
  for (var i = 0; i < numPoints; i++){
    data.push([originalData[i], Math.random()])
  }

     
  var margin = {top: 20, right: 15, bottom: 60, left: 60}
    , width = 960 - margin.left - margin.right
    , height = 200 - margin.top - margin.bottom;

  var x = d3.scaleLinear()
            .domain([0, 1])
            .range([ 0, width ]);

  var y = d3.scaleLinear()
          .domain([0, 2])
          .range([ height, 0 ]);

  var chart = d3.select('#scatterplot')
    .append('svg:svg')
    .attr('width', width + margin.right + margin.left)
    .attr('height', height + margin.top + margin.bottom)
    .attr('class', 'chart')

  var main = chart.append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
    .attr('width', width)
    .attr('height', height)
    .attr('class', 'main')   
      
  // draw the x axis
  var xAxis = d3.axisBottom()
    .scale(x);

  main.append('g')
    .attr('transform', 'translate(0,' + height + ')')
    .attr('class', 'main axis date')
    .call(xAxis);

  // draw the y axis
  var yAxis = d3.axisLeft()
    .scale(y);


  var g = main.append("svg:g"); 

  var tool_tip = d3.tip()
        .attr("class", "d3-tip")
        .offset([-8, 0])
        .html(function(d,i) { return "Accuracy: " + roundNumber(d[0]*100, 2) + "%"; });


  // parseFloat(d[0]).toFixed(2);
  chart.call(tool_tip);

  g.selectAll("scatter-dots")
    .data(data)
    .enter().append("svg:circle")
        .attr("cx", function (d,i) { return x(d[0]); } )
        .attr("cy", function (d) { return y(d[1]); } )
        .attr("r", 3)
        .on('mouseover', tool_tip.show)
        .on('mouseout', tool_tip.hide);
}
