

// //Code to extract url paraemters
// //Credit goes to
// //http://stackoverflow.com/questions/19491336/get-url-parameter-jquery-or-how-to-get-query-string-values-in-js
var queryDict = {};
location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]})

// //Using extracted url parameters to populate form


if (!("s" in queryDict)){
	$( function() {

	    $( "#dialog" ).dialog({
	      resizable: false,
	      height: "auto",
	      width: 400,
	      modal: true,
	      buttons: {
	        "Use this experiment": function() {
	          $( this ).dialog( "close" );
	          var selected = $("select#dataset option:checked").val();
	          window.location.search += '&s=' + selected;
	        },
	        Cancel: function() {
	          $( this ).dialog( "close" );
	        }
	      }
	    });


	    $( "#dataset" ).selectmenu();


  } );
}


