import argparse
import json
from pprint import pprint
import sys
import os
import csv
import collections
import io


# Sample run
#python accuracy_test.py experiments/experiment0/answers.json experiments/experiment0/responses.csv -o experiments/experiment0/experiment0.json



# Spooky unicode hacks for json to dict stuff
def byteify(input):
    if isinstance(input, dict):
        return {byteify(key): byteify(value)
                for key, value in input.iteritems()}
    elif isinstance(input, list):
        return [byteify(element) for element in input]
    elif isinstance(input, unicode):
        return input.encode('utf-8')
    else:
        return input

# def var2dict(tuple_set):
#     dictionary = {}
#     for i in tuple_set:
#         fruitdict[i] = locals()[i]
#     return dictionary

# Takes a list of dicts and returns a dictionary with the sums. Must have addition defined for elements
def sum_dict_list(dict_list):
    counter = collections.Counter()
    for d in dict_list: 
        counter.update(d)
    return dict(counter)

# Evaluates a response dict against an answer key dict, returns basic stats and can return individual results.
def evaluate_response_accuracy(response_dict, answer_key_dict, results_ret=True, acc_ret=True):
    total_correct = 0
    total_wrong = 0
    total_responses = len(response_dict)

    result_dict = {}
    for key in answer_dict.keys():
        if key in response_dict:        
            if response_dict[key] == answer_key_dict[key]:
                result_dict[key] = 1
                total_correct += 1
            else:
                result_dict[key] = 0
                total_wrong += 1

    return_dict = {"total_correct": total_correct, "total_wrong": total_wrong, "total_responses": total_responses}

    acuracy = None 
    ret_result_dict = None

    if acc_ret:
        accuracy = float(total_correct) / float(total_responses)

    if results_ret:
        ret_result_dict = result_dict

    return return_dict, result_dict, accuracy  


# Evaluates a set of response dicts against an answer key, returns nasic stats and individual results
def evaluate_response_set_accuracy(response_dict_list, answer_key_dict):
    aggregate_arr_stats_dicts = []
    aggregate_arr_result_dicts = []
    aggregate_arr_worker_accuracy = []

    for response_dict in response_dict_list:
        stats, result_dict, accuracy = evaluate_response_accuracy(response_dict, answer_key_dict, results_ret=True, acc_ret=True)
        aggregate_arr_stats_dicts.append(stats)
        aggregate_arr_result_dicts.append(result_dict)
        aggregate_arr_worker_accuracy.append(accuracy)

    return {"aggregate_stats": sum_dict_list(aggregate_arr_stats_dicts),
            "aggregate_by_q": sum_dict_list(aggregate_arr_result_dicts),
             "aggregate_by_worker": aggregate_arr_worker_accuracy,
             "answer_key": answer_key_dict}

# Loads a json file as a dict
def jsonfile2dict(file_name):
    json_data = open(file_name).read()
    data = json.loads(json_data)
    return byteify(data)

# Loads a csv as a list of dicts
def csv_to_dict(file_name):
    input_file = csv.DictReader(open(file_name))
    dict_list = []
    for row in input_file:
        dict_list.append(row)
    return dict_list

def json2file(data, file_name):
    with open(file_name, 'w') as f:
        f.write(str(data).replace("'", '"'))



# def query_var_over_responses(response_dict_list, answer_dict, variable):

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Calculate crowd-cancer accuracy given an answer.json and responses.csv.')
    
    parser.add_argument('ans_loc', metavar='answer_json_location', type=str, nargs=1,
                        help='Location of answer.json')
    parser.add_argument('resp_loc', metavar='response_csv_location', type=str, nargs=1, 
                        help='Location of responses.csv')

    parser.add_argument('-o', metavar='--output', type=str, nargs=1, 
                        help='Location of output file (for viewer)', default="")

    args = parser.parse_args()

    answer_dict = jsonfile2dict(args.ans_loc[0])
    response_dict_list = csv_to_dict(args.resp_loc[0])
    output_file = args.o[0]


    result = evaluate_response_set_accuracy(response_dict_list, answer_dict)

    print result


    if output_file != "":
        json2file(result, output_file)
    # print evaluate_accuracy(response_dict, answer_dict)

