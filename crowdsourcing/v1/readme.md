# answers.txt

Raw output of get_responses.py (Soumitri era). Is unparsed and needs to be converted into csv.

# early_data_analysis.py

Early data consistency analysis from Akash and Vipul.

# get_responses.py

Soumitri's old code for grabbing responses. CONTAINS CREDENTIALS.

# screener.py

Soumitri's old code for generating the HTML and then posting the HIT that is used for screening workers for a qualification. CONTAINS CREDENTIALS.

# test.py

Soumitri's old code for generating the HTML and then posting the HIT that is used for evaluating a worker. CONTAINS CREDENTIALS.


