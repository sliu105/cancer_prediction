import numpy
import random
from collections import defaultdict
from matplotlib import pyplot as plt

pivots = ['g16', 'g71', 'r22', 'r35']
images = ['r89', 'r21', 'g47', 'g9']
# images = ['g47']
gt = {'r89': 2, 'r21': 2, 'g47': 0, 'g9': 1}


def read_responses_and_stats():
    estimates = defaultdict(dict)
    is_bad = dict()  # flag to eliminate bad workers
    labels = {}
    dis_vals = []
    worker_responses_in_order = defaultdict(list)
    responses_by_worker = defaultdict(lambda: defaultdict(dict))
    responses_on_image = defaultdict(lambda: defaultdict(dict))

    # variance
    # image-wise worker performance

    for line in open('responses.txt', 'r'):
        line = line.strip()
        parts = line.split(' ')

        if len(parts) < 1:
            continue
        
        if parts[0] == 'Answers':
            print '----------------------------'
            worker = parts[-1]
            print 'Worker: ', worker
            num_answers = 0
            is_bad[worker] = False
        elif parts[0][:9] == 'text_name':
            if len(labels) == 4 * len(images):
                disagreements = 0
                for image in images:
                    if not (labels[(image, pivots[0])] <= labels[(image, pivots[1])] <= labels[(image, pivots[2])] <= labels[(image, pivots[3])]):
                        disagreements += 1

                    score = {}
                    for pos in range(0, 5):
                        score[pos] = 0
                        for i, pivot in enumerate(pivots):
                            if pos <= i and labels[(image, pivot)] == 1:
                                score[pos] += 1
                            elif pos > i and labels[(image, pivot)] == 0:
                                score[pos] += 1
                            # else:
                            #     score[pos] += -1

                    # score = {}
                    # for pos in range(0, 5):
                    #     score[pos] = 0
                    #     for i, pivot in enumerate(pivots):
                    #         if pos > i and labels[(image, pivot)] == 1:
                    #             score[pos] -= abs(i + 1 - pos)
                    #         elif pos <= i and labels[(image, pivot)] == 0:
                    #             score[pos] -= abs(i + 1 - pos)

                    max_score = max(score.values())
                    candidate_pos = [pos for pos in range(0,5) if score[pos] == max_score]
                    final_pos = sum(candidate_pos)/len(candidate_pos)
                    if True:  # max_score >= 3:
                        estimates[image][worker] = final_pos


                    # if score.values() != range(5) and score.values() != range(4, -1, -1):
                    #     # check if worker has not said all 0 or all 1
                    #     is_bad[worker] = False

                    print 'Image: {} ({})'.format(image, gt[image])
                    print 'Full score: {}'.format(score)

                # print disagreements
                dis_vals.append(disagreements)

                # if disagreements == 0:
                #     for image in images:
                #         for pivot in pivots:
                #             print image, pivot, labels[(image, pivot)]

            labels.clear()
        elif len(parts[0]) > 6:
            for image in images:
                for pivot in pivots:
                    if image in parts[0] and pivot in parts[0] and parts[0][0] == 'i':
                        labels[(image, pivot)] = int(parts[0][-1])
                        worker_responses_in_order[worker].append(int(parts[0][-1]))
                        responses_by_worker[worker][image][pivot] = int(parts[0][-1])
                        responses_on_image[image][worker][pivot] = int(parts[0][-1])
                    elif image in parts[0] and pivot in parts[0] and parts[0][0] == 'p':
                        labels[(image, pivot)] = 1 - int(parts[0][-1])
                        worker_responses_in_order[worker].append(1 - int(parts[0][-1]))
                        responses_by_worker[worker][image][pivot] = 1 - int(parts[0][-1])
                        responses_on_image[image][worker][pivot] = 1 - int(parts[0][-1])

    for worker in worker_responses_in_order:
        if sum(worker_responses_in_order[worker]) > 0.7 * 4 * len(images) or \
                sum(worker_responses_in_order[worker]) < 0.3 * 4 * len(images):
            # workers are bad if more than 90% 1 or 0 responses
            is_bad[worker] = True

    print dis_vals
    # print estimates
    print '==========================================='
    final_estimates = defaultdict(list)  # final estimates for images
    for image in estimates:
        print 'Image: {}, GT: {}'.format(image, gt[image])
        for worker in estimates[image]:
            print 'Worker: {}, is_bad: {}'.format(worker, is_bad[worker]),
            print 'Estimate: {}'.format(estimates[image][worker])
            if not is_bad[worker]:
                final_estimates[image].append(estimates[image][worker])

    print '==========================================='
    for image in estimates:
        print 'Image: {}, GT: {}, final estimates: {}, avg: {}, median: {}'.format(
            image,
            gt[image],
            final_estimates[image],
            numpy.mean(final_estimates[image]),
            numpy.median(final_estimates[image]),
        )
    print '==========================================='

    return responses_by_worker, responses_on_image


def calculate_error(error_metric, worker_label, image):
    # calculate some notion of accuracy against ground truth
    if error_metric == 'question_distance':
        # error / accuracy calculated for each image-pivot question
        # error as a distance measure
        return None
    elif error_metric == 'question_binary':
        # error / accuracy calculated for each image-pivot question
        # error as a right or wrong measure
        return None
    elif error_metric == 'image_distance':
        # error / accuracy calculated for each image
        # based on consolidated labels
        # error as a distance measure
        return abs(worker_label - gt[image])
    elif error_metric == 'image_binary':
        # error / accuracy calculated for each image
        # based on consolidated labels
        # error as a right or wrong measure
        return None
    else:
        raise NotImplementedError


def consolidated_worker_response(consolidation_method, worker_response):
    # extract some signal / final aggregate label
    # from a given worker
    if consolidation_method == 'min_dist_inconsistent':
        dist_inconsistent = defaultdict(int)
        for response in range(len(pivots)+1):
            for i, pivot in enumerate(pivots):
                if (response <= i and worker_response[pivot] == 1):
                    dist_inconsistent[response] += i - response + 1
                elif (response > i and worker_response[pivot] == 0):
                    dist_inconsistent[response] += response - i
        # print dist_inconsistent
        return min(dist_inconsistent.keys(), key=(lambda x: dist_inconsistent[x]))
    elif consolidation_method == 'min_num_inconsistent':
        num_inconsistent = defaultdict(int)
        for response in range(len(pivots)+1):
            for i, pivot in enumerate(pivots):
                if (response <= i and worker_response[pivot] == 1):
                    num_inconsistent[response] += 1
                elif (response > i and worker_response[pivot] == 0):
                    num_inconsistent[response] += 1
        return min(num_inconsistent.keys(), key=(lambda x: num_inconsistent[x]))


def calculate_inconsistency(consistency_metric, worker_response, label=None):
    # calculate some notion of worker internal consistency
    # returns worker consistency score
    if consistency_metric == 'dist_inconsistent_against_label':
        dist_inconsistent = 0
        for i, pivot in enumerate(pivots):
            if (label <= i and worker_response[pivot] == 1):
                dist_inconsistent += i - label + 1
            elif (label > i and worker_response[pivot] == 0):
                dist_inconsistent += label - i
        return dist_inconsistent
    elif consistency_metric == 'num_inconsistent_against_label':
        num_inconsistent = 0
        for i, pivot in enumerate(pivots):
            if (label <= i and worker_response[pivot] == 1):
                num_inconsistent += 1
            elif (label > i and worker_response[pivot] == 0):
                num_inconsistent += 1
        return num_inconsistent


def plot_accuracy_vs_consistency(accuracies, consistencies):
    raise NotImplementedError


def plot_gt_vs_signal(consolidated_worker_labels):
    raise NotImplementedError


def plot_all():
    responses_by_worker, responses_on_image = read_responses_and_stats()
    # for consolidation_method in ['min_dist_inconsistent']:
    for inconsistency_metric in ['dist_inconsistent_against_label']:
        for error_metric in ['image_distance']:
            # one aggregate plot across images
            # consistencies = {}
            for image in responses_on_image:
                # one plot per image
                inconsistencies = defaultdict(list)
                errors = defaultdict(list)
                for worker in responses_on_image[image]:
                    worker_response = responses_on_image[image][worker]
                    if len(worker_response) < 4:
                        # ignore workers who did not repsond to all pivots
                        continue
                    worker_labels = []
                    # worker_labels.append(consolidated_worker_response(
                    #     consolidation_method,
                    #     worker_response
                    # ))
                    worker_labels = range(0, len(pivots) + 1)
                    for worker_label in worker_labels:
                        inconsistencies[worker].append(
                            calculate_inconsistency(
                                inconsistency_metric,
                                worker_response,
                                worker_label
                            )
                        )
                        errors[worker].append(
                            calculate_error(
                                error_metric,
                                worker_label,
                                image
                            )
                        )
                fig_name = 'img{}__{}__vs__{}'.format(
                    image,
                    # consolidation_method,
                    error_metric,
                    inconsistency_metric
                )
                xlabel = inconsistency_metric
                ylabel = error_metric
                legend = []
                plt.figure()
                for worker in inconsistencies:
                    inconsistencies[worker] = [old_val + random.random() / 10.0 for old_val in inconsistencies[worker]]
                    errors[worker] = [old_val + random.random() / 10.0 for old_val in errors[worker]]
                    print 'Worker: {}, Image: {}, inconsistencies: {}, errors: {}'.format(
                        worker, image, inconsistencies[worker], errors[worker])
                    plt.plot(inconsistencies[worker], errors[worker], 'x')
                    legend.append(worker)
                plt.xlabel(xlabel)
                plt.ylabel(ylabel)
                plt.xticks(
                    range(12) if inconsistency_metric == 'dist_inconsistent_against_label'
                    else range(5)
                )
                plt.yticks(range(5))
                # plt.legend(legend)
                plt.title('Error (y) vs inconsistency (x)')
                plt.savefig(fig_name)
                plt.close()


if __name__ == '__main__':
    plot_all()
