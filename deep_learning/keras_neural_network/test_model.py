from keras.models import model_from_json
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
import os
import sys
import numpy as np
import pandas as pd

#--------------------------------
# test a trained model on a test dataset
# outputs average loss and accuracy of the model on that dataset (write to csv maybe?)
# commandline arguments: 1. path to the folder of the model;
#                        2. test dataset (dataset2 or dataset4)
# -------------------------------



# test_on_batch = True: batch-test many results
# test_on_batch = False: test a single result
test_on_batch = True

def test_model(path, test_data):
	# parameters
	img_width = 128
	img_height = 128
	batch_size = 32

	# load model & weights
	json_path = os.path.join(path, "model.json")
	h5_path = os.path.join(path, "weights.h5")
	if (not os.path.isfile(json_path)):
		sys.exit("model.json not found")
	if (not os.path.isfile(h5_path)):
		sys.exit("weights.h5 not found")

	# load json and reconstruct NN model
	json_file = open(json_path, 'r')
	loaded_model_json = json_file.read()
	json_file.close()
	model = model_from_json(loaded_model_json)
	print("Model loaded from json file")

	# load weights into new model
	print('h5path:', h5_path)
	model.load_weights(h5_path)
	print("Weights loaded from h5 file")
	myoptimizer = optimizers.SGD(lr=0.1, momentum = 0.9, nesterov = True)
	model.compile(loss='categorical_crossentropy',
				  optimizer=myoptimizer,
				   metrics=['accuracy'])

	if '2' in test_data or 'two' in test_data or 'second' in test_data:
		test_data_folder = '../dataset_new/dataset2/test'
	elif '4' in test_data or 'four' in test_data or 'fourth' in test_data:
		test_data_folder = '../dataset_new/dataset4/test'

	test_datagen = ImageDataGenerator(rescale=1./255)
	test_generator = test_datagen.flow_from_directory(
					test_data_folder,
					target_size=(img_width, img_height),
					batch_size=batch_size,  # batch_size number of test data are feed in each time
					class_mode='categorical',
					shuffle=False)

	preds = model.predict_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:])/batch_size))
	# losses = [loss for negative class, loss for positive class] => array of size 2
	losses = model.evaluate_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:])/batch_size))
	# losses = [1, 2]
	correct = 0 # counter that stores how many correct predictions are in preds
	loss = sum(losses) * 1.0 / 2
	print(losses)
	for i in range(len(test_generator.filenames[:])):
		real_label = test_generator.classes[i]
		probability = preds[i]

		if probability[0] > probability[1]: # predict negative
			curr_patch_pred = 0
		else:
			curr_patch_pred = 1 # predict positive

		# accumulator for single patch
		if curr_patch_pred == real_label:
			correct += 1
	accuracy = correct * 1.0 / len(test_generator.filenames[:])
	print('test results of', path, ':')
	print('accuracy = ', accuracy)
	print('loss = ', loss)
	print('')

# main function
# batch-test
if test_on_batch:
	results_folder = sys.argv[1]
	subfolders = []
	# Subroutine to find subfolders
	for root, dirs, files in os.walk(results_folder, topdown=False):
		for name in dirs:
			subfolders.append(name)

	# find the dataset corresponding each result folder
	# find the .csv file in results_folder
	filenames = os.listdir(results_folder)
	for file in filenames:
		if file.endswith('.csv'):
			logs = pd.read_csv(os.path.join(results_folder, file))

	# match folders with datasets
	# ds: key: folder name; value: dataset name
	ds = {}
	for i in range(len(logs['folder'])):
		ds[logs['folder'][i]] = logs['dataset'][i]
	# # For each subfolder
	for subfolder in subfolders:
		test_model(subfolder, ds[subfolder])

# single test
else:
	# must provide the folder path
	path = sys.argv[1]
	results_folder = os.path.dirname(path)
	folder_name = os.path.basename(path)

	filenames = os.listdir(results_folder)

	# look for csv log file in the same level as the 'path'
	for file in filenames:
		if file.endswith('.csv'):
			logs = pd.read_csv(os.path.join(results_folder, file))
	# look for the row of the folder we want to test
	for i in range(len(logs['folder'])):
		if logs['folder'][i] == folder_name:
			ds = logs[dataset][i]

	test_model(path, ds)