import os
import shutil
import numpy as np
import re
import sys

######### parameters ###########
src_path = '../datasets/second_dataset/train/negative/'
dst_path_train = '../dataset_new/dataset2/train/negative/'
dst_path_valid = '../dataset_new/dataset2/valid/negative/'
dst_path_test = '../dataset_new/dataset2/test/negative/'
train_ratio = 0.70
valid_ratio = 0.15
test_ratio  = 0.15
################################

assert (train_ratio + valid_ratio + test_ratio == 1.0), "sum of ratios does not equal to 1.0 !!!"

file_list = np.array(os.listdir(src_path))
print("Total number of patches:", len(file_list))

image_dict = {}
for name in file_list:
    temp = re.split('_|-', name)

    if 'PR' in temp[0]:
        image_name = temp[1]
    elif 'TMA' in temp[0]:
        image_name = temp[2]
    else:
        image_name = temp[3]
    if image_name not in image_dict:
        image_dict[image_name] = []
    image_dict[image_name].append(name)

keys = image_dict.keys()
keys = list(keys)

length = len(keys)
print("Number of images:", length)

split_point1 = int(length*train_ratio)
split_point2 = int(length*(train_ratio+valid_ratio))

# split on images, so all patches that belongs to a single image
# will stay together
train_keys = keys[0:split_point1]
valid_keys = keys[split_point1:split_point2]
test_keys = keys[split_point2:]

print("length of train images:", len(train_keys))
print("length of valid image:", len(valid_keys))
print("length of test images:", len(test_keys))

assert (len(train_keys) + len(valid_keys) + len(test_keys) == length), "sum of 3 keys partitions does not equal to total sum !!!"

train_list = []
for item in train_keys:
    train_list = train_list+image_dict[item]

valid_list = []
for item in valid_keys:
    valid_list = valid_list+image_dict[item]

test_list = []
for item in test_keys:
    test_list = test_list+image_dict[item]

assert (len(train_list) + len(valid_list) + len(test_list) == len(file_list)), "sum of 3 patches partitions does not equal to total sum !!!"

print("length of train patches:", len(train_list))
print("length of valid patches:", len(valid_list))
print("length of test patches:", len(test_list))


for item in test_list:
    shutil.copy(src_path+item, dst_path_test+item)

for item in valid_list:
    shutil.copy(src_path+item, dst_path_valid+item)

for item in train_list:
    shutil.copy(src_path+item, dst_path_train+item)

