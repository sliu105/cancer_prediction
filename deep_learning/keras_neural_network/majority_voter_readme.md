# Crowd Cancer Prediction

The current working repository is located on gitlab, due to constraints that github places on file sizes (the pytorch trained model files are about 600mb).

## Crowdsourcing/

All files to do with the crowdsourcing aspect!

## Deep_learning

Deep learning models and datasets!

## Inference script
* script functionality: measures **image-wide accuracy** on test dataset using majority vote; measures **patch-wide accuracy** on test dataset using patch prediction
* script location: `deep_learning/keras_neural_network/majority_voter.py`
* how to use?

`python majority_voter.py MODEL_FILE_PATH WEIGHTS_FILE_PATH TEST_DATA_PATH > path_to_log_file`

## Example parameter example

python majority_voter.py ../keras_neural_network/results/9_27_resnet34_l2\=1e-4/model.json ../keras_neural_network/results/9_27_resnet34_l2\=1e-4/weights.h5 ../datasets/first_dataset/test/ >> 9_27_resnet34_l21e-4_first_dataset_log.txt


## Future parameter example (todo)
python majority_voter.py 9_27_resnet34_l2\=1e-4 first_dataset/test/


## Surveys
Literature writeups and reviews!

