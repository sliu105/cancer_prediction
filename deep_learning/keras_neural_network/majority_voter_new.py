import os
import numpy as np
from keras.models import model_from_json
from keras.preprocessing.image import ImageDataGenerator
import re
import pandas as pd

# inputs: model_path, weights_path, dataset_path
# returns: patch wide accuracy, image wide accuracy
def majorityInstance(model_path, weights_path, dataset_path):
    # parameters
    img_width, img_height = 128, 128

    if not os.path.isfile(model_path):
        print("model file does not exist!")
        return -1,-1
    if not os.path.isfile(weights_path):
        print("weights file does not exist!")
        return -1,-1

    # load json and reconstruct NN model
    try:
        json_file = open(model_path, 'r')
        loaded_model_json = json_file.read()
        json_file.close()
        loaded_model = model_from_json(loaded_model_json)
        print("Model loaded from json file")
    except OSError:
        print('OSError when trying to load:', model_path)
        return -1,-1

    # load weights into new model
    try:
        loaded_model.load_weights(weights_path)
        print("Weights loaded from h5 file")
    except OSError:
        print('OSError when trying to load:', weights_path)
        return -1,-1

    # generate test data
    test_datagen = ImageDataGenerator(rescale=1./255)
    test_generator = test_datagen.flow_from_directory(
                    dataset_path,
                    target_size=(img_width, img_height),
                    batch_size=32,  # batch_size number of test data are feed in each time
                    class_mode='binary',
                    shuffle=False)

    # batch_size * steps should have enough threads to load all test data
    preds = loaded_model.predict_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:])/32.0))

    # store result into a dictionary
    # keys: image name, each image has multiple patches
    # value: [# of 0 class vote, # of 1 class vote, real label]
    image_dict = {}
    correct_patch = 0
    for i in range(len(test_generator.filenames[:])):
        file_name = test_generator.filenames[i].split('/')[-1]
        real_label = test_generator.classes[i]
        probability = preds[i]
        # print(probability)

        # get name of the image that current patch belongs to
        temp = re.split('_|-', file_name)
        if 'PR' in temp[0]:
            image_name = temp[1]
        elif 'TMA' in temp[0]:
            image_name = temp[2]
        else:
            image_name = temp[3]

        if image_name not in image_dict:
            image_dict[image_name] = [0, 0, real_label]

        # predict current patch
        #if probability[0] < 0.5: # for bangqi's code
        if probability[0] > probability[1]:
            image_dict[image_name][0] += 1
            curr_patch_pred = 0
        else:
            image_dict[image_name][1] += 1
            curr_patch_pred = 1

        # accumulator for single patch
        if curr_patch_pred == real_label:
            correct_patch += 1
    # end of for loop

    # print("image dictionary is:")
    # print(image_dict)

    # print("probabilities for 2 classes are:")

    # vote by majority
    images = image_dict.keys()
    correct_image = 0
    for image_name in images:
        result = image_dict[image_name]
        
        # print(result)
        if result[0] > result[1]:
            majority = 0
        else:
            majority = 1

        # majority is the predict given by majority vote
        # now compare it with real label, result[2]
        if majority == result[2]:
            correct_image += 1

    patch_accuracy = correct_patch*1.0/len(test_generator.filenames[:])
    image_accuarcy = correct_image*1.0/len(images)

    return patch_accuracy, image_accuarcy
    

if __name__ == "__main__":
    # parameters
    model_folder = './results/'
    dataset_folder = '../datasets/'
    outcsv_path = './majorityMatrix.csv'
    include_training = False

    model_subfolder_list = []
    for model_folder_name, model_subfolders, model_filenames in os.walk(model_folder):
        for model_subfolder in model_subfolders:
            model_subfolder_path = os.path.join(model_folder_name, model_subfolder)
            model_subfolder_list.append(model_subfolder_path)

    data_path_list = []
    temp = os.listdir(dataset_folder)
    for dataset in temp:
        data_path_list.append(dataset_folder+dataset+'/test')
        if include_training:
            data_path_list.append(dataset_folder+dataset+'/train')

    # do majority vote for each combination of model and dataset
    resultMatrix = []
    for model_subfolder in model_subfolder_list:
        model_name = os.path.split(model_subfolder)[1]
        model_path = os.path.join(model_subfolder, 'model.json')
        weights_path = os.path.join(model_subfolder, 'weights.h5')

        if not os.path.isfile(model_path):
            print("model file does not exist in:", model_path)
            continue
        if not os.path.isfile(weights_path):
            print("weights file does not exist in:", weights_path)
            continue
        for dataset_path in data_path_list:
            dataset_name = dataset_path.split('/')
            dataset_name = dataset_name[-2]+'/'+dataset_name[-1]
            curr_patch_accu, curr_image_accu = majorityInstance(model_path, weights_path, dataset_path)
            if(curr_patch_accu < 0 or curr_image_accu < 0):
                continue
            resultMatrix.append([model_name, dataset_name, curr_patch_accu, curr_image_accu])

    print(resultMatrix)
    df = pd.DataFrame(resultMatrix, columns={'model','dataset','patchAccuracy','imageAccuracy'})
    df.to_csv(outcsv_path, index=False)

