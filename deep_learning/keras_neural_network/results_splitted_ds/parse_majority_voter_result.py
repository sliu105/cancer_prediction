'''
read from all csv files that record majority voter results (for train/test/valid) in the input folder(s)
calculate accuracy, precision, and recall for each csv file
'''
import pandas as pd
import numpy as np
import sys
import os

csv_file = sys.argv[1]
param_info_log = pd.read_csv(csv_file)
folder_list = list(param_info_log['folder'])
print('folder_list:')
print(folder_list)
print('')
def analyze(csv_file, path):
	print(csv_file)
	logs = pd.read_csv(csv_file)
	true_pos = 0
	true_neg = 0
	false_pos = 0
	false_neg = 0
	tot = len(logs['true_label'])

	for i in range(tot):
		
		if logs['true_label'][i] == 1 and logs['right_or_wrong'][i] == 'Correct':
			true_pos += 1
		
		elif logs['true_label'][i] == 1 and logs['right_or_wrong'][i] == 'Wrong':
			false_neg += 1
			
		elif logs['true_label'][i] == 0 and logs['right_or_wrong'][i] == 'Correct':
			true_neg += 1
			
		else:
			false_pos += 1
		
	# print('true_pos:', true_pos)
	# print('false_pos:', false_pos)
	# print('true_neg:', true_neg)
	# print('false_neg:', false_neg)
	# calculate accuracy, precision, recall, f1 score
	acc = (true_pos + true_neg) * 1.0 / tot
	if true_pos == 0 and false_neg == 0:
		recall = np.inf
	else:
		recall = true_pos * 1.0 / (true_pos + false_neg)
	if true_pos == 0 and false_pos == 0:
		prec = np.inf
	else:
		prec = true_pos * 1.0 / (true_pos + false_pos)
	if np.isfinite(recall) and np.isfinite(prec):
		f1 = 2.0 * prec * recall / (prec + recall)
	else:
		f1 = np.inf
	# print('Analysis of ', csv_file)
	# print('accuracy = ', acc)
	# print('Recall = ', recall)
	# print('Precision = ', prec)
	# print('F1 score = ', f1)
	# print('')

	# write result to param_info.csv
	print('path:', path)
	if path in folder_list:
		idx = folder_list.index(path)
		print(idx)
		if 'train' in csv_file:
			param_info_log.at[idx, 'training image-wide accuracy'] = acc
			param_info_log.at[idx, 'training imagewise precision'] = prec
			param_info_log.at[idx, 'training imagewise recall'] = recall
			param_info_log.at[idx, 'training imagewise f1'] = f1
		elif 'valid' in csv_file:
			param_info_log.at[idx, 'validation image-wide accuracy'] = acc
			param_info_log.at[idx, 'valid imagewise precision'] = prec
			param_info_log.at[idx, 'valid imagewise recall'] = recall
			param_info_log.at[idx, 'valid imagewise f1'] = f1			
		else:
			param_info_log.at[idx, 'test image-wide accuracy'] = acc
			param_info_log.at[idx, 'test imagewise precision'] = prec
			param_info_log.at[idx, 'test imagewise recall'] = recall
			param_info_log.at[idx, 'test imagewise f1'] = f1
	else:
		print(path, 'not found in param_info.csv!')


# list of result folders to be processed
csv_paths = folder_list

# find all folders in folder list
for path in csv_paths:
	# find all files in each folder and select csv files related to majority voting
	for file in os.listdir(path):
		if file.endswith('.csv') and 'majority' in file:
			analyze(os.path.join(path, file), path)

# write result to a new param_info.csv
# print(param_info_log)
param_info_log.to_csv(csv_file)
