"""Trains a ResNet on the CIFAR10 dataset.
Greater than 91% test accuracy (0.52 val_loss) after 50 epochs
48sec per epoch on GTX 1080Ti
Deep Residual Learning for Image Recognition
https://arxiv.org/pdf/1512.03385.pdf
"""

from __future__ import print_function
import keras
from keras.layers import Dense, Conv2D, BatchNormalization, Activation
from keras.layers import MaxPooling2D, AveragePooling2D, Input, Flatten, Dropout
from keras.optimizers import Adam
from keras.callbacks import ReduceLROnPlateau, EarlyStopping, CSVLogger
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras.models import Model
import numpy as np
import sys
# import majority_voter
# import multi_gpu

"""
parameters
"""
data_augmentation = True
# num_filters, num_blocks, and num_sublocks may change if use resnet34 or deeper
num_filters = 64
num_blocks = 1
num_sub_blocks = 2
use_max_pool = True

# dimensions of images.
img_width, img_height = 128, 128
img_channels = 3
input_shape = (img_width, img_height, img_channels)
# directory
train_data_dir = '/home/ubuntu/cancer-prediction/deep_learning/dataset_new/dataset4/train'
validation_data_dir = '/home/ubuntu/cancer-prediction/deep_learning/dataset_new/dataset4/valid'
test_data_folder = '/home/ubuntu/cancer-prediction/deep_learning/dataset_new/dataset4/test'
# parameters
# dataset2: train: 24700, test: 8000
# dataset4: train: 26500, test: 9000
nb_train_samples = 24700 # 26500
nb_validation_samples = 5400 # 9000
nb_epoch = 35
nb_batch = 32
nb_classes = 2
dropout_mag = 0.2

# callbacks for stable loss
lr_reducer = ReduceLROnPlateau(factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.0001, patience=15)
csv_logger = CSVLogger('output.csv')
callbacks = [lr_reducer, csv_logger]

# Start model definition.
inputs = Input(shape=input_shape)
x = Conv2D(num_filters,
           kernel_size=7,
           padding='same',
           strides=2,
           kernel_initializer='he_normal',
           kernel_regularizer=l2(1e-4))(inputs)
x = BatchNormalization()(x)
x = Dropout(dropout_mag)(x)
x = Activation('relu')(x)


# Orig paper uses max pool after 1st conv.
# Reaches up 87% acc if use_max_pool = True.
# Cifar10 images are already too small at 32x32 to be maxpooled. So, we skip.
if use_max_pool:
    x = MaxPooling2D(pool_size=3, strides=2, padding='same')(x)
    num_blocks = 3

# Instantiate convolutional base (stack of blocks).
for i in range(1):
    for j in range(1):
        strides = 1
        is_first_layer_but_not_first_block = True # j == 0 and i > 0
        if is_first_layer_but_not_first_block:
            strides = 2
        y = Conv2D(num_filters,
                   kernel_size=3,
                   padding='same',
                   strides=strides,
                   kernel_initializer='he_normal',
                   kernel_regularizer=l2(1e-4))(x)
        y = BatchNormalization()(y)
        y = Dropout(dropout_mag)(y)
        y = Activation('relu')(y)
        
        if is_first_layer_but_not_first_block:
            x = Conv2D(num_filters,
                       kernel_size=1,
                       padding='same',
                       strides=2,
                       kernel_initializer='he_normal',
                       kernel_regularizer=l2(1e-4))(x)
        x = keras.layers.add([x, y])
        # y = Dropout(dropout_mag)(y)
        x = Activation('relu')(x)

    num_filters = 2 * num_filters

# Add classifier on top.
x = AveragePooling2D()(x)
y = Flatten()(x)
y = Dense(1000, activation = 'relu', kernel_initializer = 'he_normal')(y)
y = Dropout(0.5)(y)
outputs = Dense(nb_classes,
                activation='softmax',
                kernel_initializer='he_normal')(y)

# Instantiate and compile model.
model = Model(inputs=inputs, outputs=outputs)
# model = multi_gpu.make_parallel(model, 2)
model.compile(loss='categorical_crossentropy',
              optimizer=Adam(),
              metrics=['accuracy'])
model.summary()



# Run training, with or without data augmentation.
if not data_augmentation:
    print('Not using data augmentation.')
    model.fit(x_train, y_train,
              batch_size=nb_batch,
              epochs=epochs,
              validation_data=(x_test, y_test),
              shuffle=True,
              callbacks=callbacks)
else:
    print('Using real-time data augmentation.')
    # This will do preprocessing and realtime data augmentation:
    # datagen = ImageDataGenerator(
    #     featurewise_center=False,  # set input mean to 0 over the dataset
    #     samplewise_center=False,  # set each sample mean to 0
    #     featurewise_std_normalization=False,  # divide inputs by std of the dataset
    #     samplewise_std_normalization=False,  # divide each input by its std
    #     zca_whitening=False,  # apply ZCA whitening
    #     rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
    #     width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
    #     height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
    #     horizontal_flip=True,  # randomly flip images
    #     vertical_flip=False)  # randomly flip images

    # # Compute quantities required for featurewise normalization
    # # (std, mean, and principal components if ZCA whitening is applied).
    # datagen.fit(x_train)
    
    # load data
    # training data
    train_datagen = ImageDataGenerator(rescale=1./255)
    # 
    test_datagen = ImageDataGenerator(rescale=1./255)

    train_generator = train_datagen.flow_from_directory(
            train_data_dir,
            target_size=(img_width, img_height),
            batch_size=nb_batch,
            class_mode='categorical')

    validation_generator = test_datagen.flow_from_directory(
            validation_data_dir,
            target_size=(img_width, img_height),
            batch_size=nb_batch,
            class_mode='categorical')


    model.fit_generator(
            train_generator,
            samples_per_epoch=nb_train_samples,
            nb_epoch=nb_epoch,
            validation_data=validation_generator,
            nb_val_samples=nb_validation_samples,
            callbacks=callbacks)

    # test the model on the original test dataset
    test_datagen = ImageDataGenerator(rescale=1./255)
    test_generator = test_datagen.flow_from_directory(
                                        test_data_folder,
                                        target_size=(img_width, img_height),
                                        batch_size=50,  # batch_size number of test data are feed in each time
                                        class_mode='categorical',
                                        shuffle=False)

    preds = model.predict_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:])/50))
        # losses = [loss for negative class, loss for positive class] => array of size 2
    losses = model.evaluate_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:])/50))
        # losses = [1, 2]
    correct = 0 # counter that stores how many correct predictions are in preds
    loss = sum(losses) * 1.0 / 2
     
    for i in range(len(test_generator.filenames[:])):
        real_label = test_generator.classes[i]
        probability = preds[i]

        if probability[0] > probability[1]: # predict negative
            curr_patch_pred = 0
        else:
            curr_patch_pred = 1 # predict positive

                # accumulator for single patch
        if curr_patch_pred == real_label:
            correct += 1
    accuracy = correct * 1.0 / len(test_generator.filenames[:])
    
    # record the results
    output = open('test_result.txt', 'a')
    old_out = sys.stdout
    sys.stdout = output

    print('Test result of test data with no preprocessing:')
    print('accuracy = ',  accuracy)
    print('loss = ', loss)
    print('')

    output.close()
    sys.stdout = old_out

# Prepare model model saving directory.
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)

model.save_weights("weights.h5")
print("Saved model to disk")

# perform majority voting
# majority_voter.vote(dataset_name = '4', loaded_model = model)
