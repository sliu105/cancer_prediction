('Results of ', 'results_splitted_ds', 'on', 'dataset4', '_', 'train', ':')
Patch-wide accuracy is: 0.968663967611
Image-wide accuracy given by majority vote is: 1.0

('Results of ', 'results_splitted_ds', 'on', 'dataset4', '_', 'valid', ':')
Patch-wide accuracy is: 0.55962962963
Image-wide accuracy given by majority vote is: 0.537037037037

('Results of ', 'results_splitted_ds', 'on', 'dataset4', '_', 'test', ':')
Patch-wide accuracy is: 0.572037037037
Image-wide accuracy given by majority vote is: 0.574074074074

