import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

# change font size of xlabel & ylabel
# mpl.rcParams['xtick.labelsize'] = 20
# mpl.rcParams['ytick.labelsize'] = 20

# [0, 0.05, 0.2, 0.5, 0.8]
val_acc = [0.630969492, 0.619946217, 0.529180963, 0.5160, 0.518482242]
val_loss = [1.905789432, 1.785008532, 1.676289921, 1.4855, 0.747904519]

train_acc = [0.99878385, 0.978717367, 0.908504946, 0.7894, 0.613020918]
train_loss = [0.099926169, 0.148655604, 0.284251126, 0.4864, 0.707563093]

# 0 is missing for now
test_acc = [0.617037037, 0.60537037037, 0.601481481481, 0.539444444, 0.518518518519]
test_loss = [1.246228835, 1.04765669629, 0.936063994205, 0.924037224, 0.632544068827]
x = [0, 0.05, 0.2, 0.5, 0.8]

fig, ax = plt.subplots()
ax.plot(x, val_acc, linestyle = '--')
ax.plot(x, train_acc, linestyle = '--')
ax.plot(x, test_acc, linestyle = '--')
ax.scatter(x, val_acc, label = 'validation accuracy')
ax.scatter(x, train_acc, label = 'training accuracy')
ax.scatter(x, test_acc, label = 'testing accuracy')
plt.xlabel('Magnitude of dropout')
plt.ylabel('Accuracy')
plt.title('Effect of dropout on Accuracy of Resnet 1 block on dataset4')
plt.legend()

fig, ax = plt.subplots()
ax.plot(x, val_loss, linestyle = '--')
ax.plot(x, train_loss, linestyle = '--')
ax.plot(x, test_loss, linestyle = '--')
ax.scatter(x, val_loss, label = 'validation loss')
ax.scatter(x, train_loss, label = 'training loss')
ax.scatter(x, test_loss, label = 'testing loss')
plt.xlabel('Magnitude of dropout')
plt.ylabel('Loss')
plt.title('Effect of dropout on Loss of Resnet 1 block on dataset4')
plt.legend()

plt.show()