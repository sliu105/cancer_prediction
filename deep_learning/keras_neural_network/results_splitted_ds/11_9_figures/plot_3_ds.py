import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np

# change font size of xlabel & ylabel
# mpl.rcParams['xtick.labelsize'] = 20
# mpl.rcParams['ytick.labelsize'] = 20

# [0blk, 1blk, 2blk, 3blk (4blk)]
ds4_train = [0.6000, 0.99878385, 0.9987, 0.9901, 0.934449489]
cifar_train = [0.6955,  0.9164, 0.9106, 0.9006, 0.985550752]
mnist_train = [0.9995, 0.999616667, 1.0000, 0.9998]

cifar_train_loss = [0.9084, 0.3504, 0.4420, 0.4693, 0.222740032]
ds4_train_loss = [0.6578, 0.099926169, 0.0960, 0.1005, 0.124952627]
mnist_train_loss = [0.0090, 0.009660913, 0.0080, 0.0145]

ds4_valid = [0.4998, 0.630969492, 0.6470, 0.6205, 0.634539596]
cifar_valid = [0.7056,  0.8606, 0.8390, 0.8338, 0.9088]
mnist_valid = [0.9894, 0.9939, 0.9944, 0.9933]

cifar_valid_loss = [0.8751, 0.5582, 0.7098, 0.7020, 0.553017384]
ds4_valid_loss = [0.7704, 1.905789432, 2.7738, 2.2335, 1.668933702]
mnist_valid_loss = [0.0411, 0.03423613, 0.0276, 0.0403]

ds4_test_acc = [0.5435185185185185,  0.6, 0.6053703703703703, 0.5648148148148148, 0.617037037]
ds4_test_loss = [0.79211978400471028, 1.3, 1.853585000253386, 1.6177176227448165, 1.246228835]

xticks = ['0 block', '1 block', '2 blocks', '3 blocks', '4 blocks(ResNet18)']
x = np.asarray([1, 2, 3, 4, 5])

xticks1 = ['0 block', '1 block', '2 blocks', '3 blocks']
x1 = np.asarray([1, 2, 3, 4])

fig, ax = plt.subplots()
ax.plot(x, ds4_train, linestyle = '--')
# ax.plot(x, cifar_train, linestyle = '--')
# ax.plot(x1, mnist_train, linestyle = '--')
ax.scatter(x, ds4_train, label = 'dataset4')
# ax.scatter(x, cifar_train, label = 'cifar10')
# ax.scatter(x1, mnist_train, label = 'mnist')
plt.xticks(x1, xticks1)
plt.xticks(x, xticks)
plt.title('Training accuracy of different sizes of ResNet on dataset4')
plt.legend()


fig, ax = plt.subplots()
ax.plot(x, ds4_valid, linestyle = '--')
# ax.plot(x, cifar_valid, linestyle = '--')
# ax.plot(x1, mnist_valid, linestyle = '--')
ax.scatter(x, ds4_valid, label = 'dataset4')
# ax.scatter(x, cifar_valid, label = 'cifar10')
# ax.scatter(x1, mnist_valid, label = 'mnist')
plt.xticks(x1, xticks1)
plt.xticks(x, xticks)
plt.title('Validation accuracy of different sizes of ResNet on dataset4')
plt.legend()

fig, ax = plt.subplots()
ax.plot(x, ds4_train_loss, linestyle = '--')
# ax.plot(x, cifar_train_loss, linestyle = '--')
# ax.plot(x1, mnist_train_loss, linestyle = '--')
ax.scatter(x, ds4_train_loss, label = 'dataset4')
# ax.scatter(x, cifar_train_loss, label = 'cifar10')
# ax.scatter(x1, mnist_train_loss, label = 'mnist')
plt.xticks(x1, xticks1)
plt.xticks(x, xticks)
plt.title('Training loss of different sizes of ResNet on dataset4')
plt.legend()


fig, ax = plt.subplots()
ax.plot(x, ds4_valid_loss, linestyle = '--')
# ax.plot(x, cifar_valid_loss, linestyle = '--')
# ax.plot(x1, mnist_valid_loss, linestyle = '--')
ax.scatter(x, ds4_valid_loss, label = 'dataset4')
# ax.scatter(x, cifar_valid_loss, label = 'cifar10')
# ax.scatter(x1, mnist_valid_loss, label = 'mnist')
plt.xticks(x1, xticks1)
plt.xticks(x, xticks)
plt.title('Validation loss of different sizes of ResNet on dataset4')
plt.legend()

fig, ax = plt.subplots()
ax.plot(x, ds4_test_acc, linestyle = '--')
ax.scatter(x, ds4_test_acc)
plt.xticks(x, xticks)
plt.title('Testing accuracy of different sizes of ResNet on dataset4')

fig, ax = plt.subplots()
ax.plot(x, ds4_test_loss, linestyle = '--')
ax.scatter(x, ds4_test_loss)
plt.xticks(x, xticks)
plt.title('Testing loss of different sizes of ResNet on dataset4')
plt.show()