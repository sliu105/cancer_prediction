('Results of ', 'results_splitted_ds', 'on', 'dataset4', '_', 'train', ':')
Patch-wide accuracy is: 0.983805668016
Image-wide accuracy given by majority vote is: 1.0

('Results of ', 'results_splitted_ds', 'on', 'dataset4', '_', 'valid', ':')
Patch-wide accuracy is: 0.590740740741
Image-wide accuracy given by majority vote is: 0.62962962963

('Results of ', 'results_splitted_ds', 'on', 'dataset4', '_', 'test', ':')
Patch-wide accuracy is: 0.591666666667
Image-wide accuracy given by majority vote is: 0.611111111111

