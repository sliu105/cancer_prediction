from PIL import Image
import os

def transform(read_folder, write_folder):
	if not os.path.exists(write_folder):
	    os.makedirs(write_folder)

	filenames = os.listdir(read_folder)
	for file in filenames:
		im = Image.open(os.path.join(read_folder, file))
		im1 = im.rotate(90)
		# im1 = im.transpose(Image.FLIP_LEFT_RIGHT)
		# im1.show()
		im1.save(os.path.join(write_folder, file))
	print('Rotating images from ', read_folder, 'is done')

read_folder = '../dataset_new/dataset4/train/positive'
write_folder = '../dataset_new/dataset4_rotate_90/train/positive'
transform(read_folder, write_folder)

read_folder = '../dataset_new/dataset4/train/negative'
write_folder = '../dataset_new/dataset4_rotate_90/train/negative'
transform(read_folder, write_folder)
