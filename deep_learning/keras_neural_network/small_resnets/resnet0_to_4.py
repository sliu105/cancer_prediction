"""Trains a ResNet on the CIFAR10 dataset.
Greater than 91% test accuracy (0.52 val_loss) after 50 epochs
48sec per epoch on GTX 1080Ti
Deep Residual Learning for Image Recognition
https://arxiv.org/pdf/1512.03385.pdf
"""

from __future__ import print_function
import keras
from keras.layers import Dense, Conv2D, BatchNormalization, Activation
from keras.layers import MaxPooling2D, AveragePooling2D, Input, Flatten, Dropout
from keras.optimizers import Adam
from keras.callbacks import ReduceLROnPlateau, EarlyStopping, CSVLogger
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras.models import Model
import numpy as np
import sys
import matplotlib.pyplot as plt
import pandas as pd
import majority_voter
# import multi_gpu

################### parameters ###################

# dimensions of images
img_width, img_height = 128, 128
img_channels = 3
input_shape = (img_width, img_height, img_channels)

# dimension of datasets
# dataset2: train: 24700, test: 8000
# dataset4: train: 26500, test: 9000
nb_train_samples = 23450
nb_validation_samples = 4650


# num_filters, num_blocks, and num_sublocks may change if use resnet34 or deeper
num_filters = 64  # num of output feature maps
num_blocks = 2  # num of cov layer beyond 1st cov layer
num_sub_blocks = 2  # 2 sub block per block for ResNet18
use_max_pool = True
nb_epoch = 40
nb_batch = 32
nb_classes = 2
dropout_mag = 0.2  # throw away 20%

# directories
train_data_dir = '../../dataset_new/dataset2/train'
validation_data_dir = '../../dataset_new/dataset2/valid'
test_data_folder = '../../dataset_new/dataset2/test'

################### model design ###################

# callbacks for stable loss
lr_reducer = ReduceLROnPlateau(factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.0001, patience=15)
csv_logger = CSVLogger('output.csv')
callbacks = [lr_reducer, csv_logger]

#### conv1 layer of ResNet ####
inputs = Input(shape=input_shape)
x = Conv2D(num_filters,
           kernel_size=7,
           padding='same',
           strides=2,
           kernel_initializer='he_normal',
           kernel_regularizer=l2(1e-4))(inputs)
x = BatchNormalization()(x)
x = Activation('relu')(x)

# Orig paper uses max pool after 1st conv
# Reaches up 87% acc if use_max_pool = True
# Cifar10 images, with size of 32 x 32, are too small to use maxpooling
if use_max_pool:
    x = MaxPooling2D(pool_size=3, strides=2, padding='same')(x)

# Instantiate convolutional base (stack of blocks).
for i in range(num_blocks):
    for j in range(num_sub_blocks):
        strides = 1
        if num_blocks == 1:
            is_first_layer_but_not_first_block = True
        else:
            is_first_layer_but_not_first_block = j == 0 and i > 0

        if is_first_layer_but_not_first_block:
            strides = 2

        y = Conv2D(num_filters,
                   kernel_size=3,
                   padding='same',
                   strides=strides,
                   kernel_initializer='he_normal',
                   kernel_regularizer=l2(1e-4))(x)
        y = BatchNormalization()(y)
        y = Activation('relu')(y)

        y = Conv2D(num_filters,
                   kernel_size=3,
                   padding='same',
                   kernel_initializer='he_normal',
                   kernel_regularizer=l2(1e-4))(y)
        y = BatchNormalization()(y)

        if is_first_layer_but_not_first_block:
            x = Conv2D(num_filters,
                       kernel_size=1,
                       padding='same',
                       strides=2,
                       kernel_initializer='he_normal',
                       kernel_regularizer=l2(1e-4))(x)
        x = keras.layers.add([x, y])

        x = Activation('relu')(x)

    # double num of output feature maps for each following block
    num_filters = 2 * num_filters


# fully connected layer
x = AveragePooling2D()(x)
y = Flatten()(x)
outputs = Dense(nb_classes,
                activation='softmax',
                kernel_initializer='he_normal')(y)

# Instantiate and compile model
model = Model(inputs=inputs, outputs=outputs)
# model = multi_gpu.make_parallel(model, 2)
model.compile(loss='categorical_crossentropy',
              optimizer=Adam(),
              metrics=['accuracy'])
model.summary()

################### training and validation ###################

train_datagen = ImageDataGenerator(rescale=1. / 255)
test_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_width, img_height),
    batch_size=nb_batch,
    class_mode='categorical')
validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=nb_batch,
    class_mode='categorical')

model.fit_generator(
    train_generator,
    samples_per_epoch=nb_train_samples,
    nb_epoch=nb_epoch,
    validation_data=validation_generator,
    nb_val_samples=nb_validation_samples,
    callbacks=callbacks)

################### patch wide testing accuracy ###################

test_datagen = ImageDataGenerator(rescale=1. / 255)
test_generator = test_datagen.flow_from_directory(
    test_data_folder,
    target_size=(img_width, img_height),
    batch_size = 50,  # batch_size number of test data are feed in each time
    class_mode='categorical',
    shuffle=False)

preds = model.predict_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:]) / 50))
losses = model.evaluate_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:]) / 50))

correct = 0  # counter that stores how many correct predictions are in preds
loss = sum(losses) * 1.0 / 2

for i in range(len(test_generator.filenames[:])):
    real_label = test_generator.classes[i]
    probability = preds[i]
    if probability[0] > probability[1]:  # predict negative
        curr_patch_pred = 0
    else:
        curr_patch_pred = 1  # predict positive

    if curr_patch_pred == real_label:
        correct += 1

accuracy = correct * 1.0 / len(test_generator.filenames[:])

# record the results
output = open('test result.txt', 'a')
old_out = sys.stdout
sys.stdout = output
print('testing accuracy:', accuracy)
print('loss:', loss)
print('')
output.close()
sys.stdout = old_out

# Prepare model model saving directory.
model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)

model.save_weights("weights.h5")
print("Saved model to disk")

################### learning curve visualization ###################

dir = '.'
logs = pd.read_csv(dir+'/output.csv')

# summarize history for accuracy
plt.figure(1)
plt.plot(logs['acc'])
plt.plot(logs['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'valid'], loc='upper left')
plt.savefig(dir+'/accu.png')

# summarize history for loss
plt.figure(2)
plt.plot(logs['loss'])
plt.plot(logs['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'valid'], loc='upper left')
plt.savefig(dir+'/loss.png')

################### image wide testing accuracy ###################

# use majority vote on testing dataset to give image wide testing accuracy
majority_voter.vote(dataset_name='2', loaded_model=model)

