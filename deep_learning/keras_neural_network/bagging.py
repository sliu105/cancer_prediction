from keras.models import model_from_json
from keras.preprocessing.image import ImageDataGenerator
import re
import numpy as np
import sys
import os
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv

'''
Function: Use majority_voter.py as a reference, Bagging loads many models (dataset and model as independent as possible)
		  Use majority vote by the many models to predict labels of test data
Parameter: model_weight_paths -- a list of model paths in result folder
		   dataset_num -- either = 2 (dataset 1 or 2), or = 4 (dataset 4)
Output: accuracy, image labels, etc
Return value: None
'''
def bagging(model_weight_paths, dataset_num):
	# parameters
	img_width, img_height = 128, 128

	# model_file_path   = sys.argv[1]
	# weights_file_path = sys.argv[2]
	# test_data_path    = sys.argv[3]

	loaded_model = []
	folder_name = 'results/'
	# load models in the list of model_weight_paths
	for i in range(len(model_weight_paths)):
		print(model_weight_paths[i])
		# append folder name
		folder_name = folder_name + os.path.basename(os.path.normpath(model_weight_paths[i]))

		json_path = os.path.join(model_weight_paths[i], "model.json")
		h5_path = os.path.join(model_weight_paths[i], "weights.h5")
		if (not os.path.isfile(json_path)) or (not os.path.isfile(h5_path)):
			print('cannot find model.json or weights.h5 file')
			return
		# load json and reconstruct NN model
		json_file = open(json_path, 'r')
		loaded_model_json = json_file.read()
		json_file.close()
		loaded_model.append(model_from_json(loaded_model_json))
		print(model_weight_paths[i], "Model loaded from json file")

		# load weights into new model
		print('h5path:', h5_path)
		loaded_model[i].load_weights(h5_path)
		print(model_weight_paths[i], "Weights loaded from h5 file")

	# make a folder named 'folder_name' to store result files
	folder_name = 'bagging_new' + str(len(model_weight_paths))
	if not os.path.exists(folder_name):
		os.makedirs(folder_name)

	# prepare return values
	patch_acc_list = []
	image_acc_list = []
	model_acc_list = []

	
	if dataset_num == 4:
		dataset_name = 'dataset4'
	else:
		dataset_name = 'dataset2'

	# we have 3 subfolders in each dataset: train, valid, test
	for train in range(3):
		if train == 0:
			train_name = 'train'
		elif train == 1:
			train_name = 'valid'
		else:
			train_name = 'test'

		data_folder = '../dataset_new'
		test_data_path = os.path.join(data_folder, dataset_name, train_name)
		# generate test data
		test_datagen = ImageDataGenerator(rescale=1./255)
		test_generator = test_datagen.flow_from_directory(
						test_data_path,
						target_size=(img_width, img_height),
						batch_size=32,  # batch_size number of test data are feed in each time
						class_mode='binary',
						shuffle=False)

		# batch_size * steps should have enough threads to load all test data
		preds = []
		for i in loaded_model:
			preds.append(i.predict_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:])/32.0)))

		# print the large volume results to a folder		
		# change from printing to console to printing to files
		if train == 0:
			output = open(os.path.join(folder_name, 'bagging_dataset'+str(dataset_name)+'_train.txt'), 'w')
		elif train == 1:
			output = open(os.path.join(folder_name, 'bagging_dataset'+str(dataset_name)+'_validation.txt'), 'w')
		else:
			output = open(os.path.join(folder_name, 'bagging_dataset'+str(dataset_name)+'_test.txt'), 'w')
		orig_stdout = sys.stdout
		sys.stdout = output

		print("Bagging result of the following models:")
		print(model_weight_paths)
		print('')

		# store result into a dictionary
		# keys: image name, each image has multiple patches
		# value: [# of 0 class vote, # of 1 class vote, real label]

		# image_dict = {}
		image_dict_list = [dict() for j in range(len(preds))]

		correct_patch = 0
		for i in range(len(test_generator.filenames[:])):
			file_name = test_generator.filenames[i].split('/')[-1]
			real_label = test_generator.classes[i]
			
			# get name of the image that current patch belongs to
			temp = re.split('_|-', file_name)
			if 'PR' in temp[0]:
				image_name = temp[1]
			elif 'TMA' in temp[0]:
				image_name = temp[2]
			else:
				image_name = temp[3]

			if image_name not in image_dict_list[0]:
				for j in range(len(preds)):
					image_dict_list[j][image_name] = [0, 0, real_label]

			# predict current patch
			for j in range(len(preds)):

				#if probability[0] < 0.5: # for bangqi's code
				probability = preds[j][i]
				if len(probability) == 1: # for bangqi's results
					if probability[0] < 0.5:
						image_dict_list[j][image_name][0] += 1
						curr_patch_pred = 0
					else:
						image_dict_list[j][image_name][1] += 1
						curr_patch_pred = 1
				else: # for resnet results:
					if probability[0] > probability[1]:
						image_dict_list[j][image_name][0] += 1
						curr_patch_pred = 0
					else:
						image_dict_list[j][image_name][1] += 1
						curr_patch_pred = 1


				# accumulator for single patch
				if curr_patch_pred == real_label:
					correct_patch += 1

		# print("image dictionary is:")
		# print(image_dict)

		print("probabilities for 2 classes are:")
		# vote by majority


		images = image_dict_list[0].keys()
		correct_image = 0
		correct_bagging = 0
		# for each image
		for image_name in images:
			print('image name:', image_name)
			negative = 0
			positive = 0
			true_label = image_dict_list[0][image_name][2]
			print("true label:", true_label)
			# for each model's prediction
			for j in range(len(preds)):
				result = image_dict_list[j][image_name]
				print('result of model', j, ": ", result)
				print(result)
				if result[0] > result[1]: # the current j-th model votes for negative
					negative += 1
					majority = 0
				else: # the current j-th model votes for postive
					positive += 1
					majority = 1
				# majority is the predict given by majority vote
				# now compare it with real label, result[2]
				if majority == result[2]:
					correct_image += 1
			print('# of model votes positive:', positive, ", # of models vote negative: ", negative)
			# decide the bagging result of models
			if (negative > positive and true_label == 0) or (positive > negative and true_label == 1):
				correct_bagging += 1
			else:
				print("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=")
				print("Wrong!")

		patch_accuracy = correct_patch*1.0/len(model_weight_paths)/len(test_generator.filenames[:])
		print("Average Patch-wide accuracy is: {}".format(patch_accuracy))
		patch_acc_list.append(patch_accuracy)

		image_accuarcy = correct_image*1.0/len(model_weight_paths)/len(images)
		print("Average Image-wide accuracy given by majority vote is: {}".format(image_accuarcy))
		image_acc_list.append(image_accuarcy)

		bagging_acc = correct_bagging * 1.0 / len(images)
		print('correct_bagging:', correct_bagging)
		print("Bagging accuracy given by input models is: {}".format(bagging_acc))
		model_acc_list.append(bagging_acc)

		sys.stdout = orig_stdout
		output.close()
		# write 3 accuracies (patchwise, imagewise, modelwise) to csv

	# open a csv file to store accuracies
	csvname = 'accuracy_' + str(len(model_weight_paths)) + '_models_dataset' + str(dataset_num) + '.csv'
	csvfile = open(os.path.join(folder_name, csvname), 'w')
	filewritter = csv.writer(csvfile, delimiter = ' ')
	filewritter.writerow(('sub-dataset', 'patch_accuracy', 'image_accuracy', 'bagging_accuracy'))
	# the order in each list is: [train, valid, test]
	filewritter.writerow(('train', patch_acc_list[0], image_acc_list[0], model_acc_list[0]))
	filewritter.writerow(('validation', patch_acc_list[1], image_acc_list[1], model_acc_list[1]))
	filewritter.writerow(('test', patch_acc_list[2], image_acc_list[2], model_acc_list[2]))
	csvfile.close()

	return patch_acc_list, image_acc_list, model_acc_list, folder_name

# function that plots the bar chart 
def plot(patch_avg, img_avg, bagging, folder_name, dataset_num):
	if dataset_num == 4:
		xticks = ['dataset4 train', 'dataset4 validation', 'dataset4 test']
	else:
		xticks = ['dataset2 train', 'dataset2 validation', 'dataset2 test']
	x = np.asarray([1, 2, 3])
	bar_width = 0.3

	# plt.figure(1)
	fig, ax = plt.subplots(figsize = (100, 70))
	plt.bar(x, patch_avg, width = 0.3, label = 'patch-wise accuracy', color = 'r')
	plt.bar(x + bar_width, img_avg,  width = 0.3, label = 'image-wise accuracy', color = 'g')
	plt.bar(x + bar_width * 2, bagging, width = 0.3, label = 'bagging accuracy', color = 'b')

	# add notation of accuracies on top of each bar
	for i in range(len(x)):
		ax.text(x[i], patch_avg[i] + 0.01, str(np.float32(patch_avg[i])), horizontalalignment='center', \
			   fontsize = 15)
		ax.text(x[i] + bar_width, img_avg[i]+ 0.01, str(np.float32(img_avg[i])), horizontalalignment='center',\
		        fontsize = 15)
		ax.text(x[i] + bar_width * 2, bagging[i]+ 0.01, str(np.float32(bagging[i])), horizontalalignment='center',
		       fontsize = 15)
	plt.xticks(x, xticks)
	plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=2,\
	           ncol=2, mode="expand", borderaxespad=0.)
	plt.title('Comparision of results on patch-wise accuracy, image-wise accuracy, and bagging accuracy of'+str(len(patch_avg))+'models', fontsize = 20)
	plt.savefig(os.path.join(folder_name, 'bagging'+str(len(patch_avg))+'_dataset'+str(dataset_num)+'.png'))
	# we have two datasets to plot
	# if dataset_num == 4:
	# 	x = [0, 1]
	# 	xticks = ['dataset4_train', 'dataset4_test']
	# 	plt.subplots()
	# 	# plot 3 figures: patch, image, bagging
	# 	for i in range(3):
	# 		# number of bars in this figure
	# 		for j in range((len(result_list))):
				
	# 			plt.bar(x, result_list[j][i], label = algo_list[j])
	# 		plt.xticks(x, xticks)
	# 		plt.ylabel('accuracy')
	# 		plt.legend()
	# 		if i == 0:
	# 			plt.savefig('patch.png')
	# 		elif i == 1:
	# 			plt.savefig('image.png')
	# 		else:
	# 			plt.savefig('bagging.png')
	# else:
	# 	x = [0, 1, 2, 3]
	# 	xticks = ['dataset1_train', 'dataset1_test', 'dataset2_train', 'dataset2_test']
	# 	plt.subplots()

	# 	for i in range(3):
 #                        # number of bars in this figure
 #                        for j in range((len(result_list))):

 #                                plt.bar(x, result_list[j][i], label = algo_list[j])
 #                        plt.xticks(x, xticks)
 #                        plt.ylabel('accuracy')
 #                        plt.legend()
 #                        if i == 0:
 #                                plt.savefig('patch.png')
 #                        elif i == 1:
 #                                plt.savefig('image.png')
 #                        else:
 #                                plt.savefig('bagging.png')		



model1_path = 'results_splitted_ds/rerun_9_16_resnet18_shuijing'
model2_path = 'results_splitted_ds/rerun_9_18_resnet18'
model3_path = 'results_splitted_ds/rerun_10_5_resnet50'
model4_path = 'results_splitted_ds/rerun_10_9_resnet50'
model5_path = 'results_splitted_ds/rerun_10_10_resnet50'

# bagging3 on dataset2
patch3, image3, model3, folder = bagging([model1_path, model2_path, model3_path], 2)
plot(patch3, image3, model3, folder, 2)

# bagging5 on dataset12
patch5, image5, model5, folder = bagging([model1_path, model2_path, model3_path, model4_path, model5_path], 2)
plot(patch5, image5, model5, folder, 2)

# bagging on dataset 4
model1_path = 'results_splitted_ds/rerun_10_15_resnet34_ds4_no_earlystop'
model2_path = 'results_splitted_ds/rerun_10_15_resnet50_ds4'
model3_path = 'results_splitted_ds/rerun_10_18_resnet18_ds4'
model4_path = 'results_splitted_ds/rerun_10_19_resnet18_ds4'
model5_path = 'results_splitted_ds/rerun_10_19_resnet18_ds4_1'

patch3, image3, models3, folder = bagging([model1_path, model2_path, model3_path], 4)
plot(patch3, image3, model3, folder, 4)

patch5, image5, model5, folder = bagging([model1_path, model2_path, model3_path, model4_path, model5_path], 4)
plot(patch5, image5, model5, folder, 4)
