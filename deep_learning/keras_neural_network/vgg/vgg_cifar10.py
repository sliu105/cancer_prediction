from __future__ import print_function
import keras
from keras.layers import Dense, Conv2D, BatchNormalization, Activation
from keras.layers import MaxPooling2D, AveragePooling2D, Input, Flatten
from keras.optimizers import Adam, SGD
from keras.callbacks import ModelCheckpoint, ReduceLROnPlateau, EarlyStopping, CSVLogger
from keras.preprocessing.image import ImageDataGenerator
from keras.regularizers import l2
from keras import backend as K
from keras.models import Model
from keras.datasets import mnist, cifar10
import numpy as np
import os
import vgg16


img_width, img_height = 32, 32
img_channels = 3
input_shape = (img_width, img_height, img_channels)

nb_epoch = 55
nb_batch = 32
nb_classes = 10

(x_train, y_train), (x_test, y_test) = cifar10.load_data()

x_train = x_train.astype('float32')
x_test = x_test.astype('float32')
x_train /= 255
x_test /= 255

y_train = keras.utils.to_categorical(y_train, nb_classes)
y_test = keras.utils.to_categorical(y_test, nb_classes)

# callbacks for stable loss
lr_reducer = ReduceLROnPlateau(factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.001, patience=10)
csv_logger = CSVLogger('output.csv')
callbacks = [lr_reducer, csv_logger, early_stopper]

# build model
model = vgg16.VGG16(include_top=True, weights=None,
          input_tensor=None, input_shape = input_shape,
          classes=nb_classes)

myoptimizer = SGD(lr=0.01, momentum = 0.9, nesterov = True)
model.compile(loss='categorical_crossentropy',
              optimizer=myoptimizer,
              metrics=['accuracy'])
model.summary()

# fit the model 
model.fit(x_train, y_train,
          batch_size=nb_batch,
          epochs=nb_epoch,
          validation_data=(x_test, y_test),
          shuffle=True,
          callbacks=callbacks)

# give a rough idea of model performance
score = model.evaluate(x_test, y_test, verbose=0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])