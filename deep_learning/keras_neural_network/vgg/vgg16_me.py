from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.models import model_from_json
from keras.callbacks import ReduceLROnPlateau, CSVLogger, EarlyStopping
from keras.regularizers import l2

"""
parameters
"""
# dimensions of images.
img_width, img_height = 256, 256
# directory
train_data_dir = '../neural_network/data/train'
validation_data_dir = '../neural_network/data/test'


#train_data_dir = 'data/train'
#validation_data_dir = 'data/test'

# parameters
nb_train_samples = 6000
nb_validation_samples = 2000
nb_epoch = 101
nb_batch = 32

# callbacks for stable loss
lr_reducer = ReduceLROnPlateau(factor=0.1, cooldown=0, patience=5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.0001, patience=15)
csv_logger = CSVLogger('vgg16.csv')

'''
build the vgg-16 model
argument: config -- one of A(11), B(13), C(16), D(16), or E(19)
return value: model object
'''
def vgg_build(config):
	model = Sequential()

	# conv1_1
	model.add(ZeroPadding2D(padding = (1, 1)))
	model.add(Convolution2D(64, (3, 3), input_shape = (img_width, img_height, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
	model.add(Activation('relu'))
	if config != 'A':
	# conv1_2
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(64, (3, 3), kernelkernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	# pool1
	model.add(MaxPooling2D(pool_size = 2, strides = 2))

	# conv2_1
	model.add(ZeroPadding2D(padding = (1, 1)))
	model.add(Convolution2D(128, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
	model.add(Activation('relu'))
	# conv2_2
	if config != 'A':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(128, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	# pool2
	model.add(MaxPooling2D(pool_size = 2, strides = 2))

	# conv3_1
	model.add(ZeroPadding2D(padding = (1, 1)))
	model.add(Convolution2D(256, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
	model.add(Activation('relu'))
	# conv3_2
	model.add(ZeroPadding2D(padding = (1, 1)))
	model.add(Convolution2D(256, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
	model.add(Activation('relu'))
	# conv3_3
	if config == 'C':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(256, (1, 1), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	elif config == 'D':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(256, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	elif config == 'E':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(256, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))

		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(256, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))		
	# pool3
	model.add(MaxPooling2D(pool_size = (2, 2), strides = 2))

	# conv4_1
	model.add(ZeroPadding2D(padding = (1, 1)))
	model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
	model.add(Activation('relu'))
	# conv4_2
	model.add(ZeroPadding2D(padding = (1, 1)))
	model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
	model.add(Activation('relu'))
	# conv4_3
	if config == 'C':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(512, (1, 1), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	elif config == 'D':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	elif config == 'E':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
		
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	# pool4
	model.add(MaxPooling2D(pool_size = (2, 2), strides = 2))

	# conv5_1
	model.add(ZeroPadding2D(padding = (1, 1)))
	model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
	model.add(Activation('relu'))
	# conv5_2
	model.add(ZeroPadding2D(padding = (1, 1)))
	model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
	model.add(Activation('relu'))
	# conv5_3
	if config == 'C':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(512, (1, 1), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	elif config == 'D':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	elif config == 'E':
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
		
		model.add(ZeroPadding2D(padding = (1, 1)))
		model.add(Convolution2D(512, (3, 3), kernel_regularizer=l2(5e-4), kernel_initializer = 'glorot_normal'))
		model.add(Activation('relu'))
	# pool5
	model.add(MaxPooling2D(pool_size = (2, 2), strides = 2))

	model.add(Flatten())

	# fc6
	model.add(Dense(4096))
	model.add(Activation('relu'))
	model.add(Dropout(0.5))

	# fc7
	model.add(Dense(4096))
	model.add(Activation('relu'))
	model.add(Dropout(0.5))

	# fc8
	model.add(Dense(1000))
	model.add(Dense(1))
	model.add(Activation('softmax'))

	return model


'''
build and compile model
'''
model = vgg_build('A')
myoptimizer = optimizers.SGD(lr=0.01, momentum = 0.9, nesterov = True)
model.compile(loss='binary_crossentropy',
              optimizer=myoptimizer,
              metrics=['accuracy'])


"""
load data
"""
# training data
train_datagen = ImageDataGenerator(rescale=1./255)
# 
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=nb_batch,
        class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=nb_batch,
        class_mode='binary')

model.fit_generator(
        train_generator,
        samples_per_epoch=nb_train_samples,
        nb_epoch=nb_epoch,
        validation_data=validation_generator,
        nb_val_samples=nb_validation_samples,
        callbacks=[lr_reducer,early_stopper, csv_logger])

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("weights.h5")
print("Saved model to disk")
