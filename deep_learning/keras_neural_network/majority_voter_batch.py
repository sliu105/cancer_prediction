import majority_voter
import glob
import os
import pandas as pd
# CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))
# for resnet (new dataset) results: 
results_folder = 'results_splitted_ds'

if __name__ == "__main__":

	# Subroutine to find subfolders
    # subfolders = []
    # for root, dirs, files in os.walk(results_folder, topdown=False):
    #     for name in dirs:
    #         subfolders.append(name)
    subfolders = ['11_9_resnet_0blk/ds4', 'rerun_11_9_resnet_1blk/ds4', \
    '11_9_resnet_2blk/ds4', '11_9_resnet_3blk/ds4',\
    '11_16_resnet_1blk_dropout_0.2_ds2', '11_16_resnet_1blk_dropout_0.4_ds2']

    # subfolders = ['11_9_resnet_0blk/ds4']
    print(subfolders)

    # find the dataset corresponding each result folder
    # find the .csv file in results_folder
    filenames = os.listdir(results_folder)
    for file in filenames:
        if file.endswith('.csv'):
            logs = pd.read_csv(os.path.join(results_folder, file))

    # match folders with datasets
    # ds: key: folder name; value: dataset name
    ds = {}
    for i in range(len(logs['folder'])):
        ds[logs['folder'][i]] = logs['dataset'][i]
    # # For each subfolder
    for subfolder in subfolders:
        majority_voter.vote(os.path.join(results_folder, subfolder), ds[subfolder])

 #        df = pd.read_csv(SCRIPT_LOCATION + "/" + folder_input + subfolder + "/" + "labels.csv")
	# model_file_path, weights_file_path, test_data_path, patch_accuracy, image_accuracy = majority_vote(model_file_path, weights_file_path, test_data_path)

