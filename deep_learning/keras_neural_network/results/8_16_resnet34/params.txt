
resnet model: resnet34

optimizer: SGD (learning rate = 0.001, clipvalue = 0.01)

callback: ReduceLROnPlateau (factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
		  EarlyStopping(min_delta=0.001, patience=20)
