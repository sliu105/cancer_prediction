Bagging result of the following models:
['results/10_10_resnet50', 'bangqi_results/bangqi_dataset2', 'results/10_10_resnet50', 'results/9_16_resnet18_shuijing', 'bangqi_results/bangqi_dataset2_sgd']

probabilities for 2 classes are:
('image name:', 'A18')
('true label:', 0)
('result of model', 0, ': ', [138, 12, 0])
[138, 12, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [138, 12, 0])
[138, 12, 0]
('result of model', 3, ': ', [134, 16, 0])
[134, 16, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'K16')
('true label:', 0)
('result of model', 0, ': ', [125, 75, 0])
[125, 75, 0]
('result of model', 1, ': ', [0, 200, 0])
[0, 200, 0]
('result of model', 2, ': ', [125, 75, 0])
[125, 75, 0]
('result of model', 3, ': ', [124, 76, 0])
[124, 76, 0]
('result of model', 4, ': ', [0, 200, 0])
[0, 200, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'G7')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G6')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G4')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G2')
('true label:', 0)
('result of model', 0, ': ', [7, 143, 0])
[7, 143, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [7, 143, 0])
[7, 143, 0]
('result of model', 3, ': ', [34, 116, 0])
[34, 116, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'D13')
('true label:', 1)
('result of model', 0, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 3, ': ', [1, 99, 1])
[1, 99, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G9')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G8')
('true label:', 1)
('result of model', 0, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J9')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J4')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J5')
('true label:', 1)
('result of model', 0, ': ', [6, 94, 1])
[6, 94, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [6, 94, 1])
[6, 94, 1]
('result of model', 3, ': ', [22, 78, 1])
[22, 78, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J1')
('true label:', 1)
('result of model', 0, ': ', [9, 91, 1])
[9, 91, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [9, 91, 1])
[9, 91, 1]
('result of model', 3, ': ', [19, 81, 1])
[19, 81, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J2')
('true label:', 1)
('result of model', 0, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 3, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J3')
('true label:', 1)
('result of model', 0, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B4')
('true label:', 1)
('result of model', 0, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B6')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B7')
('true label:', 1)
('result of model', 0, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 3, ': ', [45, 55, 1])
[45, 55, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B1')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 3, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B9')
('true label:', 1)
('result of model', 0, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B13')
('true label:', 1)
('result of model', 0, ': ', [48, 2, 1])
[48, 2, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [48, 2, 1])
[48, 2, 1]
('result of model', 3, ': ', [47, 3, 1])
[47, 3, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'L14')
('true label:', 0)
('result of model', 0, ': ', [133, 17, 0])
[133, 17, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [133, 17, 0])
[133, 17, 0]
('result of model', 3, ': ', [133, 17, 0])
[133, 17, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'L12')
('true label:', 0)
('result of model', 0, ': ', [135, 15, 0])
[135, 15, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [135, 15, 0])
[135, 15, 0]
('result of model', 3, ': ', [136, 14, 0])
[136, 14, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'M1')
('true label:', 0)
('result of model', 0, ': ', [119, 31, 0])
[119, 31, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [119, 31, 0])
[119, 31, 0]
('result of model', 3, ': ', [112, 38, 0])
[112, 38, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'M3')
('true label:', 0)
('result of model', 0, ': ', [132, 18, 0])
[132, 18, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [132, 18, 0])
[132, 18, 0]
('result of model', 3, ': ', [136, 14, 0])
[136, 14, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'E5')
('true label:', 1)
('result of model', 0, ': ', [40, 10, 1])
[40, 10, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [40, 10, 1])
[40, 10, 1]
('result of model', 3, ': ', [42, 8, 1])
[42, 8, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'E4')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E7')
('true label:', 1)
('result of model', 0, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 3, ': ', [26, 24, 1])
[26, 24, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'E3')
('true label:', 1)
('result of model', 0, ': ', [9, 91, 1])
[9, 91, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [9, 91, 1])
[9, 91, 1]
('result of model', 3, ': ', [5, 95, 1])
[5, 95, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E2')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'H18')
('true label:', 0)
('result of model', 0, ': ', [140, 10, 0])
[140, 10, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [140, 10, 0])
[140, 10, 0]
('result of model', 3, ': ', [134, 16, 0])
[134, 16, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'H13')
('true label:', 1)
('result of model', 0, ': ', [20, 30, 1])
[20, 30, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [20, 30, 1])
[20, 30, 1]
('result of model', 3, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F12')
('true label:', 1)
('result of model', 0, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F10')
('true label:', 1)
('result of model', 0, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 3, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F14')
('true label:', 1)
('result of model', 0, ': ', [4, 96, 1])
[4, 96, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [4, 96, 1])
[4, 96, 1]
('result of model', 3, ': ', [5, 95, 1])
[5, 95, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'H9')
('true label:', 1)
('result of model', 0, ': ', [13, 137, 1])
[13, 137, 1]
('result of model', 1, ': ', [0, 150, 1])
[0, 150, 1]
('result of model', 2, ': ', [13, 137, 1])
[13, 137, 1]
('result of model', 3, ': ', [33, 117, 1])
[33, 117, 1]
('result of model', 4, ': ', [0, 150, 1])
[0, 150, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'H3')
('true label:', 1)
('result of model', 0, ': ', [34, 16, 1])
[34, 16, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [34, 16, 1])
[34, 16, 1]
('result of model', 3, ': ', [29, 21, 1])
[29, 21, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'H5')
('true label:', 1)
('result of model', 0, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K13')
('true label:', 0)
('result of model', 0, ': ', [139, 11, 0])
[139, 11, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [139, 11, 0])
[139, 11, 0]
('result of model', 3, ': ', [136, 14, 0])
[136, 14, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'K12')
('true label:', 0)
('result of model', 0, ': ', [101, 99, 0])
[101, 99, 0]
('result of model', 1, ': ', [0, 200, 0])
[0, 200, 0]
('result of model', 2, ': ', [101, 99, 0])
[101, 99, 0]
('result of model', 3, ': ', [96, 104, 0])
[96, 104, 0]
('result of model', 4, ': ', [0, 200, 0])
[0, 200, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'K11')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D11')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K15')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K14')
('true label:', 0)
('result of model', 0, ': ', [138, 12, 0])
[138, 12, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [138, 12, 0])
[138, 12, 0]
('result of model', 3, ': ', [137, 13, 0])
[137, 13, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'B14')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B12')
('true label:', 1)
('result of model', 0, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 3, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K6')
('true label:', 0)
('result of model', 0, ': ', [80, 70, 0])
[80, 70, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [80, 70, 0])
[80, 70, 0]
('result of model', 3, ': ', [95, 55, 0])
[95, 55, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'K5')
('true label:', 0)
('result of model', 0, ': ', [128, 22, 0])
[128, 22, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [128, 22, 0])
[128, 22, 0]
('result of model', 3, ': ', [115, 35, 0])
[115, 35, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'B11')
('true label:', 1)
('result of model', 0, ': ', [31, 19, 1])
[31, 19, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [31, 19, 1])
[31, 19, 1]
('result of model', 3, ': ', [39, 11, 1])
[39, 11, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'K9')
('true label:', 0)
('result of model', 0, ': ', [81, 119, 0])
[81, 119, 0]
('result of model', 1, ': ', [0, 200, 0])
[0, 200, 0]
('result of model', 2, ': ', [81, 119, 0])
[81, 119, 0]
('result of model', 3, ': ', [79, 121, 0])
[79, 121, 0]
('result of model', 4, ': ', [0, 200, 0])
[0, 200, 0]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'C9')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C8')
('true label:', 0)
('result of model', 0, ': ', [81, 69, 0])
[81, 69, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [81, 69, 0])
[81, 69, 0]
('result of model', 3, ': ', [109, 41, 0])
[109, 41, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'C2')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C1')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J12')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J13')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C5')
('true label:', 1)
('result of model', 0, ': ', [38, 62, 1])
[38, 62, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [38, 62, 1])
[38, 62, 1]
('result of model', 3, ': ', [44, 56, 1])
[44, 56, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C4')
('true label:', 1)
('result of model', 0, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 3, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I17')
('true label:', 1)
('result of model', 0, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 3, ': ', [20, 30, 1])
[20, 30, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I11')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I10')
('true label:', 1)
('result of model', 0, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 3, ': ', [23, 27, 1])
[23, 27, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I13')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G13')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G11')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G10')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F2')
('true label:', 0)
('result of model', 0, ': ', [70, 80, 0])
[70, 80, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [70, 80, 0])
[70, 80, 0]
('result of model', 3, ': ', [78, 72, 0])
[78, 72, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'F3')
('true label:', 1)
('result of model', 0, ': ', [29, 21, 1])
[29, 21, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [29, 21, 1])
[29, 21, 1]
('result of model', 3, ': ', [31, 19, 1])
[31, 19, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'F5')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F7')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F9')
('true label:', 1)
('result of model', 0, ': ', [47, 3, 1])
[47, 3, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [47, 3, 1])
[47, 3, 1]
('result of model', 3, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'J10')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E13')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E15')
('true label:', 1)
('result of model', 0, ': ', [36, 14, 1])
[36, 14, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [36, 14, 1])
[36, 14, 1]
('result of model', 3, ': ', [26, 24, 1])
[26, 24, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'E14')
('true label:', 0)
('result of model', 0, ': ', [99, 101, 0])
[99, 101, 0]
('result of model', 1, ': ', [0, 200, 0])
[0, 200, 0]
('result of model', 2, ': ', [99, 101, 0])
[99, 101, 0]
('result of model', 3, ': ', [105, 95, 0])
[105, 95, 0]
('result of model', 4, ': ', [0, 200, 0])
[0, 200, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'E16')
('true label:', 1)
('result of model', 0, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 3, ': ', [1, 99, 1])
[1, 99, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C12')
('true label:', 0)
('result of model', 0, ': ', [150, 0, 0])
[150, 0, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [150, 0, 0])
[150, 0, 0]
('result of model', 3, ': ', [150, 0, 0])
[150, 0, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'C11')
('true label:', 1)
('result of model', 0, ': ', [35, 65, 1])
[35, 65, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [35, 65, 1])
[35, 65, 1]
('result of model', 3, ': ', [36, 64, 1])
[36, 64, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C14')
('true label:', 1)
('result of model', 0, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 3, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'A11')
('true label:', 1)
('result of model', 0, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I3')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I2')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I5')
('true label:', 1)
('result of model', 0, ': ', [12, 88, 1])
[12, 88, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [12, 88, 1])
[12, 88, 1]
('result of model', 3, ': ', [16, 84, 1])
[16, 84, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I4')
('true label:', 0)
('result of model', 0, ': ', [90, 60, 0])
[90, 60, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [90, 60, 0])
[90, 60, 0]
('result of model', 3, ': ', [92, 58, 0])
[92, 58, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'I7')
('true label:', 1)
('result of model', 0, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 3, ': ', [21, 29, 1])
[21, 29, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I6')
('true label:', 1)
('result of model', 0, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 3, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'A3')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'A2')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'A6')
('true label:', 0)
('result of model', 0, ': ', [42, 208, 0])
[42, 208, 0]
('result of model', 1, ': ', [0, 250, 0])
[0, 250, 0]
('result of model', 2, ': ', [42, 208, 0])
[42, 208, 0]
('result of model', 3, ': ', [48, 202, 0])
[48, 202, 0]
('result of model', 4, ': ', [0, 250, 0])
[0, 250, 0]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'L6')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'L4')
('true label:', 0)
('result of model', 0, ': ', [141, 9, 0])
[141, 9, 0]
('result of model', 1, ': ', [0, 150, 0])
[0, 150, 0]
('result of model', 2, ': ', [141, 9, 0])
[141, 9, 0]
('result of model', 3, ': ', [112, 38, 0])
[112, 38, 0]
('result of model', 4, ': ', [0, 150, 0])
[0, 150, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'L5')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'L2')
('true label:', 0)
('result of model', 0, ': ', [148, 52, 0])
[148, 52, 0]
('result of model', 1, ': ', [0, 200, 0])
[0, 200, 0]
('result of model', 2, ': ', [148, 52, 0])
[148, 52, 0]
('result of model', 3, ': ', [147, 53, 0])
[147, 53, 0]
('result of model', 4, ': ', [0, 200, 0])
[0, 200, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'D9')
('true label:', 1)
('result of model', 0, ': ', [8, 92, 1])
[8, 92, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [8, 92, 1])
[8, 92, 1]
('result of model', 3, ': ', [7, 93, 1])
[7, 93, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D6')
('true label:', 1)
('result of model', 0, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 3, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D7')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D5')
('true label:', 1)
('result of model', 0, ': ', [4, 96, 1])
[4, 96, 1]
('result of model', 1, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 2, ': ', [4, 96, 1])
[4, 96, 1]
('result of model', 3, ': ', [0, 100, 1])
[0, 100, 1]
('result of model', 4, ': ', [0, 100, 1])
[0, 100, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D1')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 2, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 3, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 4, ': ', [0, 50, 1])
[0, 50, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
Average Patch-wide accuracy is: 0.726390243902
Average Image-wide accuracy given by majority vote is: 0.835051546392
('correct_bagging:', 84)
Bagging accuracy given by input models is: 0.865979381443
