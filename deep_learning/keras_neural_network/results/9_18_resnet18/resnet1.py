from __future__ import print_function
from keras.preprocessing.image import ImageDataGenerator
from keras.models import model_from_json

from keras.datasets import cifar10

from keras.utils import np_utils
from keras.callbacks import ReduceLROnPlateau, CSVLogger, EarlyStopping
from keras import optimizers

import numpy as np
import resnet

"""
parameters
"""
# dimensions of images.
img_width, img_height = 128, 128
img_channels = 3
# directory
train_data_dir = '/home/kexu6/ml/cancer-prediction/deep_learning/datasets/second_dataset/train'
validation_data_dir = '/home/kexu6/ml/cancer-prediction/deep_learning/datasets/second_dataset/test'
# parameters
nb_train_samples = 24700
nb_validation_samples = 8200
nb_epoch = 100
nb_batch = 32
nb_classes = 2

# callbacks for stable loss
lr_reducer = ReduceLROnPlateau (factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.0001, patience=25)
csv_logger = CSVLogger('/home/kexu6/ml/cancer-prediction/deep_learning/keras_neural_network/results/9_16_resnet18/kexu_run/para_row10/resnet18.csv')

# build model
model = resnet.ResnetBuilder.build_resnet_18((img_channels, img_width, img_height), nb_classes)
myoptimizer = optimizers.SGD(lr=0.1, momentum = 0.9, nesterov = True)
# myoptimizer = optimizers.Adam(lr=0.1, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
model.compile(loss='categorical_crossentropy',
              optimizer=myoptimizer,
              metrics=['accuracy'])

"""
load data
"""
# training data
train_datagen = ImageDataGenerator(rescale=1./255)
# 
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=nb_batch,
        class_mode='categorical')

validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=nb_batch,
        class_mode='categorical')


model.fit_generator(
        train_generator,
        samples_per_epoch=nb_train_samples,
        nb_epoch=nb_epoch,
        # validation_split = 0.25,
        validation_data=validation_generator,
        nb_val_samples=nb_validation_samples,
        callbacks=[lr_reducer, csv_logger])

model_json = model.to_json()
with open("/home/kexu6/ml/cancer-prediction/deep_learning/keras_neural_network/results/9_16_resnet18/kexu_run/para_row10/model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("/home/kexu6/ml/cancer-prediction/deep_learning/keras_neural_network/results/9_16_resnet18/kexu_run/para_row10/weights.h5")
print("Saved model to disk")
