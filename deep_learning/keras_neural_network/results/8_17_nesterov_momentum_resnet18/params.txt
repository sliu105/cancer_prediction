resnet model: resnet18

optimizer: SGD (learning rate = 0.001, clipvalue = 0.005, momentum = 0.9, nesterov = True)

callback: ReduceLROnPlateau (factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)

result: not much different from the result without using nesterov momentum :(