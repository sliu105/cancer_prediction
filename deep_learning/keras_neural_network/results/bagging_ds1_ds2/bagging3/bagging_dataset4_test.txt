Bagging result of the following models:
['bangqi_results/bangqi_6_9_1_44pm_1/', 'results/8_18_resnet18_l2=1e-5/', 'results/10_5_resnet50/']

probabilities for 2 classes are:
('image name:', 'AP8')
('true label:', 0)
('result of model', 0, ': ', [95, 5, 0])
[95, 5, 0]
('result of model', 1, ': ', [34, 66, 0])
[34, 66, 0]
('result of model', 2, ': ', [11, 89, 0])
[11, 89, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BV4')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [43, 57, 1])
[43, 57, 1]
('result of model', 2, ': ', [17, 83, 1])
[17, 83, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AP2')
('true label:', 1)
('result of model', 0, ': ', [94, 6, 1])
[94, 6, 1]
('result of model', 1, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 2, ': ', [19, 81, 1])
[19, 81, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AP1')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [46, 54, 1])
[46, 54, 1]
('result of model', 2, ': ', [12, 88, 1])
[12, 88, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BN3')
('true label:', 1)
('result of model', 0, ': ', [72, 28, 1])
[72, 28, 1]
('result of model', 1, ': ', [40, 60, 1])
[40, 60, 1]
('result of model', 2, ': ', [10, 90, 1])
[10, 90, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BY2')
('true label:', 1)
('result of model', 0, ': ', [76, 24, 1])
[76, 24, 1]
('result of model', 1, ': ', [45, 55, 1])
[45, 55, 1]
('result of model', 2, ': ', [16, 84, 1])
[16, 84, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BN6')
('true label:', 0)
('result of model', 0, ': ', [42, 58, 0])
[42, 58, 0]
('result of model', 1, ': ', [45, 55, 0])
[45, 55, 0]
('result of model', 2, ': ', [10, 90, 0])
[10, 90, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 0)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BY1')
('true label:', 1)
('result of model', 0, ': ', [60, 40, 1])
[60, 40, 1]
('result of model', 1, ': ', [37, 63, 1])
[37, 63, 1]
('result of model', 2, ': ', [15, 85, 1])
[15, 85, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BQ6')
('true label:', 0)
('result of model', 0, ': ', [70, 30, 0])
[70, 30, 0]
('result of model', 1, ': ', [47, 53, 0])
[47, 53, 0]
('result of model', 2, ': ', [9, 91, 0])
[9, 91, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BQ7')
('true label:', 0)
('result of model', 0, ': ', [92, 8, 0])
[92, 8, 0]
('result of model', 1, ': ', [35, 65, 0])
[35, 65, 0]
('result of model', 2, ': ', [12, 88, 0])
[12, 88, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BF6')
('true label:', 0)
('result of model', 0, ': ', [95, 5, 0])
[95, 5, 0]
('result of model', 1, ': ', [34, 66, 0])
[34, 66, 0]
('result of model', 2, ': ', [6, 94, 0])
[6, 94, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BW2')
('true label:', 1)
('result of model', 0, ': ', [86, 14, 1])
[86, 14, 1]
('result of model', 1, ': ', [40, 60, 1])
[40, 60, 1]
('result of model', 2, ': ', [17, 83, 1])
[17, 83, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BW6')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [42, 58, 0])
[42, 58, 0]
('result of model', 2, ': ', [12, 88, 0])
[12, 88, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AW7')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [38, 62, 0])
[38, 62, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AW4')
('true label:', 1)
('result of model', 0, ': ', [96, 4, 1])
[96, 4, 1]
('result of model', 1, ': ', [39, 61, 1])
[39, 61, 1]
('result of model', 2, ': ', [21, 79, 1])
[21, 79, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AO1')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [41, 59, 1])
[41, 59, 1]
('result of model', 2, ': ', [23, 77, 1])
[23, 77, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AO8')
('true label:', 0)
('result of model', 0, ': ', [62, 38, 0])
[62, 38, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BZ1')
('true label:', 1)
('result of model', 0, ': ', [88, 12, 1])
[88, 12, 1]
('result of model', 1, ': ', [34, 66, 1])
[34, 66, 1]
('result of model', 2, ': ', [22, 78, 1])
[22, 78, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AX8')
('true label:', 0)
('result of model', 0, ': ', [97, 3, 0])
[97, 3, 0]
('result of model', 1, ': ', [33, 67, 0])
[33, 67, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'J1')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [34, 66, 1])
[34, 66, 1]
('result of model', 2, ': ', [9, 91, 1])
[9, 91, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'CC3')
('true label:', 1)
('result of model', 0, ': ', [98, 2, 1])
[98, 2, 1]
('result of model', 1, ': ', [47, 53, 1])
[47, 53, 1]
('result of model', 2, ': ', [16, 84, 1])
[16, 84, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'B4')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [39, 61, 1])
[39, 61, 1]
('result of model', 2, ': ', [11, 89, 1])
[11, 89, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'B6')
('true label:', 0)
('result of model', 0, ': ', [97, 3, 0])
[97, 3, 0]
('result of model', 1, ': ', [37, 63, 0])
[37, 63, 0]
('result of model', 2, ': ', [17, 83, 0])
[17, 83, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'B1')
('true label:', 1)
('result of model', 0, ': ', [95, 5, 1])
[95, 5, 1]
('result of model', 1, ': ', [26, 74, 1])
[26, 74, 1]
('result of model', 2, ': ', [16, 84, 1])
[16, 84, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'B2')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [38, 62, 1])
[38, 62, 1]
('result of model', 2, ': ', [18, 82, 1])
[18, 82, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BG7')
('true label:', 0)
('result of model', 0, ': ', [48, 52, 0])
[48, 52, 0]
('result of model', 1, ': ', [47, 53, 0])
[47, 53, 0]
('result of model', 2, ': ', [8, 92, 0])
[8, 92, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 0)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AG7')
('true label:', 0)
('result of model', 0, ': ', [95, 5, 0])
[95, 5, 0]
('result of model', 1, ': ', [42, 58, 0])
[42, 58, 0]
('result of model', 2, ': ', [14, 86, 0])
[14, 86, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AG5')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [50, 50, 0])
[50, 50, 0]
('result of model', 2, ': ', [10, 90, 0])
[10, 90, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BT4')
('true label:', 1)
('result of model', 0, ': ', [86, 14, 1])
[86, 14, 1]
('result of model', 1, ': ', [26, 74, 1])
[26, 74, 1]
('result of model', 2, ': ', [8, 92, 1])
[8, 92, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AV4')
('true label:', 1)
('result of model', 0, ': ', [78, 22, 1])
[78, 22, 1]
('result of model', 1, ': ', [37, 63, 1])
[37, 63, 1]
('result of model', 2, ': ', [18, 82, 1])
[18, 82, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AV2')
('true label:', 1)
('result of model', 0, ': ', [79, 21, 1])
[79, 21, 1]
('result of model', 1, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 2, ': ', [10, 90, 1])
[10, 90, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AV3')
('true label:', 1)
('result of model', 0, ': ', [63, 37, 1])
[63, 37, 1]
('result of model', 1, ': ', [47, 53, 1])
[47, 53, 1]
('result of model', 2, ': ', [16, 84, 1])
[16, 84, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'CB3')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [33, 67, 1])
[33, 67, 1]
('result of model', 2, ': ', [15, 85, 1])
[15, 85, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AN6')
('true label:', 0)
('result of model', 0, ': ', [97, 3, 0])
[97, 3, 0]
('result of model', 1, ': ', [43, 57, 0])
[43, 57, 0]
('result of model', 2, ': ', [9, 91, 0])
[9, 91, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'M2')
('true label:', 1)
('result of model', 0, ': ', [95, 5, 1])
[95, 5, 1]
('result of model', 1, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 2, ': ', [12, 88, 1])
[12, 88, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BL4')
('true label:', 1)
('result of model', 0, ': ', [68, 32, 1])
[68, 32, 1]
('result of model', 1, ': ', [35, 65, 1])
[35, 65, 1]
('result of model', 2, ': ', [14, 86, 1])
[14, 86, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BL6')
('true label:', 0)
('result of model', 0, ': ', [82, 18, 0])
[82, 18, 0]
('result of model', 1, ': ', [40, 60, 0])
[40, 60, 0]
('result of model', 2, ': ', [10, 90, 0])
[10, 90, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BL3')
('true label:', 1)
('result of model', 0, ': ', [81, 19, 1])
[81, 19, 1]
('result of model', 1, ': ', [43, 57, 1])
[43, 57, 1]
('result of model', 2, ': ', [14, 86, 1])
[14, 86, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BL2')
('true label:', 1)
('result of model', 0, ': ', [92, 8, 1])
[92, 8, 1]
('result of model', 1, ': ', [40, 60, 1])
[40, 60, 1]
('result of model', 2, ': ', [21, 79, 1])
[21, 79, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BD7')
('true label:', 0)
('result of model', 0, ': ', [55, 45, 0])
[55, 45, 0]
('result of model', 1, ': ', [38, 62, 0])
[38, 62, 0]
('result of model', 2, ': ', [8, 92, 0])
[8, 92, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BD2')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [35, 65, 1])
[35, 65, 1]
('result of model', 2, ': ', [14, 86, 1])
[14, 86, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AL1')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 2, ': ', [20, 80, 1])
[20, 80, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BD8')
('true label:', 0)
('result of model', 0, ': ', [69, 31, 0])
[69, 31, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'E3')
('true label:', 1)
('result of model', 0, ': ', [94, 6, 1])
[94, 6, 1]
('result of model', 1, ': ', [39, 61, 1])
[39, 61, 1]
('result of model', 2, ': ', [21, 79, 1])
[21, 79, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'P3')
('true label:', 1)
('result of model', 0, ': ', [96, 4, 1])
[96, 4, 1]
('result of model', 1, ': ', [40, 60, 1])
[40, 60, 1]
('result of model', 2, ': ', [16, 84, 1])
[16, 84, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BG6')
('true label:', 0)
('result of model', 0, ': ', [97, 3, 0])
[97, 3, 0]
('result of model', 1, ': ', [43, 57, 0])
[43, 57, 0]
('result of model', 2, ': ', [7, 93, 0])
[7, 93, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BU5')
('true label:', 0)
('result of model', 0, ': ', [98, 2, 0])
[98, 2, 0]
('result of model', 1, ': ', [44, 56, 0])
[44, 56, 0]
('result of model', 2, ': ', [4, 96, 0])
[4, 96, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BM3')
('true label:', 1)
('result of model', 0, ': ', [96, 4, 1])
[96, 4, 1]
('result of model', 1, ': ', [41, 59, 1])
[41, 59, 1]
('result of model', 2, ': ', [17, 83, 1])
[17, 83, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'CE1')
('true label:', 1)
('result of model', 0, ': ', [88, 12, 1])
[88, 12, 1]
('result of model', 1, ': ', [33, 67, 1])
[33, 67, 1]
('result of model', 2, ': ', [11, 89, 1])
[11, 89, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BM8')
('true label:', 0)
('result of model', 0, ': ', [95, 5, 0])
[95, 5, 0]
('result of model', 1, ': ', [32, 68, 0])
[32, 68, 0]
('result of model', 2, ': ', [15, 85, 0])
[15, 85, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AE7')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'CE6')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [15, 85, 0])
[15, 85, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'H3')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 2, ': ', [9, 91, 1])
[9, 91, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'H5')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [42, 58, 0])
[42, 58, 0]
('result of model', 2, ': ', [16, 84, 0])
[16, 84, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BE8')
('true label:', 0)
('result of model', 0, ': ', [86, 14, 0])
[86, 14, 0]
('result of model', 1, ': ', [36, 64, 0])
[36, 64, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BE5')
('true label:', 0)
('result of model', 0, ': ', [97, 3, 0])
[97, 3, 0]
('result of model', 1, ': ', [42, 58, 0])
[42, 58, 0]
('result of model', 2, ': ', [11, 89, 0])
[11, 89, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BN7')
('true label:', 0)
('result of model', 0, ': ', [81, 19, 0])
[81, 19, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [14, 86, 0])
[14, 86, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BR1')
('true label:', 1)
('result of model', 0, ': ', [94, 6, 1])
[94, 6, 1]
('result of model', 1, ': ', [29, 71, 1])
[29, 71, 1]
('result of model', 2, ': ', [12, 88, 1])
[12, 88, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'K2')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [38, 62, 1])
[38, 62, 1]
('result of model', 2, ': ', [16, 84, 1])
[16, 84, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'CD7')
('true label:', 0)
('result of model', 0, ': ', [97, 3, 0])
[97, 3, 0]
('result of model', 1, ': ', [36, 64, 0])
[36, 64, 0]
('result of model', 2, ': ', [12, 88, 0])
[12, 88, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BJ2')
('true label:', 1)
('result of model', 0, ': ', [77, 23, 1])
[77, 23, 1]
('result of model', 1, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 2, ': ', [16, 84, 1])
[16, 84, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'K5')
('true label:', 0)
('result of model', 0, ': ', [90, 10, 0])
[90, 10, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [9, 91, 0])
[9, 91, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AL6')
('true label:', 0)
('result of model', 0, ': ', [96, 4, 0])
[96, 4, 0]
('result of model', 1, ': ', [40, 60, 0])
[40, 60, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BJ8')
('true label:', 0)
('result of model', 0, ': ', [99, 1, 0])
[99, 1, 0]
('result of model', 1, ': ', [35, 65, 0])
[35, 65, 0]
('result of model', 2, ': ', [14, 86, 0])
[14, 86, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AD6')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [14, 86, 0])
[14, 86, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AS3')
('true label:', 1)
('result of model', 0, ': ', [94, 6, 1])
[94, 6, 1]
('result of model', 1, ': ', [43, 57, 1])
[43, 57, 1]
('result of model', 2, ': ', [14, 86, 1])
[14, 86, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AS7')
('true label:', 0)
('result of model', 0, ': ', [98, 2, 0])
[98, 2, 0]
('result of model', 1, ': ', [31, 69, 0])
[31, 69, 0]
('result of model', 2, ': ', [11, 89, 0])
[11, 89, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'BZ5')
('true label:', 0)
('result of model', 0, ': ', [58, 42, 0])
[58, 42, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [8, 92, 0])
[8, 92, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'F6')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [45, 55, 0])
[45, 55, 0]
('result of model', 2, ': ', [14, 86, 0])
[14, 86, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'M4')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [34, 66, 1])
[34, 66, 1]
('result of model', 2, ': ', [21, 79, 1])
[21, 79, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AK3')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [33, 67, 1])
[33, 67, 1]
('result of model', 2, ': ', [15, 85, 1])
[15, 85, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AK2')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [39, 61, 1])
[39, 61, 1]
('result of model', 2, ': ', [18, 82, 1])
[18, 82, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AK1')
('true label:', 1)
('result of model', 0, ': ', [87, 13, 1])
[87, 13, 1]
('result of model', 1, ': ', [44, 56, 1])
[44, 56, 1]
('result of model', 2, ': ', [18, 82, 1])
[18, 82, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BZ8')
('true label:', 0)
('result of model', 0, ': ', [95, 5, 0])
[95, 5, 0]
('result of model', 1, ': ', [44, 56, 0])
[44, 56, 0]
('result of model', 2, ': ', [8, 92, 0])
[8, 92, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'Q2')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [38, 62, 1])
[38, 62, 1]
('result of model', 2, ': ', [13, 87, 1])
[13, 87, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AR4')
('true label:', 1)
('result of model', 0, ': ', [74, 26, 1])
[74, 26, 1]
('result of model', 1, ': ', [30, 70, 1])
[30, 70, 1]
('result of model', 2, ': ', [18, 82, 1])
[18, 82, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AR5')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [38, 62, 0])
[38, 62, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AR7')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [40, 60, 0])
[40, 60, 0]
('result of model', 2, ': ', [9, 91, 0])
[9, 91, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'CC8')
('true label:', 0)
('result of model', 0, ': ', [99, 1, 0])
[99, 1, 0]
('result of model', 1, ': ', [43, 57, 0])
[43, 57, 0]
('result of model', 2, ': ', [6, 94, 0])
[6, 94, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'CF7')
('true label:', 0)
('result of model', 0, ': ', [97, 3, 0])
[97, 3, 0]
('result of model', 1, ': ', [35, 65, 0])
[35, 65, 0]
('result of model', 2, ': ', [13, 87, 0])
[13, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'CF5')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [39, 61, 0])
[39, 61, 0]
('result of model', 2, ': ', [12, 88, 0])
[12, 88, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AJ6')
('true label:', 0)
('result of model', 0, ': ', [100, 0, 0])
[100, 0, 0]
('result of model', 1, ': ', [37, 63, 0])
[37, 63, 0]
('result of model', 2, ': ', [8, 92, 0])
[8, 92, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AQ7')
('true label:', 0)
('result of model', 0, ': ', [78, 22, 0])
[78, 22, 0]
('result of model', 1, ': ', [34, 66, 0])
[34, 66, 0]
('result of model', 2, ': ', [8, 92, 0])
[8, 92, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'L2')
('true label:', 1)
('result of model', 0, ': ', [95, 5, 1])
[95, 5, 1]
('result of model', 1, ': ', [32, 68, 1])
[32, 68, 1]
('result of model', 2, ': ', [12, 88, 1])
[12, 88, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BI4')
('true label:', 1)
('result of model', 0, ': ', [95, 5, 1])
[95, 5, 1]
('result of model', 1, ': ', [46, 54, 1])
[46, 54, 1]
('result of model', 2, ': ', [12, 88, 1])
[12, 88, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'BA2')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [40, 60, 1])
[40, 60, 1]
('result of model', 2, ': ', [9, 91, 1])
[9, 91, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'AI8')
('true label:', 0)
('result of model', 0, ': ', [89, 11, 0])
[89, 11, 0]
('result of model', 1, ': ', [34, 66, 0])
[34, 66, 0]
('result of model', 2, ': ', [11, 89, 0])
[11, 89, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'CA1')
('true label:', 1)
('result of model', 0, ': ', [57, 43, 1])
[57, 43, 1]
('result of model', 1, ': ', [42, 58, 1])
[42, 58, 1]
('result of model', 2, ': ', [11, 89, 1])
[11, 89, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'D2')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [47, 53, 1])
[47, 53, 1]
('result of model', 2, ': ', [20, 80, 1])
[20, 80, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
('image name:', 'D1')
('true label:', 1)
('result of model', 0, ': ', [100, 0, 1])
[100, 0, 1]
('result of model', 1, ': ', [34, 66, 1])
[34, 66, 1]
('result of model', 2, ': ', [20, 80, 1])
[20, 80, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 1)
Average Patch-wide accuracy is: 0.493185185185
Average Image-wide accuracy given by majority vote is: 0.5
('correct_bagging:', 47)
Bagging accuracy given by input models is: 0.522222222222
