Bagging result of the following models:
['bangqi_results/bangqi_6_9_1_44pm_1/', 'results/8_18_resnet18_l2=1e-5/', 'results/10_5_resnet50/', 'results/9_27_resnet34_l2=1e-4/', 'bangqi_results/bangqi_6_13_12_18pm']

probabilities for 2 classes are:
('image name:', 'G6')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'G4')
('true label:', 0)
('result of model', 0, ': ', [46, 13, 0])
[46, 13, 0]
('result of model', 1, ': ', [38, 21, 0])
[38, 21, 0]
('result of model', 2, ': ', [22, 37, 0])
[22, 37, 0]
('result of model', 3, ': ', [27, 32, 0])
[27, 32, 0]
('result of model', 4, ': ', [13, 46, 0])
[13, 46, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'G1')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 2, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [8, 5, 1])
[8, 5, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'G9')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [3, 10, 1])
[3, 10, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'J8')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [3, 10, 1])
[3, 10, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J5')
('true label:', 1)
('result of model', 0, ': ', [2, 9, 1])
[2, 9, 1]
('result of model', 1, ': ', [5, 6, 1])
[5, 6, 1]
('result of model', 2, ': ', [4, 7, 1])
[4, 7, 1]
('result of model', 3, ': ', [9, 2, 1])
[9, 2, 1]
('result of model', 4, ': ', [3, 8, 1])
[3, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'J7')
('true label:', 1)
('result of model', 0, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'J3')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [4, 9, 1])
[4, 9, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'B4')
('true label:', 1)
('result of model', 0, ': ', [12, 1, 1])
[12, 1, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 4, ': ', [9, 4, 1])
[9, 4, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'B6')
('true label:', 1)
('result of model', 0, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [7, 6, 1])
[7, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'B1')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 3, ': ', [9, 4, 1])
[9, 4, 1]
('result of model', 4, ': ', [7, 6, 1])
[7, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'B2')
('true label:', 0)
('result of model', 0, ': ', [46, 0, 0])
[46, 0, 0]
('result of model', 1, ': ', [36, 10, 0])
[36, 10, 0]
('result of model', 2, ': ', [17, 29, 0])
[17, 29, 0]
('result of model', 3, ': ', [18, 28, 0])
[18, 28, 0]
('result of model', 4, ': ', [6, 40, 0])
[6, 40, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'B9')
('true label:', 1)
('result of model', 0, ': ', [3, 23, 1])
[3, 23, 1]
('result of model', 1, ': ', [4, 22, 1])
[4, 22, 1]
('result of model', 2, ': ', [3, 23, 1])
[3, 23, 1]
('result of model', 3, ': ', [10, 16, 1])
[10, 16, 1]
('result of model', 4, ': ', [6, 20, 1])
[6, 20, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'L16')
('true label:', 0)
('result of model', 0, ': ', [8, 38, 0])
[8, 38, 0]
('result of model', 1, ': ', [40, 6, 0])
[40, 6, 0]
('result of model', 2, ': ', [21, 25, 0])
[21, 25, 0]
('result of model', 3, ': ', [18, 28, 0])
[18, 28, 0]
('result of model', 4, ': ', [9, 37, 0])
[9, 37, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'K4')
('true label:', 1)
('result of model', 0, ': ', [0, 4, 1])
[0, 4, 1]
('result of model', 1, ': ', [0, 4, 1])
[0, 4, 1]
('result of model', 2, ': ', [1, 3, 1])
[1, 3, 1]
('result of model', 3, ': ', [3, 1, 1])
[3, 1, 1]
('result of model', 4, ': ', [1, 3, 1])
[1, 3, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'L11')
('true label:', 0)
('result of model', 0, ': ', [8, 38, 0])
[8, 38, 0]
('result of model', 1, ': ', [31, 15, 0])
[31, 15, 0]
('result of model', 2, ': ', [17, 29, 0])
[17, 29, 0]
('result of model', 3, ': ', [16, 30, 0])
[16, 30, 0]
('result of model', 4, ': ', [9, 37, 0])
[9, 37, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'M6')
('true label:', 0)
('result of model', 0, ': ', [14, 32, 0])
[14, 32, 0]
('result of model', 1, ': ', [31, 15, 0])
[31, 15, 0]
('result of model', 2, ': ', [10, 36, 0])
[10, 36, 0]
('result of model', 3, ': ', [19, 27, 0])
[19, 27, 0]
('result of model', 4, ': ', [7, 39, 0])
[7, 39, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'M1')
('true label:', 0)
('result of model', 0, ': ', [12, 34, 0])
[12, 34, 0]
('result of model', 1, ': ', [34, 12, 0])
[34, 12, 0]
('result of model', 2, ': ', [18, 28, 0])
[18, 28, 0]
('result of model', 3, ': ', [9, 37, 0])
[9, 37, 0]
('result of model', 4, ': ', [8, 38, 0])
[8, 38, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'E8')
('true label:', 1)
('result of model', 0, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 1, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [11, 2, 1])
[11, 2, 1]
('result of model', 4, ': ', [4, 9, 1])
[4, 9, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'E6')
('true label:', 1)
('result of model', 0, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E1')
('true label:', 1)
('result of model', 0, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'E3')
('true label:', 1)
('result of model', 0, ': ', [13, 13, 1])
[13, 13, 1]
('result of model', 1, ': ', [6, 20, 1])
[6, 20, 1]
('result of model', 2, ': ', [6, 20, 1])
[6, 20, 1]
('result of model', 3, ': ', [17, 9, 1])
[17, 9, 1]
('result of model', 4, ': ', [13, 13, 1])
[13, 13, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'H10')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [8, 5, 1])
[8, 5, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'H11')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [9, 4, 1])
[9, 4, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'H15')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'F12')
('true label:', 1)
('result of model', 0, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'F10')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'F17')
('true label:', 1)
('result of model', 0, ': ', [13, 0, 1])
[13, 0, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [9, 4, 1])
[9, 4, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'F15')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [4, 9, 1])
[4, 9, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'H8')
('true label:', 0)
('result of model', 0, ': ', [46, 0, 0])
[46, 0, 0]
('result of model', 1, ': ', [30, 16, 0])
[30, 16, 0]
('result of model', 2, ': ', [20, 26, 0])
[20, 26, 0]
('result of model', 3, ': ', [15, 31, 0])
[15, 31, 0]
('result of model', 4, ': ', [13, 33, 0])
[13, 33, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'H3')
('true label:', 1)
('result of model', 0, ': ', [13, 0, 1])
[13, 0, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'H4')
('true label:', 1)
('result of model', 0, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [10, 3, 1])
[10, 3, 1]
('result of model', 4, ': ', [7, 6, 1])
[7, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'H5')
('true label:', 1)
('result of model', 0, ': ', [13, 13, 1])
[13, 13, 1]
('result of model', 1, ': ', [4, 22, 1])
[4, 22, 1]
('result of model', 2, ': ', [5, 21, 1])
[5, 21, 1]
('result of model', 3, ': ', [9, 17, 1])
[9, 17, 1]
('result of model', 4, ': ', [8, 18, 1])
[8, 18, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D14')
('true label:', 1)
('result of model', 0, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 1, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K12')
('true label:', 0)
('result of model', 0, ': ', [5, 40, 0])
[5, 40, 0]
('result of model', 1, ': ', [32, 13, 0])
[32, 13, 0]
('result of model', 2, ': ', [15, 30, 0])
[15, 30, 0]
('result of model', 3, ': ', [18, 27, 0])
[18, 27, 0]
('result of model', 4, ': ', [7, 38, 0])
[7, 38, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'D12')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [9, 4, 1])
[9, 4, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'D18')
('true label:', 0)
('result of model', 0, ': ', [8, 38, 0])
[8, 38, 0]
('result of model', 1, ': ', [31, 15, 0])
[31, 15, 0]
('result of model', 2, ': ', [18, 28, 0])
[18, 28, 0]
('result of model', 3, ': ', [17, 29, 0])
[17, 29, 0]
('result of model', 4, ': ', [12, 34, 0])
[12, 34, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'B16')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'K2')
('true label:', 0)
('result of model', 0, ': ', [14, 32, 0])
[14, 32, 0]
('result of model', 1, ': ', [30, 16, 0])
[30, 16, 0]
('result of model', 2, ': ', [20, 26, 0])
[20, 26, 0]
('result of model', 3, ': ', [20, 26, 0])
[20, 26, 0]
('result of model', 4, ': ', [15, 31, 0])
[15, 31, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'B14')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K7')
('true label:', 0)
('result of model', 0, ': ', [13, 33, 0])
[13, 33, 0]
('result of model', 1, ': ', [36, 10, 0])
[36, 10, 0]
('result of model', 2, ': ', [17, 29, 0])
[17, 29, 0]
('result of model', 3, ': ', [20, 26, 0])
[20, 26, 0]
('result of model', 4, ': ', [5, 41, 0])
[5, 41, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'B11')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'B18')
('true label:', 0)
('result of model', 0, ': ', [46, 0, 0])
[46, 0, 0]
('result of model', 1, ': ', [35, 11, 0])
[35, 11, 0]
('result of model', 2, ': ', [15, 31, 0])
[15, 31, 0]
('result of model', 3, ': ', [12, 34, 0])
[12, 34, 0]
('result of model', 4, ': ', [5, 41, 0])
[5, 41, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'C9')
('true label:', 1)
('result of model', 0, ': ', [13, 0, 1])
[13, 0, 1]
('result of model', 1, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'C8')
('true label:', 1)
('result of model', 0, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'J18')
('true label:', 1)
('result of model', 0, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C3')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 3, ': ', [10, 3, 1])
[10, 3, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'J15')
('true label:', 1)
('result of model', 0, ': ', [2, 8, 1])
[2, 8, 1]
('result of model', 1, ': ', [2, 8, 1])
[2, 8, 1]
('result of model', 2, ': ', [3, 7, 1])
[3, 7, 1]
('result of model', 3, ': ', [7, 3, 1])
[7, 3, 1]
('result of model', 4, ': ', [2, 8, 1])
[2, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'J13')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [4, 9, 1])
[4, 9, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C5')
('true label:', 1)
('result of model', 0, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 1, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [9, 4, 1])
[9, 4, 1]
('result of model', 4, ': ', [7, 6, 1])
[7, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'I14')
('true label:', 0)
('result of model', 0, ': ', [5, 54, 0])
[5, 54, 0]
('result of model', 1, ': ', [41, 18, 0])
[41, 18, 0]
('result of model', 2, ': ', [26, 33, 0])
[26, 33, 0]
('result of model', 3, ': ', [18, 41, 0])
[18, 41, 0]
('result of model', 4, ': ', [10, 49, 0])
[10, 49, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'I16')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I11')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'G16')
('true label:', 0)
('result of model', 0, ': ', [45, 14, 0])
[45, 14, 0]
('result of model', 1, ': ', [38, 21, 0])
[38, 21, 0]
('result of model', 2, ': ', [20, 39, 0])
[20, 39, 0]
('result of model', 3, ': ', [19, 40, 0])
[19, 40, 0]
('result of model', 4, ': ', [19, 40, 0])
[19, 40, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'G15')
('true label:', 1)
('result of model', 0, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'G11')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [4, 9, 1])
[4, 9, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'F2')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 4, ': ', [8, 5, 1])
[8, 5, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'F5')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F6')
('true label:', 0)
('result of model', 0, ': ', [45, 0, 0])
[45, 0, 0]
('result of model', 1, ': ', [32, 13, 0])
[32, 13, 0]
('result of model', 2, ': ', [17, 28, 0])
[17, 28, 0]
('result of model', 3, ': ', [13, 32, 0])
[13, 32, 0]
('result of model', 4, ': ', [9, 36, 0])
[9, 36, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'F7')
('true label:', 1)
('result of model', 0, ': ', [15, 11, 1])
[15, 11, 1]
('result of model', 1, ': ', [3, 23, 1])
[3, 23, 1]
('result of model', 2, ': ', [8, 18, 1])
[8, 18, 1]
('result of model', 3, ': ', [16, 10, 1])
[16, 10, 1]
('result of model', 4, ': ', [8, 18, 1])
[8, 18, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'M11')
('true label:', 0)
('result of model', 0, ': ', [11, 35, 0])
[11, 35, 0]
('result of model', 1, ': ', [31, 15, 0])
[31, 15, 0]
('result of model', 2, ': ', [20, 26, 0])
[20, 26, 0]
('result of model', 3, ': ', [17, 29, 0])
[17, 29, 0]
('result of model', 4, ': ', [14, 32, 0])
[14, 32, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'J10')
('true label:', 1)
('result of model', 0, ': ', [13, 13, 1])
[13, 13, 1]
('result of model', 1, ': ', [6, 20, 1])
[6, 20, 1]
('result of model', 2, ': ', [3, 23, 1])
[3, 23, 1]
('result of model', 3, ': ', [15, 11, 1])
[15, 11, 1]
('result of model', 4, ': ', [14, 12, 1])
[14, 12, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'M16')
('true label:', 0)
('result of model', 0, ': ', [2, 35, 0])
[2, 35, 0]
('result of model', 1, ': ', [28, 9, 0])
[28, 9, 0]
('result of model', 2, ': ', [15, 22, 0])
[15, 22, 0]
('result of model', 3, ': ', [15, 22, 0])
[15, 22, 0]
('result of model', 4, ': ', [6, 31, 0])
[6, 31, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'E11')
('true label:', 1)
('result of model', 0, ': ', [12, 1, 1])
[12, 1, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 4, ': ', [2, 11, 1])
[2, 11, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'E13')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [9, 4, 1])
[9, 4, 1]
('result of model', 4, ': ', [3, 10, 1])
[3, 10, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'E16')
('true label:', 1)
('result of model', 0, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [8, 5, 1])
[8, 5, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'C13')
('true label:', 1)
('result of model', 0, ': ', [13, 0, 1])
[13, 0, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'C10')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [3, 10, 1])
[3, 10, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'C16')
('true label:', 0)
('result of model', 0, ': ', [46, 0, 0])
[46, 0, 0]
('result of model', 1, ': ', [36, 10, 0])
[36, 10, 0]
('result of model', 2, ': ', [18, 28, 0])
[18, 28, 0]
('result of model', 3, ': ', [21, 25, 0])
[21, 25, 0]
('result of model', 4, ': ', [7, 39, 0])
[7, 39, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'C15')
('true label:', 1)
('result of model', 0, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'A15')
('true label:', 1)
('result of model', 0, ': ', [12, 1, 1])
[12, 1, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [9, 4, 1])
[9, 4, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'A17')
('true label:', 1)
('result of model', 0, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 1, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [7, 6, 1])
[7, 6, 1]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'A10')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'A12')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [4, 9, 1])
[4, 9, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [7, 6, 1])
[7, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'I1')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [8, 5, 1])
[8, 5, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'I3')
('true label:', 1)
('result of model', 0, ': ', [10, 3, 1])
[10, 3, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [7, 6, 1])
[7, 6, 1]
('result of model', 4, ': ', [4, 9, 1])
[4, 9, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'I4')
('true label:', 0)
('result of model', 0, ': ', [8, 50, 0])
[8, 50, 0]
('result of model', 1, ': ', [40, 18, 0])
[40, 18, 0]
('result of model', 2, ': ', [20, 38, 0])
[20, 38, 0]
('result of model', 3, ': ', [26, 32, 0])
[26, 32, 0]
('result of model', 4, ': ', [17, 41, 0])
[17, 41, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'I7')
('true label:', 1)
('result of model', 0, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [3, 10, 1])
[3, 10, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'I6')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [10, 3, 1])
[10, 3, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'A2')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'A5')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [8, 5, 1])
[8, 5, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'A4')
('true label:', 0)
('result of model', 0, ': ', [45, 1, 0])
[45, 1, 0]
('result of model', 1, ': ', [39, 7, 0])
[39, 7, 0]
('result of model', 2, ': ', [24, 22, 0])
[24, 22, 0]
('result of model', 3, ': ', [15, 31, 0])
[15, 31, 0]
('result of model', 4, ': ', [9, 37, 0])
[9, 37, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'A7')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [4, 9, 1])
[4, 9, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I9')
('true label:', 1)
('result of model', 0, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 3, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'L6')
('true label:', 0)
('result of model', 0, ': ', [4, 42, 0])
[4, 42, 0]
('result of model', 1, ': ', [31, 15, 0])
[31, 15, 0]
('result of model', 2, ': ', [15, 31, 0])
[15, 31, 0]
('result of model', 3, ': ', [16, 30, 0])
[16, 30, 0]
('result of model', 4, ': ', [8, 38, 0])
[8, 38, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'L1')
('true label:', 0)
('result of model', 0, ': ', [3, 43, 0])
[3, 43, 0]
('result of model', 1, ': ', [33, 13, 0])
[33, 13, 0]
('result of model', 2, ': ', [19, 27, 0])
[19, 27, 0]
('result of model', 3, ': ', [14, 32, 0])
[14, 32, 0]
('result of model', 4, ': ', [14, 32, 0])
[14, 32, 0]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'D9')
('true label:', 1)
('result of model', 0, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [6, 7, 1])
[6, 7, 1]
('result of model', 4, ': ', [7, 6, 1])
[7, 6, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'D7')
('true label:', 1)
('result of model', 0, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 3, ': ', [5, 8, 1])
[5, 8, 1]
('result of model', 4, ': ', [6, 7, 1])
[6, 7, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D4')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [11, 2, 1])
[11, 2, 1]
('result of model', 4, ': ', [8, 5, 1])
[8, 5, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'D2')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 2, ': ', [1, 12, 1])
[1, 12, 1]
('result of model', 3, ': ', [9, 4, 1])
[9, 4, 1]
('result of model', 4, ': ', [5, 8, 1])
[5, 8, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'D1')
('true label:', 1)
('result of model', 0, ': ', [0, 13, 1])
[0, 13, 1]
('result of model', 1, ': ', [2, 11, 1])
[2, 11, 1]
('result of model', 2, ': ', [3, 10, 1])
[3, 10, 1]
('result of model', 3, ': ', [10, 3, 1])
[10, 3, 1]
('result of model', 4, ': ', [7, 6, 1])
[7, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
Average Patch-wide accuracy is: 0.555
Average Image-wide accuracy given by majority vote is: 0.652747252747
('correct_bagging:', 68)
Bagging accuracy given by input models is: 0.747252747253
