Bagging result of the following models:
['bangqi_results/bangqi_6_9_1_44pm_1/', 'results/8_18_resnet18_l2=1e-5/', 'results/10_5_resnet50/', 'results/9_27_resnet34_l2=1e-4/', 'bangqi_results/bangqi_6_13_12_18pm']

probabilities for 2 classes are:
('image name:', 'Q3')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [15, 35, 1])
[15, 35, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [16, 34, 1])
[16, 34, 1]
('result of model', 4, ': ', [42, 8, 1])
[42, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'K4')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [18, 32, 1])
[18, 32, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 4, ': ', [44, 6, 1])
[44, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AP6')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [29, 21, 0])
[29, 21, 0]
('result of model', 2, ': ', [7, 43, 0])
[7, 43, 0]
('result of model', 3, ': ', [17, 33, 0])
[17, 33, 0]
('result of model', 4, ': ', [43, 7, 0])
[43, 7, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'J1')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 2, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 3, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 4, ': ', [44, 6, 1])
[44, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'K1')
('true label:', 1)
('result of model', 0, ': ', [49, 1, 1])
[49, 1, 1]
('result of model', 1, ': ', [16, 34, 1])
[16, 34, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [18, 32, 1])
[18, 32, 1]
('result of model', 4, ': ', [42, 8, 1])
[42, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AN1')
('true label:', 1)
('result of model', 0, ': ', [39, 11, 1])
[39, 11, 1]
('result of model', 1, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 4, ': ', [45, 5, 1])
[45, 5, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'M3')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [16, 34, 1])
[16, 34, 1]
('result of model', 2, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 3, ': ', [23, 27, 1])
[23, 27, 1]
('result of model', 4, ': ', [43, 7, 1])
[43, 7, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AN3')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [23, 27, 1])
[23, 27, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [16, 34, 1])
[16, 34, 1]
('result of model', 4, ': ', [46, 4, 1])
[46, 4, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AD6')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [24, 26, 0])
[24, 26, 0]
('result of model', 2, ': ', [6, 44, 0])
[6, 44, 0]
('result of model', 3, ': ', [10, 40, 0])
[10, 40, 0]
('result of model', 4, ': ', [45, 5, 0])
[45, 5, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AD7')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [21, 29, 0])
[21, 29, 0]
('result of model', 2, ': ', [5, 45, 0])
[5, 45, 0]
('result of model', 3, ': ', [15, 35, 0])
[15, 35, 0]
('result of model', 4, ': ', [42, 8, 0])
[42, 8, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'G1')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [23, 27, 1])
[23, 27, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [48, 2, 1])
[48, 2, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AJ4')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [18, 32, 1])
[18, 32, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [42, 8, 1])
[42, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'A5')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [20, 30, 0])
[20, 30, 0]
('result of model', 2, ': ', [4, 46, 0])
[4, 46, 0]
('result of model', 3, ': ', [10, 40, 0])
[10, 40, 0]
('result of model', 4, ': ', [43, 7, 0])
[43, 7, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'E4')
('true label:', 1)
('result of model', 0, ': ', [45, 5, 1])
[45, 5, 1]
('result of model', 1, ': ', [15, 35, 1])
[15, 35, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [22, 28, 1])
[22, 28, 1]
('result of model', 4, ': ', [46, 4, 1])
[46, 4, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AF6')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [20, 30, 0])
[20, 30, 0]
('result of model', 2, ': ', [8, 42, 0])
[8, 42, 0]
('result of model', 3, ': ', [15, 35, 0])
[15, 35, 0]
('result of model', 4, ': ', [47, 3, 0])
[47, 3, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'E3')
('true label:', 1)
('result of model', 0, ': ', [47, 3, 1])
[47, 3, 1]
('result of model', 1, ': ', [15, 35, 1])
[15, 35, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [42, 8, 1])
[42, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'E2')
('true label:', 1)
('result of model', 0, ': ', [45, 5, 1])
[45, 5, 1]
('result of model', 1, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 2, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 3, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 4, ': ', [42, 8, 1])
[42, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AG8')
('true label:', 0)
('result of model', 0, ': ', [48, 2, 0])
[48, 2, 0]
('result of model', 1, ': ', [21, 29, 0])
[21, 29, 0]
('result of model', 2, ': ', [6, 44, 0])
[6, 44, 0]
('result of model', 3, ': ', [15, 35, 0])
[15, 35, 0]
('result of model', 4, ': ', [49, 1, 0])
[49, 1, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AO7')
('true label:', 0)
('result of model', 0, ': ', [26, 24, 0])
[26, 24, 0]
('result of model', 1, ': ', [30, 20, 0])
[30, 20, 0]
('result of model', 2, ': ', [6, 44, 0])
[6, 44, 0]
('result of model', 3, ': ', [17, 33, 0])
[17, 33, 0]
('result of model', 4, ': ', [42, 8, 0])
[42, 8, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'L7')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [21, 29, 0])
[21, 29, 0]
('result of model', 2, ': ', [9, 41, 0])
[9, 41, 0]
('result of model', 3, ': ', [20, 30, 0])
[20, 30, 0]
('result of model', 4, ': ', [44, 6, 0])
[44, 6, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'L3')
('true label:', 1)
('result of model', 0, ': ', [41, 9, 1])
[41, 9, 1]
('result of model', 1, ': ', [16, 34, 1])
[16, 34, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [48, 2, 1])
[48, 2, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'N3')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 4, ': ', [44, 6, 1])
[44, 6, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AM6')
('true label:', 0)
('result of model', 0, ': ', [49, 1, 0])
[49, 1, 0]
('result of model', 1, ': ', [26, 24, 0])
[26, 24, 0]
('result of model', 2, ': ', [6, 44, 0])
[6, 44, 0]
('result of model', 3, ': ', [14, 36, 0])
[14, 36, 0]
('result of model', 4, ': ', [45, 5, 0])
[45, 5, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'AE5')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [25, 25, 0])
[25, 25, 0]
('result of model', 2, ': ', [9, 41, 0])
[9, 41, 0]
('result of model', 3, ': ', [15, 35, 0])
[15, 35, 0]
('result of model', 4, ': ', [46, 4, 0])
[46, 4, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AE7')
('true label:', 0)
('result of model', 0, ': ', [49, 1, 0])
[49, 1, 0]
('result of model', 1, ': ', [27, 23, 0])
[27, 23, 0]
('result of model', 2, ': ', [6, 44, 0])
[6, 44, 0]
('result of model', 3, ': ', [16, 34, 0])
[16, 34, 0]
('result of model', 4, ': ', [41, 9, 0])
[41, 9, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'AI4')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 2, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 3, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 4, ': ', [42, 8, 1])
[42, 8, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AI7')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [20, 30, 0])
[20, 30, 0]
('result of model', 2, ': ', [4, 46, 0])
[4, 46, 0]
('result of model', 3, ': ', [13, 37, 0])
[13, 37, 0]
('result of model', 4, ': ', [42, 8, 0])
[42, 8, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'AK6')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [28, 22, 0])
[28, 22, 0]
('result of model', 2, ': ', [5, 45, 0])
[5, 45, 0]
('result of model', 3, ': ', [11, 39, 0])
[11, 39, 0]
('result of model', 4, ': ', [45, 5, 0])
[45, 5, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'AK5')
('true label:', 0)
('result of model', 0, ': ', [49, 1, 0])
[49, 1, 0]
('result of model', 1, ': ', [31, 19, 0])
[31, 19, 0]
('result of model', 2, ': ', [5, 45, 0])
[5, 45, 0]
('result of model', 3, ': ', [21, 29, 0])
[21, 29, 0]
('result of model', 4, ': ', [47, 3, 0])
[47, 3, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'H5')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [29, 21, 0])
[29, 21, 0]
('result of model', 2, ': ', [4, 46, 0])
[4, 46, 0]
('result of model', 3, ': ', [12, 38, 0])
[12, 38, 0]
('result of model', 4, ': ', [46, 4, 0])
[46, 4, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'B3')
('true label:', 1)
('result of model', 0, ': ', [46, 4, 1])
[46, 4, 1]
('result of model', 1, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 2, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 3, ': ', [20, 30, 1])
[20, 30, 1]
('result of model', 4, ': ', [43, 7, 1])
[43, 7, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'AG5')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [24, 26, 0])
[24, 26, 0]
('result of model', 2, ': ', [10, 40, 0])
[10, 40, 0]
('result of model', 3, ': ', [16, 34, 0])
[16, 34, 0]
('result of model', 4, ': ', [41, 9, 0])
[41, 9, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
('image name:', 'D5')
('true label:', 0)
('result of model', 0, ': ', [28, 22, 0])
[28, 22, 0]
('result of model', 1, ': ', [32, 18, 0])
[32, 18, 0]
('result of model', 2, ': ', [2, 48, 0])
[2, 48, 0]
('result of model', 3, ': ', [18, 32, 0])
[18, 32, 0]
('result of model', 4, ': ', [45, 5, 0])
[45, 5, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'M2')
('true label:', 1)
('result of model', 0, ': ', [47, 3, 1])
[47, 3, 1]
('result of model', 1, ': ', [16, 34, 1])
[16, 34, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [20, 30, 1])
[20, 30, 1]
('result of model', 4, ': ', [45, 5, 1])
[45, 5, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'D3')
('true label:', 1)
('result of model', 0, ': ', [48, 2, 1])
[48, 2, 1]
('result of model', 1, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 4, ': ', [48, 2, 1])
[48, 2, 1]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
('image name:', 'Y7')
('true label:', 0)
('result of model', 0, ': ', [50, 0, 0])
[50, 0, 0]
('result of model', 1, ': ', [20, 30, 0])
[20, 30, 0]
('result of model', 2, ': ', [10, 40, 0])
[10, 40, 0]
('result of model', 3, ': ', [13, 37, 0])
[13, 37, 0]
('result of model', 4, ': ', [45, 5, 0])
[45, 5, 0]
('# of model votes positive:', 3, ', # of models vote negative: ', 2)
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++=
Wrong!
Average Patch-wide accuracy is: 0.514666666667
Average Image-wide accuracy given by majority vote is: 0.544444444444
('correct_bagging:', 26)
Bagging accuracy given by input models is: 0.722222222222
