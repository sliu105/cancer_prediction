Bagging result of the following models:
['bangqi_results/bangqi_6_9_1_44pm_1/', 'results/8_18_resnet18_l2=1e-5/', 'results/10_5_resnet50/', 'results/9_27_resnet34_l2=1e-4/', 'bangqi_results/bangqi_6_13_12_18pm']

probabilities for 2 classes are:
('image name:', 'I4')
('true label:', 0)
('result of model', 0, ': ', [149, 1, 0])
[149, 1, 0]
('result of model', 1, ': ', [111, 39, 0])
[111, 39, 0]
('result of model', 2, ': ', [110, 40, 0])
[110, 40, 0]
('result of model', 3, ': ', [115, 35, 0])
[115, 35, 0]
('result of model', 4, ': ', [61, 89, 0])
[61, 89, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'K16')
('true label:', 0)
('result of model', 0, ': ', [7, 193, 0])
[7, 193, 0]
('result of model', 1, ': ', [118, 82, 0])
[118, 82, 0]
('result of model', 2, ': ', [114, 86, 0])
[114, 86, 0]
('result of model', 3, ': ', [115, 85, 0])
[115, 85, 0]
('result of model', 4, ': ', [77, 123, 0])
[77, 123, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'G7')
('true label:', 1)
('result of model', 0, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [7, 43, 1])
[7, 43, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G6')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 2, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G4')
('true label:', 1)
('result of model', 0, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 1, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 2, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 3, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 4, ': ', [11, 39, 1])
[11, 39, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G2')
('true label:', 0)
('result of model', 0, ': ', [143, 7, 0])
[143, 7, 0]
('result of model', 1, ': ', [118, 32, 0])
[118, 32, 0]
('result of model', 2, ': ', [104, 46, 0])
[104, 46, 0]
('result of model', 3, ': ', [105, 45, 0])
[105, 45, 0]
('result of model', 4, ': ', [55, 95, 0])
[55, 95, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'D13')
('true label:', 1)
('result of model', 0, ': ', [26, 74, 1])
[26, 74, 1]
('result of model', 1, ': ', [15, 85, 1])
[15, 85, 1]
('result of model', 2, ': ', [16, 84, 1])
[16, 84, 1]
('result of model', 3, ': ', [28, 72, 1])
[28, 72, 1]
('result of model', 4, ': ', [20, 80, 1])
[20, 80, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G9')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [5, 45, 1])
[5, 45, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G8')
('true label:', 1)
('result of model', 0, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 1, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J9')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 2, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [8, 42, 1])
[8, 42, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J4')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [15, 35, 1])
[15, 35, 1]
('result of model', 2, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [14, 36, 1])
[14, 36, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J5')
('true label:', 1)
('result of model', 0, ': ', [46, 54, 1])
[46, 54, 1]
('result of model', 1, ': ', [14, 86, 1])
[14, 86, 1]
('result of model', 2, ': ', [23, 77, 1])
[23, 77, 1]
('result of model', 3, ': ', [15, 85, 1])
[15, 85, 1]
('result of model', 4, ': ', [24, 76, 1])
[24, 76, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J1')
('true label:', 1)
('result of model', 0, ': ', [19, 81, 1])
[19, 81, 1]
('result of model', 1, ': ', [15, 85, 1])
[15, 85, 1]
('result of model', 2, ': ', [18, 82, 1])
[18, 82, 1]
('result of model', 3, ': ', [18, 82, 1])
[18, 82, 1]
('result of model', 4, ': ', [18, 82, 1])
[18, 82, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J2')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J3')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B4')
('true label:', 1)
('result of model', 0, ': ', [41, 9, 1])
[41, 9, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 3, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'B6')
('true label:', 1)
('result of model', 0, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 1, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [15, 35, 1])
[15, 35, 1]
('result of model', 4, ': ', [9, 41, 1])
[9, 41, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B7')
('true label:', 1)
('result of model', 0, ': ', [53, 47, 1])
[53, 47, 1]
('result of model', 1, ': ', [17, 83, 1])
[17, 83, 1]
('result of model', 2, ': ', [15, 85, 1])
[15, 85, 1]
('result of model', 3, ': ', [27, 73, 1])
[27, 73, 1]
('result of model', 4, ': ', [24, 76, 1])
[24, 76, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'B1')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 2, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [8, 42, 1])
[8, 42, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B9')
('true label:', 1)
('result of model', 0, ': ', [16, 34, 1])
[16, 34, 1]
('result of model', 1, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 4, ': ', [11, 39, 1])
[11, 39, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B13')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 2, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'L14')
('true label:', 0)
('result of model', 0, ': ', [46, 104, 0])
[46, 104, 0]
('result of model', 1, ': ', [121, 29, 0])
[121, 29, 0]
('result of model', 2, ': ', [110, 40, 0])
[110, 40, 0]
('result of model', 3, ': ', [107, 43, 0])
[107, 43, 0]
('result of model', 4, ': ', [65, 85, 0])
[65, 85, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'L12')
('true label:', 0)
('result of model', 0, ': ', [19, 131, 0])
[19, 131, 0]
('result of model', 1, ': ', [111, 39, 0])
[111, 39, 0]
('result of model', 2, ': ', [102, 48, 0])
[102, 48, 0]
('result of model', 3, ': ', [98, 52, 0])
[98, 52, 0]
('result of model', 4, ': ', [55, 95, 0])
[55, 95, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'M1')
('true label:', 0)
('result of model', 0, ': ', [39, 111, 0])
[39, 111, 0]
('result of model', 1, ': ', [113, 37, 0])
[113, 37, 0]
('result of model', 2, ': ', [98, 52, 0])
[98, 52, 0]
('result of model', 3, ': ', [107, 43, 0])
[107, 43, 0]
('result of model', 4, ': ', [68, 82, 0])
[68, 82, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'M3')
('true label:', 0)
('result of model', 0, ': ', [21, 129, 0])
[21, 129, 0]
('result of model', 1, ': ', [116, 34, 0])
[116, 34, 0]
('result of model', 2, ': ', [99, 51, 0])
[99, 51, 0]
('result of model', 3, ': ', [108, 42, 0])
[108, 42, 0]
('result of model', 4, ': ', [63, 87, 0])
[63, 87, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'E5')
('true label:', 1)
('result of model', 0, ': ', [19, 31, 1])
[19, 31, 1]
('result of model', 1, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E4')
('true label:', 1)
('result of model', 0, ': ', [20, 30, 1])
[20, 30, 1]
('result of model', 1, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 2, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E7')
('true label:', 1)
('result of model', 0, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 4, ': ', [13, 37, 1])
[13, 37, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E3')
('true label:', 1)
('result of model', 0, ': ', [38, 62, 1])
[38, 62, 1]
('result of model', 1, ': ', [21, 79, 1])
[21, 79, 1]
('result of model', 2, ': ', [22, 78, 1])
[22, 78, 1]
('result of model', 3, ': ', [18, 82, 1])
[18, 82, 1]
('result of model', 4, ': ', [29, 71, 1])
[29, 71, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'E2')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'H18')
('true label:', 0)
('result of model', 0, ': ', [150, 0, 0])
[150, 0, 0]
('result of model', 1, ': ', [113, 37, 0])
[113, 37, 0]
('result of model', 2, ': ', [105, 45, 0])
[105, 45, 0]
('result of model', 3, ': ', [107, 43, 0])
[107, 43, 0]
('result of model', 4, ': ', [69, 81, 0])
[69, 81, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'H13')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 2, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'F12')
('true label:', 1)
('result of model', 0, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 1, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [9, 41, 1])
[9, 41, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F10')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [14, 36, 1])
[14, 36, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F14')
('true label:', 1)
('result of model', 0, ': ', [5, 95, 1])
[5, 95, 1]
('result of model', 1, ': ', [18, 82, 1])
[18, 82, 1]
('result of model', 2, ': ', [15, 85, 1])
[15, 85, 1]
('result of model', 3, ': ', [25, 75, 1])
[25, 75, 1]
('result of model', 4, ': ', [36, 64, 1])
[36, 64, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'H9')
('true label:', 1)
('result of model', 0, ': ', [53, 97, 1])
[53, 97, 1]
('result of model', 1, ': ', [20, 130, 1])
[20, 130, 1]
('result of model', 2, ': ', [31, 119, 1])
[31, 119, 1]
('result of model', 3, ': ', [35, 115, 1])
[35, 115, 1]
('result of model', 4, ': ', [39, 111, 1])
[39, 111, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'H3')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [8, 42, 1])
[8, 42, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'H5')
('true label:', 1)
('result of model', 0, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 1, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K13')
('true label:', 0)
('result of model', 0, ': ', [13, 137, 0])
[13, 137, 0]
('result of model', 1, ': ', [113, 37, 0])
[113, 37, 0]
('result of model', 2, ': ', [90, 60, 0])
[90, 60, 0]
('result of model', 3, ': ', [103, 47, 0])
[103, 47, 0]
('result of model', 4, ': ', [64, 86, 0])
[64, 86, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'K12')
('true label:', 0)
('result of model', 0, ': ', [19, 181, 0])
[19, 181, 0]
('result of model', 1, ': ', [119, 81, 0])
[119, 81, 0]
('result of model', 2, ': ', [119, 81, 0])
[119, 81, 0]
('result of model', 3, ': ', [126, 74, 0])
[126, 74, 0]
('result of model', 4, ': ', [69, 131, 0])
[69, 131, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'K11')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [10, 40, 1])
[10, 40, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D11')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 3, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 4, ': ', [9, 41, 1])
[9, 41, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K15')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 2, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [9, 41, 1])
[9, 41, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K14')
('true label:', 0)
('result of model', 0, ': ', [17, 133, 0])
[17, 133, 0]
('result of model', 1, ': ', [112, 38, 0])
[112, 38, 0]
('result of model', 2, ': ', [106, 44, 0])
[106, 44, 0]
('result of model', 3, ': ', [100, 50, 0])
[100, 50, 0]
('result of model', 4, ': ', [68, 82, 0])
[68, 82, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'B14')
('true label:', 1)
('result of model', 0, ': ', [1, 49, 1])
[1, 49, 1]
('result of model', 1, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [13, 37, 1])
[13, 37, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'B12')
('true label:', 1)
('result of model', 0, ': ', [1, 99, 1])
[1, 99, 1]
('result of model', 1, ': ', [22, 78, 1])
[22, 78, 1]
('result of model', 2, ': ', [15, 85, 1])
[15, 85, 1]
('result of model', 3, ': ', [22, 78, 1])
[22, 78, 1]
('result of model', 4, ': ', [26, 74, 1])
[26, 74, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'K6')
('true label:', 0)
('result of model', 0, ': ', [102, 48, 0])
[102, 48, 0]
('result of model', 1, ': ', [117, 33, 0])
[117, 33, 0]
('result of model', 2, ': ', [105, 45, 0])
[105, 45, 0]
('result of model', 3, ': ', [99, 51, 0])
[99, 51, 0]
('result of model', 4, ': ', [49, 101, 0])
[49, 101, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'K5')
('true label:', 0)
('result of model', 0, ': ', [71, 79, 0])
[71, 79, 0]
('result of model', 1, ': ', [104, 46, 0])
[104, 46, 0]
('result of model', 2, ': ', [104, 46, 0])
[104, 46, 0]
('result of model', 3, ': ', [100, 50, 0])
[100, 50, 0]
('result of model', 4, ': ', [58, 92, 0])
[58, 92, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'B11')
('true label:', 1)
('result of model', 0, ': ', [48, 2, 1])
[48, 2, 1]
('result of model', 1, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 2, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 3, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 4, ': ', [9, 41, 1])
[9, 41, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'K9')
('true label:', 0)
('result of model', 0, ': ', [22, 178, 0])
[22, 178, 0]
('result of model', 1, ': ', [115, 85, 0])
[115, 85, 0]
('result of model', 2, ': ', [116, 84, 0])
[116, 84, 0]
('result of model', 3, ': ', [124, 76, 0])
[124, 76, 0]
('result of model', 4, ': ', [78, 122, 0])
[78, 122, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'C9')
('true label:', 1)
('result of model', 0, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 1, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 2, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [17, 33, 1])
[17, 33, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C8')
('true label:', 0)
('result of model', 0, ': ', [150, 0, 0])
[150, 0, 0]
('result of model', 1, ': ', [118, 32, 0])
[118, 32, 0]
('result of model', 2, ': ', [113, 37, 0])
[113, 37, 0]
('result of model', 3, ': ', [107, 43, 0])
[107, 43, 0]
('result of model', 4, ': ', [56, 94, 0])
[56, 94, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'C2')
('true label:', 1)
('result of model', 0, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 1, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 2, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 3, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C1')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [15, 35, 1])
[15, 35, 1]
('result of model', 4, ': ', [16, 34, 1])
[16, 34, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J12')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 4, ': ', [10, 40, 1])
[10, 40, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J13')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 4, ': ', [13, 37, 1])
[13, 37, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'J10')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C4')
('true label:', 1)
('result of model', 0, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I17')
('true label:', 1)
('result of model', 0, ': ', [49, 1, 1])
[49, 1, 1]
('result of model', 1, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 2, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [9, 41, 1])
[9, 41, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'I11')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 2, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 3, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 4, ': ', [10, 40, 1])
[10, 40, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I10')
('true label:', 1)
('result of model', 0, ': ', [23, 27, 1])
[23, 27, 1]
('result of model', 1, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 2, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 3, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 4, ': ', [8, 42, 1])
[8, 42, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I13')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 3, ': ', [15, 35, 1])
[15, 35, 1]
('result of model', 4, ': ', [11, 39, 1])
[11, 39, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G13')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 2, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [11, 39, 1])
[11, 39, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G11')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [17, 33, 1])
[17, 33, 1]
('result of model', 4, ': ', [12, 38, 1])
[12, 38, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'G10')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 2, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 3, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 4, ': ', [9, 41, 1])
[9, 41, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F2')
('true label:', 0)
('result of model', 0, ': ', [149, 1, 0])
[149, 1, 0]
('result of model', 1, ': ', [103, 47, 0])
[103, 47, 0]
('result of model', 2, ': ', [109, 41, 0])
[109, 41, 0]
('result of model', 3, ': ', [102, 48, 0])
[102, 48, 0]
('result of model', 4, ': ', [60, 90, 0])
[60, 90, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'F3')
('true label:', 1)
('result of model', 0, ': ', [49, 1, 1])
[49, 1, 1]
('result of model', 1, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 4, ': ', [14, 36, 1])
[14, 36, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'F5')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [11, 39, 1])
[11, 39, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'F7')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 4, ': ', [14, 36, 1])
[14, 36, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'F9')
('true label:', 1)
('result of model', 0, ': ', [50, 0, 1])
[50, 0, 1]
('result of model', 1, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 4, ': ', [17, 33, 1])
[17, 33, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'C5')
('true label:', 1)
('result of model', 0, ': ', [62, 38, 1])
[62, 38, 1]
('result of model', 1, ': ', [13, 87, 1])
[13, 87, 1]
('result of model', 2, ': ', [15, 85, 1])
[15, 85, 1]
('result of model', 3, ': ', [21, 79, 1])
[21, 79, 1]
('result of model', 4, ': ', [34, 66, 1])
[34, 66, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'E13')
('true label:', 1)
('result of model', 0, ': ', [37, 13, 1])
[37, 13, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 3, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'E15')
('true label:', 1)
('result of model', 0, ': ', [43, 7, 1])
[43, 7, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [21, 29, 1])
[21, 29, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'E14')
('true label:', 0)
('result of model', 0, ': ', [70, 130, 0])
[70, 130, 0]
('result of model', 1, ': ', [129, 71, 0])
[129, 71, 0]
('result of model', 2, ': ', [109, 91, 0])
[109, 91, 0]
('result of model', 3, ': ', [126, 74, 0])
[126, 74, 0]
('result of model', 4, ': ', [72, 128, 0])
[72, 128, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'E16')
('true label:', 1)
('result of model', 0, ': ', [40, 60, 1])
[40, 60, 1]
('result of model', 1, ': ', [10, 90, 1])
[10, 90, 1]
('result of model', 2, ': ', [13, 87, 1])
[13, 87, 1]
('result of model', 3, ': ', [24, 76, 1])
[24, 76, 1]
('result of model', 4, ': ', [24, 76, 1])
[24, 76, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'C12')
('true label:', 0)
('result of model', 0, ': ', [143, 7, 0])
[143, 7, 0]
('result of model', 1, ': ', [117, 33, 0])
[117, 33, 0]
('result of model', 2, ': ', [93, 57, 0])
[93, 57, 0]
('result of model', 3, ': ', [113, 37, 0])
[113, 37, 0]
('result of model', 4, ': ', [73, 77, 0])
[73, 77, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'C11')
('true label:', 1)
('result of model', 0, ': ', [78, 22, 1])
[78, 22, 1]
('result of model', 1, ': ', [18, 82, 1])
[18, 82, 1]
('result of model', 2, ': ', [13, 87, 1])
[13, 87, 1]
('result of model', 3, ': ', [18, 82, 1])
[18, 82, 1]
('result of model', 4, ': ', [26, 74, 1])
[26, 74, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'C14')
('true label:', 1)
('result of model', 0, ': ', [32, 18, 1])
[32, 18, 1]
('result of model', 1, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 4, ': ', [7, 43, 1])
[7, 43, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'A11')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 2, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [10, 40, 1])
[10, 40, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I3')
('true label:', 1)
('result of model', 0, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 1, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 2, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I2')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [13, 37, 1])
[13, 37, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [8, 42, 1])
[8, 42, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'I5')
('true label:', 1)
('result of model', 0, ': ', [60, 40, 1])
[60, 40, 1]
('result of model', 1, ': ', [15, 85, 1])
[15, 85, 1]
('result of model', 2, ': ', [11, 89, 1])
[11, 89, 1]
('result of model', 3, ': ', [26, 74, 1])
[26, 74, 1]
('result of model', 4, ': ', [18, 82, 1])
[18, 82, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'A18')
('true label:', 0)
('result of model', 0, ': ', [148, 2, 0])
[148, 2, 0]
('result of model', 1, ': ', [121, 29, 0])
[121, 29, 0]
('result of model', 2, ': ', [102, 48, 0])
[102, 48, 0]
('result of model', 3, ': ', [102, 48, 0])
[102, 48, 0]
('result of model', 4, ': ', [62, 88, 0])
[62, 88, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'I7')
('true label:', 1)
('result of model', 0, ': ', [37, 13, 1])
[37, 13, 1]
('result of model', 1, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [14, 36, 1])
[14, 36, 1]
('# of model votes positive:', 4, ', # of models vote negative: ', 1)
('image name:', 'I6')
('true label:', 1)
('result of model', 0, ': ', [2, 98, 1])
[2, 98, 1]
('result of model', 1, ': ', [20, 80, 1])
[20, 80, 1]
('result of model', 2, ': ', [17, 83, 1])
[17, 83, 1]
('result of model', 3, ': ', [19, 81, 1])
[19, 81, 1]
('result of model', 4, ': ', [22, 78, 1])
[22, 78, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'A3')
('true label:', 1)
('result of model', 0, ': ', [2, 48, 1])
[2, 48, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [5, 45, 1])
[5, 45, 1]
('result of model', 3, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 4, ': ', [13, 37, 1])
[13, 37, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'A2')
('true label:', 1)
('result of model', 0, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 1, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 2, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [14, 36, 1])
[14, 36, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'A6')
('true label:', 0)
('result of model', 0, ': ', [141, 109, 0])
[141, 109, 0]
('result of model', 1, ': ', [138, 112, 0])
[138, 112, 0]
('result of model', 2, ': ', [122, 128, 0])
[122, 128, 0]
('result of model', 3, ': ', [127, 123, 0])
[127, 123, 0]
('result of model', 4, ': ', [93, 157, 0])
[93, 157, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'L6')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [4, 46, 1])
[4, 46, 1]
('result of model', 2, ': ', [14, 36, 1])
[14, 36, 1]
('result of model', 3, ': ', [12, 38, 1])
[12, 38, 1]
('result of model', 4, ': ', [13, 37, 1])
[13, 37, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'L4')
('true label:', 0)
('result of model', 0, ': ', [134, 16, 0])
[134, 16, 0]
('result of model', 1, ': ', [124, 26, 0])
[124, 26, 0]
('result of model', 2, ': ', [118, 32, 0])
[118, 32, 0]
('result of model', 3, ': ', [112, 38, 0])
[112, 38, 0]
('result of model', 4, ': ', [62, 88, 0])
[62, 88, 0]
('# of model votes positive:', 1, ', # of models vote negative: ', 4)
('image name:', 'L5')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 2, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 3, ': ', [18, 32, 1])
[18, 32, 1]
('result of model', 4, ': ', [16, 34, 1])
[16, 34, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'L2')
('true label:', 0)
('result of model', 0, ': ', [57, 143, 0])
[57, 143, 0]
('result of model', 1, ': ', [126, 74, 0])
[126, 74, 0]
('result of model', 2, ': ', [110, 90, 0])
[110, 90, 0]
('result of model', 3, ': ', [122, 78, 0])
[122, 78, 0]
('result of model', 4, ': ', [77, 123, 0])
[77, 123, 0]
('# of model votes positive:', 2, ', # of models vote negative: ', 3)
('image name:', 'D9')
('true label:', 1)
('result of model', 0, ': ', [8, 92, 1])
[8, 92, 1]
('result of model', 1, ': ', [17, 83, 1])
[17, 83, 1]
('result of model', 2, ': ', [13, 87, 1])
[13, 87, 1]
('result of model', 3, ': ', [27, 73, 1])
[27, 73, 1]
('result of model', 4, ': ', [19, 81, 1])
[19, 81, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D6')
('true label:', 1)
('result of model', 0, ': ', [9, 41, 1])
[9, 41, 1]
('result of model', 1, ': ', [6, 44, 1])
[6, 44, 1]
('result of model', 2, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 3, ': ', [11, 39, 1])
[11, 39, 1]
('result of model', 4, ': ', [15, 35, 1])
[15, 35, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D7')
('true label:', 1)
('result of model', 0, ': ', [3, 47, 1])
[3, 47, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [7, 43, 1])
[7, 43, 1]
('result of model', 4, ': ', [11, 39, 1])
[11, 39, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D5')
('true label:', 1)
('result of model', 0, ': ', [8, 92, 1])
[8, 92, 1]
('result of model', 1, ': ', [13, 87, 1])
[13, 87, 1]
('result of model', 2, ': ', [19, 81, 1])
[19, 81, 1]
('result of model', 3, ': ', [25, 75, 1])
[25, 75, 1]
('result of model', 4, ': ', [28, 72, 1])
[28, 72, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
('image name:', 'D1')
('true label:', 1)
('result of model', 0, ': ', [0, 50, 1])
[0, 50, 1]
('result of model', 1, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 2, ': ', [8, 42, 1])
[8, 42, 1]
('result of model', 3, ': ', [10, 40, 1])
[10, 40, 1]
('result of model', 4, ': ', [17, 33, 1])
[17, 33, 1]
('# of model votes positive:', 5, ', # of models vote negative: ', 0)
Average Patch-wide accuracy is: 0.722317073171
Average Image-wide accuracy given by majority vote is: 0.892783505155
('correct_bagging:', 97)
Bagging accuracy given by input models is: 1.0
