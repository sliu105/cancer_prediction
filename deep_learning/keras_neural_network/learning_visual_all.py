import matplotlib.pyplot as plt
import pandas as pd
import os
import sys

# how to run this script:
# Type "python learning_visual_all.py path_to_all_result_folders" in terminal
# results_folder = sys.argv[1]
results_folder = 'results_splitted_ds/'

# Plot entire results_splitted_ds folder 
# comment line 16 - 18 and fill the path into subfolders in line 15 if plot single csv
# Subroutine to find subfolders
subfolders = []
for root, dirs, files in os.walk(results_folder, topdown=False):
    for name in dirs:
    	if 'bagging' not in name:
        	subfolders.append(name)

for subfolder in subfolders:
	dir = os.path.join(results_folder, subfolder)

	for root, dirs, files in os.walk(dir):
		for file in files:
			if file.endswith('.csv'):
				logs = pd.read_csv(os.path.join(dir, file))
				
				# plt.figure()
				# plt.plot(logs['acc'], label = 'training accuracy')
				# plt.plot(logs['val_acc'], label = 'validation accuracy')
				# plt.plot(logs['loss'], label = 'training loss')
				# plt.plot(logs['val_loss'], label = 'validation loss')
				# plt.title('accuracy and loss')
				# plt.xlabel('epoch')
				# plt.legend(['train', 'valid'], loc='upper left')
				# plt.savefig(dir+'/acc-loss.png')

				plt.figure()
				plt.plot(logs['loss'])
				plt.plot(logs['val_loss'])
				plt.title('model loss')
				plt.ylabel('loss')
				plt.xlabel('epoch')
				plt.legend(['train', 'valid'], loc='upper left')
				plt.savefig(dir+'/loss.png')

