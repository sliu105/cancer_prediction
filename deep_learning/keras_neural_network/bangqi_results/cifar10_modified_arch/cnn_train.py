"""
 @author     : Bangqi Wang (bwang.will@gmail.com)
 @file       : cnn_train.py
 @brief      : training neural network
"""


"""
Tutorial:
    1. split the positive and negative images into train, validation, and test.

        + main
            |
            +- cnn_train.py
            |
            +- weights.h5 (output: saved weights)
            |
            +- model.json (outpout: saved model)
            |
            + data
                |
                +- train
                |   |
                |   +- negative
                |   |   |
                |   |   +- images
                |   |
                |   +- positive
                |       |
                |       +- images
                |
                +- validation
                |   |
                |   +- negative
                |   |   |
                |   |   +- images
                |   |
                |   +- positive
                |       |
                |       +- images
                |
                +- test (not used here)

    2. For train, validation, and test datasets, split the images into negative and positive folder.

    3. set the parameters in 'cnn_train.py' (line 72 - 80).

    4. run the script by 'python cnn_train.py'.

    5. the model and weights will be stored in 'model.json' and 'weights.h5'.

    5. copy the results from terminal to a separate txt file.

    6. use 'draw.py' in 'processing/draw_training_curve/' to plot accuracy and loss curves.
"""


"""
libraries
"""
import numpy as np
import keras
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Input, Activation, Dropout, Flatten, Dense
from keras.models import model_from_json
from keras.datasets import cifar10
from keras.callbacks import ReduceLROnPlateau, CSVLogger, EarlyStopping

# train lenet on cifar10 or our data
use_cifar10 = True

"""
parameters
"""
nb_epoch = 101
nb_batch = 32

if not use_cifar10:
    # directory
	train_data_dir = 'data/train'
    	validation_data_dir = 'data/validation'
    # parameters
    	nb_train_samples = 6000
    	nb_validation_samples = 2000
    	input_shape = (128, 128, 3)
    
    	nb_classes = 2
    	kernel_size = 5
else:
    # load data
    	nb_classes = 10
    	(x_train, y_train), (x_test, y_test) = cifar10.load_data()
    	img_width = x_train.shape[1]
    	img_height = x_train.shape[2]
    	channels = x_train.shape[3]
    	input_shape = (img_height, img_width, channels)
    	x_train = x_train.reshape(x_train.shape[0], img_width, img_height, channels)
    	x_test = x_test.reshape(x_test.shape[0], img_width, img_height, channels)

    # normalize data
    	x_train = x_train.astype('float32') / 255
    	x_test = x_test.astype('float32') / 255

    	y_train = keras.utils.to_categorical(y_train, nb_classes)
    	y_test = keras.utils.to_categorical(y_test, nb_classes)
    	nb_classes = 10
    	kernel_size = 3
# callbacks
lr_reducer = ReduceLROnPlateau (factor=np.sqrt(0.1), cooldown=0, patience=5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.0001, patience=10)
csv_logger = CSVLogger('output.csv')

"""
neural network layout
"""
model = Sequential()
model.add(Convolution2D(32, kernel_size = (kernel_size, kernel_size), input_shape = input_shape, data_format = 'channels_last'))
model.add(Activation('relu'))
if not use_cifar10:
	model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Convolution2D(32, kernel_size=(kernel_size, kernel_size)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Convolution2D(64, kernel_size = (3, 3)))
model.add(Activation('relu'))
if not use_cifar10:
	model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Convolution2D(64, kernel_size = (3, 3)))
model.add(Activation('relu'))
if use_cifar10:
	model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Flatten())
if not use_cifar10:
	model.add(Dropout(0.5))
	model.add(Dense(2048))
	model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1024))
model.add(Activation('relu'))
model.add(Dropout(0.5))
if not use_cifar10:
    model.add(Dense(1))
else:
    model.add(Dense(nb_classes))
model.add(Activation('sigmoid'))

model.summary()



"""
load data
"""
if not use_cifar10:
    model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])
    # training data
    train_datagen = ImageDataGenerator(rescale=1./255)
    # 
    test_datagen = ImageDataGenerator(rescale=1./255)

    train_generator = train_datagen.flow_from_directory(
            train_data_dir,
            target_size=(img_width, img_height),
            batch_size=nb_batch,
            class_mode='binary')

    validation_generator = test_datagen.flow_from_directory(
            validation_data_dir,
            target_size=(img_width, img_height),
            batch_size=nb_batch,
            class_mode='binary')

    model.fit_generator(
            train_generator,
            samples_per_epoch=nb_train_samples,
            nb_epoch=nb_epoch,
            validation_data=validation_generator,
            nb_val_samples=nb_validation_samples, 
            callbacks=[lr_reducer, early_stopper, csv_logger])

else:
    model.compile(loss = 'categorical_crossentropy',
		  optimizer = 'rmsprop',
		  metrics = ['accuracy'])
    datagen = ImageDataGenerator(
    featurewise_center=False,  # set input mean to 0 over the dataset
    samplewise_center=False,  # set each sample mean to 0
    featurewise_std_normalization=False,  # divide inputs by std of the dataset
    samplewise_std_normalization=False,  # divide each input by its std
    zca_whitening=False,  # apply ZCA whitening
    rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
    width_shift_range=0.1,  # randomly shift images horizontally (fraction of total width)
    height_shift_range=0.1,  # randomly shift images vertically (fraction of total height)
    horizontal_flip=True,  # randomly flip images
    vertical_flip=False)  # randomly flip images

    # Compute quantities required for featurewise normalization
    # (std, mean, and principal components if ZCA whitening is applied).
    datagen.fit(x_train)

    # Fit the model on the batches generated by datagen.flow().
    model.fit_generator(datagen.flow(x_train, y_train, batch_size=nb_batch),
                        steps_per_epoch=x_train.shape[0],
                        validation_data=(x_test, y_test),
                        epochs=nb_epoch, verbose=1,
                        callbacks=[lr_reducer, early_stopper, csv_logger])

    # Score trained model.
    scores = model.evaluate(x_test, y_test, verbose=1)
    print('Test loss:', scores[0])
    print('Test accuracy:', scores[1])

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("weights.h5")
print("Saved model to disk")
