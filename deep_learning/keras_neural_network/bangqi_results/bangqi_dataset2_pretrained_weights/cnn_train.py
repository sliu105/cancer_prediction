"""
 @author     : Bangqi Wang (bwang.will@gmail.com)
 @file       : cnn_train.py
 @brief      : training neural network
"""


"""
Tutorial:
    1. split the positive and negative images into train, validation, and test.

        + main
            |
            +- cnn_train.py
            |
            +- weights.h5 (output: saved weights)
            |
            +- model.json (outpout: saved model)
            |
            + data
                |
                +- train
                |   |
                |   +- negative
                |   |   |
                |   |   +- images
                |   |
                |   +- positive
                |       |
                |       +- images
                |
                +- validation
                |   |
                |   +- negative
                |   |   |
                |   |   +- images
                |   |
                |   +- positive
                |       |
                |       +- images
                |
                +- test (not used here)

    2. For train, validation, and test datasets, split the images into negative and positive folder.

    3. set the parameters in 'cnn_train.py' (line 72 - 80).

    4. run the script by 'python cnn_train.py'.

    5. the model and weights will be stored in 'model.json' and 'weights.h5'.

    5. copy the results from terminal to a separate txt file.

    6. use 'draw.py' in 'processing/draw_training_curve/' to plot accuracy and loss curves.
"""


"""
libraries
"""
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.models import model_from_json
from keras.callbacks import ReduceLROnPlateau, CSVLogger, EarlyStopping


"""
parameters
"""
# dimensions of images.
img_width, img_height = 128, 128
# directory
train_data_dir = '/home/ubuntu/cancer-prediction/deep_learning/datasets/second_dataset/train'
validation_data_dir = '/home/ubuntu/cancer-prediction/deep_learning/datasets/second_dataset/test'
# parameters
nb_train_samples =24700
nb_validation_samples = 8000
nb_epoch = 50
nb_batch = 32
weights_path = '/home/ubuntu/cancer-prediction/deep_learning/keras_neural_network/bangqi_results/bangqi_6_13_12_18pm/weights.h5'

# callbacks
lr_reducer = ReduceLROnPlateau (factor=0.1, cooldown=0, patience=5, min_lr=0.5e-6)
early_stopper = EarlyStopping(min_delta=0.0001, patience=10)
csv_logger = CSVLogger('output.csv')
"""
neural network layout
"""
model = Sequential()
model.add(Convolution2D(32, 5, 5, input_shape=(img_width, img_height, 3)))
model.add(Activation('relu'))

model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Convolution2D(32, 5, 5))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Convolution2D(64, 3, 3))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Convolution2D(64, 3, 3))
model.add(Activation('relu'))

model.add(Flatten())
model.add(Dropout(0.5))
model.add(Dense(2048))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1024))
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(1))
model.add(Activation('sigmoid'))

model.summary()
model.load_weights(weights_path)
print('Weights loaded from ', weights_path)
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])


"""
load data
"""
# training data
train_datagen = ImageDataGenerator(rescale=1./255)
# 
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=nb_batch,
        class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=nb_batch,
        class_mode='binary')

model.fit_generator(
        train_generator,
        samples_per_epoch=nb_train_samples,
        nb_epoch=nb_epoch,
        validation_data=validation_generator,
        nb_val_samples=nb_validation_samples,
        callbacks=[lr_reducer, early_stopper, csv_logger])

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("weights.h5")
print("Saved model to disk")
