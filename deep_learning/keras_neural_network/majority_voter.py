from keras.models import model_from_json
from keras.preprocessing.image import ImageDataGenerator
import re
import numpy as np
import sys
import os
import pandas as pd

# if model_weight_path is None && loaded_model is not None: directly use loaded_model
# if model_weight_path is not None && loaded_model is None: load model from json and h5 before using the model
# dataset_num = 2 or 4
# train = 1 : predict on training data, train =0: predict on testing data
def vote(model_weight_path, dataset_name):
	# parameters
	img_width, img_height = 128, 128

	# model_file_path   = sys.argv[1]
	# weights_file_path = sys.argv[2]
	# test_data_path    = sys.argv[3]

	# if we are loading model from json and h5 files, we load model
		# check whether both json and h5 file exist
	json_path = os.path.join(model_weight_path, "model.json")
	h5_path = os.path.join(model_weight_path, "weights.h5")
	if (not os.path.isfile(json_path)) or (not os.path.isfile(h5_path)):
		return
	# load json and reconstruct NN model
	json_file = open(json_path, 'r')
	loaded_model_json = json_file.read()
	json_file.close()
	loaded_model = model_from_json(loaded_model_json)
	print(model_weight_path)
	print("Model loaded from json file")

	# load weights into new model
	# print('h5path:', h5_path)
	loaded_model.load_weights(h5_path)
	print("Weights loaded from h5 file")

	# parse the correct dataset name
	if '2' in dataset_name or 'two' in dataset_name or 'second' in dataset_name:
		dataset_name = 'dataset2'
	elif '4' in dataset_name or 'four' in dataset_name or 'fourth' in dataset_name:
		dataset_name = 'dataset4'

	# test model on train & test data of dataset 1~3
	for train in range(3):
		if train == 0:
			train_name = 'train'
		elif train == 1:
			train_name = 'valid'
		else:
			train_name = 'test'

		data_folder = '/home/ubuntu/cancer-prediction/deep_learning/dataset_new'
		test_data_path = os.path.join(data_folder, dataset_name, train_name)
		# generate test data
		test_datagen = ImageDataGenerator(rescale=1./255)
		test_generator = test_datagen.flow_from_directory(
						test_data_path,
						target_size=(img_width, img_height),
						batch_size= 50,  # batch_size number of test data are feed in each time
						class_mode='binary',
						shuffle=False)

		# batch_size * steps should have enough threads to load all test data
		preds = loaded_model.predict_generator(test_generator, steps=np.ceil(len(test_generator.filenames[:])/50))

		# # change from printing to console to printing to files
		# if train == 0:
		# 	output = open(os.path.join(model_weight_path, 'majority_vote_dataset'+str(dataset+1)+'_train.csv'), 'w')
		# elif train == 1:
		# 	output = open(os.path.join(model_weight_path, 'majority_vote_dataset'+str(dataset+1)+'_valid.csv'), 'w')
		# else:
		# 	output = open(os.path.join(model_weight_path, 'majority_vote_dataset'+str(dataset+1)+'_test.csv'), 'w')
		
		# csvwriter = csv.writer(output, delimiter = ' ', quotechar = '|')
		# orig_stdout = sys.stdout
		# sys.stdout = output

		# store result into a dictionary
		# keys: image name, each image has multiple patches
		# value: [# of 0 class vote, # of 1 class vote, real label]
		image_dict = {}
		correct_patch = 0
		for i in range(len(test_generator.filenames[:])):
			file_name = test_generator.filenames[i].split('/')[-1]
			real_label = test_generator.classes[i]
			probability = preds[i]
			# print(probability)

			# get name of the image that current patch belongs to
			temp = re.split('_|-', file_name)
			if 'PR' in temp[0]:
				image_name = temp[1]
			elif 'TMA' in temp[0]:
				image_name = temp[2]
			else:
				image_name = temp[3]

			if image_name not in image_dict:
				image_dict[image_name] = [0, 0, real_label, file_name]

			# predict current patch
			#if probability[0] < 0.5: # for bangqi's code
			if probability[0] > probability[1]:
				image_dict[image_name][0] += 1
				curr_patch_pred = 0
			else:
				image_dict[image_name][1] += 1
				curr_patch_pred = 1

			# accumulator for single patch
			if curr_patch_pred == real_label:
				correct_patch += 1

		# print("image dictionary is:")
		# print(image_dict)

		# print("probabilities for 2 classes are:")

		# we convert the image dictionary to the format that fits the csv
		raw_data = {'image_name': [], 'negative_patchs': [],\
		'positive_patches': [], 'true_label': [], 'right_or_wrong':[]}
		# vote by majority
		images = image_dict.keys()
		correct_image = 0
		for image_name in images:
			result = image_dict[image_name]

			raw_data['image_name'].append(result[3])
			raw_data['negative_patchs'].append(result[0])
			raw_data['positive_patches'].append(result[1])
			raw_data['true_label'].append(result[2])
			# print(result)
			if result[0] > result[1]:
				majority = 0
			else:
				majority = 1

			# majority is the predict given by majority vote
			# now compare it with real label, result[2]
			if majority == result[2]:
				correct_image += 1
				raw_data['right_or_wrong'].append('Correct')
			else:
				raw_data['right_or_wrong'].append('Wrong')

		# save the results of all images in current sub-dataset to csv file
		df = pd.DataFrame(raw_data, columns = ['image_name', 'negative_patchs', 'positive_patches',\
			'true_label', 'right_or_wrong'])
		df.to_csv(os.path.join(model_weight_path, 'majority_vote_'+dataset_name+'_'+train_name+'.csv'))
		if train == 0:
		# report overall accuracy onto txt file
			output = open(os.path.join(model_weight_path, 'majority_vote_'+dataset_name+'.txt'), 'w')
		else:
			output = open(os.path.join(model_weight_path, 'majority_vote_'+dataset_name+'.txt'), 'a')

		orig_stdout = sys.stdout
		sys.stdout = output		
		patch_accuracy = correct_patch*1.0/len(test_generator.filenames[:])
		print('Results of ', model_weight_path, 'on', dataset_name, '_', train_name,':')
		print("Patch-wide accuracy is: {}".format(patch_accuracy))
		image_accuarcy = correct_image*1.0/len(images)
		print("Image-wide accuracy given by majority vote is: {}".format(image_accuarcy))
		print('')
		sys.stdout = orig_stdout
		output.close()
