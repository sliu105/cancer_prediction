import pandas as pd
import sys
import os
import csv
import numpy as np
import glob

# if update param_info on the entire result folder in /keras_neural_network, full = 1
# if update param_info for a single run, full = 0
full = 1

# How to run: 
# type in terminal "python fill_train_valid_data.py path_to_result_folders path_of_parameter_info_to_write"
directory = sys.argv[1] # directory where we will search for log file from
csv_to_write = sys.argv[2] # directory where we will update param_info in
csvfile = open(csv_to_write, 'rb')

# create a new param_info.csv in the directory where the current param_info.csv is located 
out_file = open(os.path.join(os.path.dirname(csv_to_write), 'updated_file.csv'), 'wb')
reader = csv.reader(csvfile)
writer = csv.writer(out_file)

# subfolder: path of the folder that we will search for csv from
# return value: None
def read_data(subfolder):
	filenames = os.listdir(subfolder)

	for file in filenames:
		print('file:', file)
		# print('file:', file)
		if file.endswith('.csv'):
			logs = pd.read_csv(os.path.join(subfolder, file))
			# print('logs:', logs)
			# take average of loss and accuracy in the last 5 epochs before the model stops
			train_acc = np.mean(np.asarray(logs['acc'][-5:-1]))
			train_loss = np.mean(np.asarray(logs['loss'][-5:-1]))
			valid_acc = np.mean(np.asarray(logs['val_acc'][-5:-1]))
			valid_loss = np.mean(np.asarray(logs['val_loss'][-5:-1]))
			print([train_acc, train_loss, valid_acc, valid_loss])
			data = [train_acc, train_loss, valid_acc, valid_loss]
	return data

def update(reader, writer, dictionary):
	# match the folder name and fill in value
	count = 0
	for line in reader:
		if count == 0:
			writer.writerow(line)
			count = 1
			continue

		# print(line[0])
		if line[0] in dictionary.keys():
			line[8] = dictionary[line[0]][0]
			line[9] = dictionary[line[0]][1]
			line[10] = dictionary[line[0]][2]
			line[11] = dictionary[line[0]][3]
			# print('line to write:', line)
			writer.writerow(line)
# dictionary: 
# key: name of result folder
# value: [train_acc, train_loss, val_acc, val_loss] 
# => consistent with order in csv to read and csv to write
subfolders = {}

# if we are updating the log of many runs in a whole result folder:
if full == 1:
	for root, dirs, files in os.walk(directory, topdown=False):
		# iterate every result run
		for name in dirs:
			# print('name:', name)
			if ('bagging' not in name) and (name not in subfolders.keys()): # exclude bagging results
				# iterate every file in a result folder
				subfolder = os.path.join(directory, name)
				subfolders[name] = read_data(subfolder)

else:
	# last layer of the directory is the key
	subfolders[os.path.basename(directory)] = read_data(directory)
	# copy writer from reader line by line
	for line in reader:
		writer.writerow(line)
	# then pass writer to update

# update param_info
update(reader, writer, subfolders)

csvfile.close()
out_file.close()

