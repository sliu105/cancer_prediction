# Crowd Cancer Prediction/deep_learning/neural_network

## New Inference Script
* script functionality: measures **image-wide accuracy** on dataset using majority vote; measures **patch-wide accuracy** on dataset using patch prediction
* script location: `/cancer-prediction/deep_learning/keras_neural_network/majority_voter_new.py`
* script option: can either do majority vote for **all combinations of models and datasets** OR for **a particular model on a particular dataset** 
* If you want to do majority vote for **all combinations of models and datasets**:
    1. set value for `model_folder`, `dataset_folder`, `outcsv_path`
    2. if you also want make inference on training data set, set `include_training = True`; default value is `False`
* If you want to do majority vote for **a particular model on a particular dataset**:
    1. go to `/cancer-prediction/deep_learning/keras_neural_network`
    2. type `python`
    3. type `from majority_voter_new import majorityInstance`
    4. get result by `patch_accu, image_accu = majorityInstance(model_path, weights_path, dataset_path)`

## Other scripts
/data: image patch data for training and testing, with 6000 patches in /test and 2000 patches in /train

/new_data?: perhaps more patches

/output: Bangqi's files

/predict: 

/results: results for duplicating Bangqi's cnn network (folders are in the form of "bangqi_..."), and results of different trails of Resnet (all rest of them), each child folder includes a run with logs parameters, models and weights. See the excel file in /results folder for specific parameter values of Resnet. draw.py generates loss and accuracy v.s. epoch graphs

/sample_output_format: Bangqi's files

cnn_load.py: generates visualization of testing results (possibly buggy)

cnn_train.py: main file of Bangqi's cnn network, run this file to train and test cnn on dataset in /data

negative.txt:

positive.txt:

resnet.py: resnet model of multiple sizes downloaded from https://github.com/raghakot/keras-resnet

resnet1.py: main file of ResNet model building and training, run this file to train and test ResNet on dataset in /data

stackedBarGraph.py: class of bar graph generation imported by cnn_load.py

vgg16.py: VGG Net of 16 layers downloaded from https://github.com/fchollet/keras/blob/master/keras/applications/vgg16.py

vgg16_train.py: main file of VGG Net model building and training, run this file to train and test VGG Net on dataset in /data

bagging.py: take several trained models and their weights, bootstrap-aggregate on them to boost performace

learning_visual.py: plot accuracy vs epoch, and loss vs epoch for A SINGLE  result folder  that was passed as command line argument
	How to run this script: Type in terminal: python learning_visual.py path_of_folder_containing_csv_log
	if you are running this script on a server, uncomment line 8-9 to avoid display error

learning_visual_all.py plot accuracy vs epoch, and loss vs epoch for ALL result folders in the folder that was passed as command line argument
	how to run this script: Type "python learning_visual_all.py path_to_all_result_folders" in terminal
