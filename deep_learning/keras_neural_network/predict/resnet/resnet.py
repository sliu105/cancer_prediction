import keras_resnet.models
from keras.preprocessing.image import ImageDataGenerator
from keras.layers import Activation, Dropout, Flatten, Dense, Input
from keras.models import model_from_json
import keras


# directory
train_data_dir = 'data/train'
validation_data_dir = 'data/validation'

img_width, img_height = 128, 128
nb_train_samples = 6000
nb_validation_samples = 2000
nb_epoch = 101
nb_batch = 32

shape = (128, 128, 3)

x = Input(shape)

y = keras_resnet.models.ResNet50(x)

y = Flatten()(y.output)

y = Dense(1, activation="softmax")(y)

model = keras.models.Model(x, y)

model.compile(optimizer = "adam", loss = "binary_crossentropy", metrics = ["accuracy"])

# training data
train_datagen = ImageDataGenerator(rescale=1./255)
# 
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=nb_batch,
        class_mode='binary')

validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=nb_batch,
        class_mode='binary')

model.fit_generator(
        train_generator,
        samples_per_epoch=nb_train_samples,
        nb_epoch=nb_epoch,
        validation_data=validation_generator,
        nb_val_samples=nb_validation_samples)

model_json = model.to_json()
with open("model.json", "w") as json_file:
    json_file.write(model_json)
# serialize weights to HDF5
model.save_weights("weights.h5")
print("Saved model to disk")
# (training_x, training_y), (_, _) = keras.datasets.cifar10.load_data()

# training_y = keras.utils.np_utils.to_categorical(training_y)

# model.fit(training_x, training_y)