__author__ = "Bangqi Wang"
__email__ = "bwang.will@gmail.com"


""" Library """ 
"""         """
from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.models import model_from_json
from matplotlib import pyplot as plt
from stackedBarGraph import StackedBarGrapher
import numpy as np



""" Variable """
"""          """
# dimensions of our images.
img_width, img_height = 128, 128
# directory path
# test_data_dir = 'positive/'
test_data_dir = 'data/test'
# number of test samples
nb_test_samples = 200
# number of patches for each sample
nb_sample_patches = 25



""" Load model """
"""            """
# load json and create model
json_file = open('model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
loaded_model = model_from_json(loaded_model_json)

# load weights into new model
loaded_model.load_weights("model.h5")
print ("Loaded model from disk") 

# compile model
loaded_model.compile(loss='binary_crossentropy',
                     optimizer='rmsprop',
                     metrics=['accuracy'])



""" Data Generator """
"""                """
# this is the augmentation configuration we will use for testing
test_datagen = ImageDataGenerator(rescale=1./255)
# data generating
test_generator = test_datagen.flow_from_directory(
        test_data_dir,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='binary')

# predict labels
labels = loaded_model.predict_generator(test_generator,steps=200)


""" Calculate Original Labels """
"""                           """
count, negative, positive = 0, 0, 0
images, predictions = [], []
num_len = 0 
for label in labels:
    num_len += 1
    count += 1
    if label[0] < 0.5:
        negative += 1
    else:
        positive += 1
    if count%nb_sample_patches == 0:
        predictions.append([negative/float(nb_sample_patches), positive/float(nb_sample_patches)])
        images.append(str(count/nb_sample_patches))
        negative, positive = 0, 0
# print num_len
print predictions


""" Plot Graph """
"""            """
SBG = StackedBarGrapher()
d = np.array(predictions)

d_widths = [1.] * len(images)
d_labels = images
d_colors = ['#90EE90', '#FA8072']
fig = plt.figure()

ax = fig.add_subplot(111)
SBG.stackedBarPlot(ax,
                   d,
                   d_colors,
                   xLabels=d_labels,
                   yTicks=11,
                   widths=d_widths,
                   gap=0.2
                  )
plt.title("Predictions on Patches")
plt.xlabel("Images")
plt.ylabel("Percentage")
plt.plot((-0.5, len(images)-0.5), (0.5, 0.5), 'k--')

fig.subplots_adjust(bottom=0.4)
plt.tight_layout()
plt.show()
plt.close(fig)
del fig



