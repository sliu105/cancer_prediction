import pandas as pd

resultMatrix = pd.read_csv('./majorityMatrix.csv')
subM1 = resultMatrix[resultMatrix['dataset']=='first_dataset/test']
subM2 = resultMatrix[resultMatrix['dataset']=='second_dataset/test']
subM3 = resultMatrix[resultMatrix['dataset']=='third_dataset/test']
subM4 = resultMatrix[resultMatrix['dataset']=='fourth_dataset/test']

print('Model that has highest imageAccuracy for frist_dataset/test:')
print(subM1.loc[subM1['imageAccuracy'].idxmax()])

print('\nModel that has highest imageAccuracy for second_dataset/test:')
print(subM2.loc[subM2['imageAccuracy'].idxmax()])

print('\nModel that has highest imageAccuracy for third_dataset/test:')
print(subM3.loc[subM3['imageAccuracy'].idxmax()])

print('\nModel that has highest imageAccuracy for fourth_dataset/test:')
print(subM4.loc[subM4['imageAccuracy'].idxmax()])
