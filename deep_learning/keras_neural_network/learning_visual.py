# import matplotlib.pyplot as plt
import pandas as pd
import os
import sys

# How to run this script:
# Type following  in terminal:
# python learning_visual.py path_of_folder_containing_csv_log

 
# -------------Attention----------------
# if you are running this script on a server, uncomment line 8-9 to avoid display error
# import matplotlib
# matplotlib.use('Agg')
import matplotlib.pyplot as plt

dir = sys.argv[1]
############ parameters ###############
# change those 2 parameters each time you run the script !
# dir = './results_splitted_ds/rerun_10_19_resnet18_ds4_1'
# logs = pd.read_csv(dir+'/resnet18.csv')
#######################################
for root, dirs, files in os.walk(dir):
	for file in files:
		if file.endswith('.csv'):
			logs = pd.read_csv(os.path.join(dir, file))
			# summarize history for accuracy
			plt.figure(1)
			plt.plot(logs['acc'])
			plt.plot(logs['val_acc'])
			plt.title('model accuracy')
			plt.ylabel('accuracy')
			plt.xlabel('epoch')
			plt.legend(['train', 'valid'], loc='upper left')
			plt.savefig(dir+'/accu.png')

			# summarize history for loss
			plt.figure(2)
			plt.plot(logs['loss'])
			plt.plot(logs['val_loss'])
			plt.title('model loss')
			plt.ylabel('loss')
			plt.xlabel('epoch')
			plt.legend(['train', 'valid'], loc='upper left')
			plt.savefig(dir+'/loss.png')

