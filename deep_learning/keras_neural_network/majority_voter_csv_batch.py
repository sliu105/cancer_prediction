import majority_voter_csv
import glob
import os
import pandas as pd
import sys

# input the directory of the csv file and result folders 
path = sys.argv[1]
# input the csv file to be processed
csv_file = sys.argv[2]


if __name__ == "__main__":
    # read the giant log
    logs = pd.read_csv(os.path.join(path, csv_file))

    for i in range(len(logs['folder'])):
        # only process results that are not processed yet (majority columns are empty)
        majority_voter_csv.vote(path, i, logs)


 #        df = pd.read_csv(SCRIPT_LOCATION + "/" + folder_input + subfolder + "/" + "labels.csv")
	# model_file_path, weights_file_path, test_data_path, patch_accuracy, image_accuracy = majority_vote(model_file_path, weights_file_path, test_data_path)

