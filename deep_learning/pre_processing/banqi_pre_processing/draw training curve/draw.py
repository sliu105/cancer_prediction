"""
 @author     : Bangqi Wang (bwang.will@gmail.com)
 @file       : draw.py
 @brief      : draw training curves
"""


"""
tutorial:
    
    1. copy the training data from terminal to txt file.

    2. run script to plot curves
"""


"""
library
"""
import matplotlib.pyplot as plt


"""
file name
"""
FILE_NAME = 'example/data.txt'


"""
reading data
"""
with open(FILE_NAME, 'r') as file:
    loss, acc, val_loss, val_acc, x, count = [], [], [], [], [], 0
    for line in file:
        line = line.strip().split(' ')
        if line[0] != 'Found' and line[0] != 'Epoch':
            loss.append(line[6])
            acc.append(line[9])
            val_loss.append(line[12])
            val_acc.append(line[15])
            x.append(count)
            count += 1

    # overall: acc + loss
    f, ax = plt.subplots()
    ax.plot(x, loss, 'b', label='loss')
    ax.plot(x, acc, 'g', label='acc')
    ax.plot(x, val_loss, 'r', label='val_loss')
    ax.plot(x, val_acc, 'k', label='val_acc')
    plt.xlabel('number of epoch')
    plt.ylabel('acc & loss')
    plt.title('CNN')
    plt.legend(loc='upper left')
    plt.grid(True)
    plt.show()

    # acc
    f, ax = plt.subplots()
    ax.plot(x, acc, 'g', label='acc')
    ax.plot(x, val_acc, 'k', label='val_acc')
    plt.xlabel('number of epoch')
    plt.ylabel('acc')
    plt.title('acc')
    plt.legend(loc='upper left')
    plt.grid(True)
    plt.show()

    # loss
    f, ax = plt.subplots()
    ax.plot(x, loss, 'b', label='loss')
    ax.plot(x, val_loss, 'r', label='val_loss')
    plt.xlabel('number of epoch')
    plt.ylabel('loss')
    plt.title('loss')
    plt.legend(loc='upper left')
    plt.grid(True)
    plt.show()
