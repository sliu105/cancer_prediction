"""
 @author     : Bangqi Wang (bwang.will@gmail.com)
 @file       : enlarge_dataset.py
 @brief      : cut images and enlarge dataset
"""


"""
Tutorial:
	this file generate the new datasets with zooming and cutting.

	1. put the script in data folder

		+ main
			|
			+- data
				|
				+- enlarge_dataset.py
				|
				+- positive
				|	|
				|	+- images
				|
				+- negative
				|	|
				|	+- images
				|
				+- p
				|	|
				|	+- cutted_images
				|
				+- n
					|
					+- cutted_images

	2. change parameters: PIECES_NUMBER, LABEL, SAVE_TO_DIRECTORY

	3. run script to cut images.

	4. in output directory, run 'sips -z 128 128 *.png' to resize images.

		sips -z 128 128 *.png (only works on mac terminal)

"""


"""
libraries
"""
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img
import os


"""
Parameters
"""
PIECES_NUMBER = 10
LABEL = 'example'
# LABEL = 'negative'
# LABEL = 'positive'
SAVE_TO_DIRECTORY = 'output' 
# SAVE_TO_DIRECTORY = 'n'
# SAVE_TO_DIRECTORY = 'p'


"""
Image Data Generator
"""
# parameters for cutting
datagen = ImageDataGenerator(
		rescale = 1./255,
		zoom_range = [0.25,0.25],
		rotation_range = 360,
		horizontal_flip = True,
		vertical_flip = True,
		width_shift_range = 0.35,
		height_shift_range = 0.35,
		fill_mode = 'nearest')

"""
Image Cutting
"""
# generate negative or positive training set
for filename in os.listdir(LABEL):
	if filename.endswith('.png'): 
		name = filename.split('.')[0]
		img = load_img(LABEL + '/' + filename)
		x = img_to_array(img)
		x = x.reshape((1,) + x.shape)

		i = 0
		for batch in datagen.flow(x, batch_size = 1, 
									save_to_dir = SAVE_TO_DIRECTORY, 
									save_prefix = name,
									save_format = 'png'):
			i += 1
			if i >= PIECES_NUMBER:
				break
