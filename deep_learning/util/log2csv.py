import sys

def log2csv(input_filename, output_filename):
    file = open(input_filename, "r")
    output_file = open(output_filename, "a+")
    output_file.write("epoch,loss,acc,val_loss,val_acc\n") 
    flag = 0
    for line in file:
        if flag == 0:
            epoch = line.split(" ")[1]
            epoch_num = epoch.split("/")[0]
            output_file.write(str(epoch_num) + ",") 
            flag = 1
        else:
            space_parsed_arr = line.split(" ")
            loss = space_parsed_arr[6]
            acc = space_parsed_arr[9]
            val_loss = space_parsed_arr[12]
            val_acc = space_parsed_arr[15]
            output_file.write(loss+","+acc+","+val_loss+","+val_acc) 
            flag = 0


if __name__ == "__main__":
    stream_lined_input = sys.argv[1]
    input_filename = stream_lined_input + "output.txt"
    output_filename = stream_lined_input +"output.csv"
    log2csv(input_filename, output_filename)

# Usage
# python log2csv.py ../keras_neural_network/results/8_18
