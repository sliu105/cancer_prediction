# Initial Setup: Installing nvidia-docker and nvidia-docker-plugin

wget -P /tmp https://github.com/NVIDIA/nvidia-docker/releases/download/v1.0.1/nvidia-docker_1.0.1-1_amd64.deb

sudo dpkg -i /tmp/nvidia-docker*.deb && rm /tmp/nvidia-docker*.deb

# Initial Setup Test: Test nvidia-smi
nvidia-docker run --rm nvidia/cuda nvidia-smi

# Running the container via registry (Prefered method) CURRENTLY BROKEN DO NOT USE

## Pytorch

sudo docker login registry.gitlab.com

sudo docker run --rm -ti --ipc=host -v $(pwd):/src registry.gitlab.com/akuznets0v/cancer-prediction-images/pytorch

## Keras

sudo docker login registry.gitlab.com

sudo docker run --rm -ti --ipc=host -v $(pwd):/src registry.gitlab.com/akuznets0v/cancer-prediction-images/keras_cudnn6

# Building Docker Image (Backup) USE THIS FOR NOW

## General instructions

Follow the below instructions based on what framework you are using. Both will mount to /src, so first thing you should do is 'cd /src'. From there it should be as if you were running it on your machine. 

## Pytorch

### Build the Docker image

sudo docker build -t pytorch -f Dockerfile.pytorch .

### Run the Docker container based off of the above image.

sudo nvidia-docker run --rm -ti --ipc=host -v $(pwd):/src --name test pytorch

## Keras

### Build the Docker image

sudo docker build -t keras_cudnn6 -f Dockerfile.keras .

### Run the Docker container based off of the above image.

sudo nvidia-docker run --rm -ti --ipc=host -v $(pwd):/src --name test keras_cudnn6

## (Additional useful things) To remove a docker container (if needed)

sudo nvidia-docker container rm test

# How to upload a new container to registry
sudo docker login registry.gitlab.com

docker build -t registry.gitlab.com/akuznets0v/cancer-prediction-images .

docker push registry.gitlab.com/akuznets0v/cancer-prediction-images

# Basic Debugging

## ssh key issues

### Problem

akuznet2@ubuntu:~$ ssh kuz@192.17.239.4
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the RSA key sent by the remote host is
19:06:5b:ff:ff:6b:b9:76:20:0e:7f:0e:74:57:0a:db.
Please contact your system administrator.
Add correct host key in /home/akuznet2/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/akuznet2/.ssh/known_hosts:9
  remove with: ssh-keygen -f "/home/akuznet2/.ssh/known_hosts" -R 192.17.239.4
RSA host key for 192.17.239.4 has changed and you have requested strict checking.
Host key verification failed.

### Solution

As the error says, it is likely safe to just remove the old key and accept a new one when you connect again. Copy and paste the recommended command and run it. On successful first connection you should see:

akuznet2@ubuntu:~$ ssh kuz@192.17.239.4
The authenticity of host '192.17.239.4 (192.17.239.4)' can't be established.
RSA key fingerprint is 19:06:5b:ff:ff:6b:b9:76:20:0e:7f:0e:74:57:0a:db.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added '192.17.239.4' (RSA) to the list of known hosts.

