import img_stats 
import pytest
import numpy as np
import os
from scipy import misc


SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))

def two_image_calculate_mean_and_std(img1, img2):

    # Loading image 1
    image_data = misc.imread(img1)
    red, green, blue = image_data[:,:,0], image_data[:,:,1] , image_data[:,:,2]


    # Loading image 2
    image_data2 = misc.imread(img2)
    red = np.append(red, image_data2[:,:,0])
    green = np.append(green, image_data2[:,:,1])
    blue = np.append(blue, image_data2[:,:,2])

    return [np.mean(red), np.mean(green), np.mean(blue)] , [np.std(red, ddof=1), np.std(green, ddof=1), np.std(blue, ddof=1)] 

def run_test():
    print(two_image_calculate_mean_and_std(SCRIPT_PATH +  "/stats_test/test1.png", SCRIPT_PATH +  "/stats_test/test2.png"))
    # print(img_stats.calculate_mean_and_std(SCRIPT_PATH + "/stats_test"))
    print(img_stats.brute_image_calculate_mean_and_std(SCRIPT_PATH + "/stats_test"))
    # assert(two_image_calculate_mean_and_std(SCRIPT_PATH +  "/stats_test/test1.png", SCRIPT_PATH +  "/stats_test/test2.png") == img_stats.calculate_mean_and_std(SCRIPT_PATH + "/stats_test"))