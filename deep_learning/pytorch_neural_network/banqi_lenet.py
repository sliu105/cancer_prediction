import torch 
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable


import numpy as np
import os
from PIL import Image
# from matplotlib import pyplot as plt
# import matplotlib.image as mpimg
import img_stats

BATCH_SIZE = 128
TEST_BATCH_SIZE = 1000
EPOCHS = 100
LEARNING_RATE = 0.001
SGD_MOMENTUM = 0.5  
SEED = 1
LOG_INTERVAL = 10
SCRIPT_PATH = os.path.dirname(os.path.abspath(__file__))

TEST_DIR = '../neural_network/data/test'
TRAIN_DIR = '../neural_network/data/train'


#Enable Cuda
torch.cuda.manual_seed(SEED)

#Dataloader
kwargs = {'num_workers': 1, 'pin_memory': True}

data_transform = transforms.Compose([
        transforms.RandomHorizontalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.80525186910000002, 0.65896583929999997, 0.74770916259999998],
                             std=[0.11813979450000001, 0.1889225917, 0.14809392709999999])
    ])

#[0.80525186910000002, 0.65896583929999997, 0.74770916259999998] [0.11813979450000001, 0.1889225917, 0.14809392709999999]


train_dataset = datasets.ImageFolder(root=TRAIN_DIR,
                                           transform=data_transform)
test_dataset = datasets.ImageFolder(root=TEST_DIR,
                                           transform=data_transform)

train_loader = torch.utils.data.DataLoader(train_dataset,
                                             batch_size=BATCH_SIZE, 
                                             shuffle=True,
                                             **kwargs)
test_loader = torch.utils.data.DataLoader(test_dataset,
                                             batch_size=TEST_BATCH_SIZE,
                                             shuffle=True,
                                             **kwargs)


#Network
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv2d(3, 32, kernel_size=5) 
        self.conv2 = nn.Conv2d(32, 64, kernel_size=5)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=3)
        self.conv4 = nn.Conv2d(64, 64, kernel_size=3)

        self.conv4_drop = nn.Dropout2d()

        self.fc1 = nn.Linear(64 * 11 * 11, 2048) # Derived from x.size()
        self.fc1_drop = nn.Dropout2d()
        self.fc2 = nn.Linear(2048, 1024)
        self.fc1_drop = nn.Dropout2d()
        self.fc3 = nn.Linear(1024, 2)



    def forward(self, x):
        # print(x.size())
        x = F.relu(F.max_pool2d(self.conv1(x), kernel_size=2))
        # print(x.size())
        x = F.relu(F.max_pool2d(self.conv2(x), kernel_size=2))
        # print(x.size())
        x = F.relu(F.max_pool2d(self.conv3(x), kernel_size=2))
        # print(x.size())
        x = F.relu(self.conv4(x))
        # print(x.size())
        # Derived from x.size()
        # Equivalent to flatten()
        # print(x.size())
        x = x.view(-1, 64 * 11 * 11)

        # print(x.size())
        x = F.relu(self.fc1(x))
        # print(x.size())
        x = F.relu(self.fc2(x))
        # print(x.size())
        return F.log_softmax(x)


model = Net()
model.cuda()

optimizer = optim.SGD(model.parameters(), lr=LEARNING_RATE, momentum=SGD_MOMENTUM)

# train_data_array = []
# tests_data_array = []

def train(epoch):
    model.train()
    for batch, (data, target) in enumerate(train_loader):
        data, target = data.cuda(), target.cuda()
        data, target = Variable(data), Variable(target)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch % LOG_INTERVAL == 0:
            # train_acc_array.append(loss.data[0])
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(epoch, batch * len(data), len(train_loader.dataset), 100. * batch / len(train_loader), loss.data[0]))

def test(epoch):
    model.eval()
    test_loss = 0
    correct = 0
    for data, target in test_loader:
        data, target = data.cuda(), target.cuda()
        data, target = Variable(data, volatile=True), Variable(target)
        output = model(data)
        test_loss += F.nll_loss(output, target).data[0]
        pred = output.data.max(1)[1]
        correct += pred.eq(target.data).cpu().sum()
    test_loss /= len(test_loader)
    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{} ({:.0f}%)\n'.format(test_loss, correct, len(test_loader.dataset), 100. * correct / len(test_loader.dataset)))

def learn():
    for e in range(EPOCHS):
        train(e + 1)
        test(e + 1)

def get_trained_model():
    return model.state_dict()

# def plot_and_dump(input_data):
#     plt.plot(range(0,len(input_data)), [1,4,9,16], 'ro')
#     # plt.axis([0, 6, 0, 20])
#     plt.savefig('trained_advanced_lenet.png')

def get_testcase():
    return next(iter(test_loader))

if __name__ == '__main__':
    learn()
    print(model)
    torch.save(model.state_dict(), os.path.dirname(os.path.realpath(__file__)) + "/trained_advanced_lenet_128.pyt")

