import pytest
import numpy as np
import os
from scipy import misc



# Calculates a merged std without Bessel correction
# Implemeneted from https://stats.stackexchange.com/a/56000
def merge_std(std1, num1, mean1, std2, num2, mean2):
    y_hat = ((num1*mean1) + (num2*mean2)) / (num1+num2)
    return np.sqrt( ( num1*std1**2 + num2*std2**2 + num1*(mean1-y_hat)**2 + num2*(mean2-y_hat)**2 )  / (num1+num2))


# Calculates means and std for a folder
# Calculates means through mean of means
# Calculates std through merging std (without Bessel)
def calculate_mean_and_std(folder):
    # Mean calculations
    red_mean_sum, green_mean_sum, blue_mean_sum = 0,0,0
    red_std, green_std, blue_std = 0,0,0 
    total_imgs = 0
    total_pixels = 0

    num_img = len(os.listdir(folder))

    for image in os.listdir(folder):

        # Loading image
        image_data = misc.imread(folder + "/" + image)

        red, green, blue = image_data[:,:,0], image_data[:,:,1] , image_data[:,:,2]

        # Current std
        temp_red_std = np.std(red)
        temp_green_std = np.std(green)
        temp_blue_std = np.std(blue)

        temp_red_mean = np.mean(red)
        temp_green_mean = np.mean(green)
        temp_blue_mean = np.mean(blue)


        if total_imgs != 0:
            running_red_mean = (red_mean_sum/total_imgs)
            running_blue_mean = (green_mean_sum/total_imgs)
            running_green_mean = (blue_mean_sum/total_imgs)
        else:
            running_red_mean = 0
            running_green_mean = 0
            running_blue_mean = 0
 
        image_pixels = red.shape[0] * red.shape[1]



        # Update global stds
        red_std = merge_std(red_std, total_pixels, running_red_mean, temp_red_std, image_pixels, temp_red_mean) 
        green_std = merge_std(green_std, total_pixels, running_green_mean,  temp_green_std, image_pixels, temp_blue_mean) 
        blue_std =  merge_std(blue_std, total_pixels, running_blue_mean, temp_blue_std, image_pixels, temp_green_mean) 

        # Accumulation of sums
        red_mean_sum += temp_red_mean
        green_mean_sum += temp_green_mean
        blue_mean_sum += temp_blue_mean

        # Update global img count and pixel count
        total_imgs += 1
        total_pixels += image_pixels


    # Mean of means used instead of giant mean
    # total_img = number of means;
    red_avg = red_mean_sum / total_imgs
    green_avg = green_mean_sum / total_imgs
    blue_avg =  blue_mean_sum / total_imgs


    return [red_avg, green_avg, blue_avg], [red_std, green_std, blue_std]
